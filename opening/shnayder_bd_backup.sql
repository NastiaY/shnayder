--
-- PostgreSQL database dump
--

-- Dumped from database version 11.19
-- Dumped by pg_dump version 15rc2

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: public; Type: SCHEMA; Schema: -; Owner: postgres
--

-- *not* creating schema, since initdb creates it


ALTER SCHEMA public OWNER TO postgres;

SET default_tablespace = '';

--
-- Name: accounts; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.accounts (
    login character varying(255) NOT NULL,
    password character varying(255) NOT NULL,
    role boolean DEFAULT false NOT NULL
);


ALTER TABLE public.accounts OWNER TO postgres;

--
-- Name: COLUMN accounts.role; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.accounts.role IS 'true - администратор, false - пользователь';


--
-- Name: accounts_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.accounts_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.accounts_id_seq OWNER TO postgres;

--
-- Name: clients_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.clients_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.clients_id_seq OWNER TO postgres;

--
-- Name: list; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.list (
    email character varying(255) NOT NULL
);


ALTER TABLE public.list OWNER TO postgres;

--
-- Name: messenger; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.messenger (
    "idRoom" integer NOT NULL,
    "idUser" integer NOT NULL,
    "createdAt" timestamp without time zone NOT NULL,
    message character varying NOT NULL,
    state boolean DEFAULT false NOT NULL
);


ALTER TABLE public.messenger OWNER TO postgres;

--
-- Name: COLUMN messenger.state; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.messenger.state IS 'true - прочитан, false - не прочитан';


--
-- Name: orders_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.orders_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.orders_id_seq OWNER TO postgres;

--
-- Name: orders; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.orders (
    id integer DEFAULT nextval('public.orders_id_seq'::regclass) NOT NULL,
    fio character varying NOT NULL,
    phone character varying NOT NULL,
    email character varying NOT NULL,
    "createdAt" timestamp without time zone NOT NULL,
    "updatedAt" timestamp without time zone NOT NULL,
    "statusUser" boolean,
    "statusReady" boolean DEFAULT false NOT NULL,
    brif jsonb NOT NULL
);


ALTER TABLE public.orders OWNER TO postgres;

--
-- Name: COLUMN orders."statusUser"; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.orders."statusUser" IS 'true - в работе, false - отказался, null - думает';


--
-- Name: COLUMN orders."statusReady"; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.orders."statusReady" IS 'true - выполнен, false - в работе (не обработан)';


--
-- Name: projects_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.projects_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.projects_id_seq OWNER TO postgres;

--
-- Name: projects; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.projects (
    id integer DEFAULT nextval('public.projects_id_seq'::regclass) NOT NULL,
    title character varying NOT NULL,
    area integer NOT NULL,
    city character varying NOT NULL,
    description jsonb DEFAULT '["", ""]'::jsonb NOT NULL,
    "createdAt" timestamp without time zone NOT NULL,
    "statusGallery" boolean DEFAULT true NOT NULL,
    type boolean DEFAULT true NOT NULL,
    style character varying NOT NULL,
    color character varying NOT NULL
);


ALTER TABLE public.projects OWNER TO postgres;

--
-- Name: COLUMN projects.type; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.projects.type IS 'true - жилой, false - коммерческий';


--
-- Name: reviews_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.reviews_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.reviews_id_seq OWNER TO postgres;

--
-- Name: reviews; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.reviews (
    message character varying NOT NULL,
    "userId" integer NOT NULL,
    status boolean DEFAULT true NOT NULL,
    id integer DEFAULT nextval('public.reviews_id_seq'::regclass) NOT NULL,
    "createdAt" timestamp without time zone NOT NULL
);


ALTER TABLE public.reviews OWNER TO postgres;

--
-- Name: COLUMN reviews.status; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.reviews.status IS 'true - активен, false - скрыт';


--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.users_id_seq
    START WITH 3
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.users_id_seq OWNER TO postgres;

--
-- Name: users; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.users (
    "firstName" character varying(255) NOT NULL,
    "surName" character varying(255) NOT NULL,
    "secondName" character varying(255) NOT NULL,
    "createdAt" timestamp without time zone NOT NULL,
    "updatedAt" timestamp without time zone,
    phone character varying(50) NOT NULL,
    email character varying(255) NOT NULL,
    id integer DEFAULT nextval('public.users_id_seq'::regclass) NOT NULL,
    status boolean DEFAULT true NOT NULL
);


ALTER TABLE public.users OWNER TO postgres;

--
-- Name: COLUMN users.status; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.users.status IS 'true - активен, false - заблокирован';


--
-- Data for Name: accounts; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.accounts VALUES
	('stasy57@yandex.ru', '$2a$10$1rknjB95S.TZfQkmzwu0J.ojsJRZ0vb.VBL.NnYTHNOzJOZ3lCWKO', true),
	('shnaiderdarina@ya.ru', '$2a$10$Yo0qcfnfGWznxc7JfhWr4.Zug0uSvg8pGhS1P1BRcN2cb2DDN6OPW', true),
	('bebright72@yandex.ru', '$2a$10$VAWXUt3/NIfjlOqzmeZQ0.kaFztvGFxkdws3AOCM8JAuWR5zllYqy', false),
	('dgfgdfg@yandex.ru', '$2a$10$C1Mc65l4ruZfH8ge5MWvg.ikMCvM12ejs1XSQsagIS8EqWxguPVya', false),
	('sdfsdf@yandex.ru', '$2a$10$Y4dE66Ipv8/xL8KiyVg37OfCF2ZK3pzwaUdJDsh81WwNVb7RLZgDi', false),
	('kidgood@yandex.ru', '$2a$10$KJp7Y8UxlCnGvWHY4zagaeJOkNSN/Fh7.X292EEifZvNMtqUeNTSe', false),
	('dasdasda@yandex.ru', '$2a$10$2eFgL7q8nKENoTW57N6Lke7J6/dpqrn/EQwUCxqtcnhfWGJzeIOGW', false),
	('dfsdfsdfsdf@yandex.ru', '$2a$10$az/Hvdu02q1NI7sRUJdaF.bV12vFn5WQWfYZh0j9QEJW54q/xke6C', false),
	('dasdasd@yandex.ru', '$2a$10$zqZElu5Da5o3NywH3MqI6Odzr2X6YLHUqDPN.X2xZZ9IMedVqBEqa', false),
	('fsdfsdfds@yandex.ru', '$2a$10$g10CtvWRZTqcX/ukPoaCS.xvNtXSL24XpFjTSNQmmWTC/VYIq7VL2', false),
	('sfasdasdasd@sdadasd', '$2a$10$lnkh9PGyJvmYRgoRbANK8ebQdp1T5KP2XW/BLE8caqI25fgbDrJ4m', false),
	('sfasdassds@sdadasd', '$2a$10$3qYexgjz2oAT4KAVgQFwzO9MnSIl7EWOMQr9zu4TZPb/AqGsHbXHy', false),
	('fsdfdsfsdf@yandex.ru', '$2a$10$dxMLVtnnDiWeDyGl2T1K3exjg1nU08L40l8t5KG0NkeOrZ3vikmKC', false);


--
-- Data for Name: list; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.list VALUES
	('stasy57@yandex.ru'),
	('shnaiderdarina@ya.ru'),
	('dgfgdfg@yandex.ru'),
	('sdfsdf@yandex.ru'),
	('jdlakhlkfhaas@khasd'),
	('kidgood@yandex.ru'),
	('dfsdfsdfsdf@yandex.ru'),
	('dsdasdasdas@sdasd'),
	('asdasdsd@sfsdfdsf');


--
-- Data for Name: messenger; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.messenger VALUES
	(1, 1, '2023-07-16 06:36:58.915', 'jkfgdg', true),
	(1, 1, '2023-07-16 06:37:33.078', 'kjafjka', true),
	(3, 3, '2023-07-16 06:42:50.528', 'ugkfjkdgsfsd', true),
	(3, 3, '2023-07-16 06:43:01.616', 'sdfhsdfh', true),
	(3, 3, '2023-07-16 06:43:03.057', 'dsfhs', true),
	(3, 3, '2023-07-16 06:43:04.408', 'ghsdjkfgs', true),
	(3, 3, '2023-07-16 06:43:05.496', 'kgsdkf', true),
	(3, 2, '2023-07-16 06:45:05.633', 'c', true),
	(3, 2, '2023-07-16 06:48:55.535', 'jkfgsd', true),
	(6, 6, '2023-07-16 07:05:19.57', 'ноа', true),
	(6, 6, '2023-07-16 07:05:23.555', 'епвглнворлд', true),
	(6, 2, '2023-07-16 07:19:42.795', 'fgd', true),
	(6, 2, '2023-07-16 07:19:44.339', 'fhlk', true),
	(12, 12, '2023-07-16 08:42:16.733', 'dasdasd sdsadasd', false),
	(12, 12, '2023-07-16 08:42:21.717', 'dsds asddasd', false);


--
-- Data for Name: orders; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.orders VALUES
	(4, 'Ан Юр Мих', '89068212756', 'stasy57@yandex.ru', '2023-07-05 13:25:20.932', '2023-07-16 07:18:05.289', NULL, true, '{"area": 170, "type": "офис", "color": "светлая", "light": ["встраиваемые светильники", "подвесные светильники", "трековые системы"], "style": "эко дизайн", "comment": "fghfghfg fhfgjh kyturyrtyty rtytryrtyrtyrty rtyrtyrt", "material": ["дерево", "камень", "ткани"], "typeLight": "верхний свет", "typeMaterial": "натуральные"}'),
	(2, 'Ан Юр Мих', '89068212756', 'stasy57@yandex.ru', '2023-07-05 13:20:04.975', '2023-07-16 07:18:18.844', NULL, true, '{"area": 170, "type": "кафе/ресторан", "color": "насыщенная", "light": ["скрытые подсветки", "торшеры", "бра", "ночники"], "style": "минимализм", "comment": "xfxdsgyfsdfsdf dsfsdfsdfsd dfsdfsdfdsf sdfsdfsdfsdf sdfsdfsdfsd", "material": ["серебро", "экзотические породы древесины"], "typeLight": "настенное/декоративное", "typeMaterial": "необычные"}'),
	(7, 'Ан Юр Мих', '89068212756', 'stasy57@yandex.ru', '2023-07-16 06:35:22.196', '2023-07-16 06:35:22.196', NULL, false, '{"area": 170, "type": "частный дом", "color": "монохром", "light": ["подвесные светильники", "трековые системы"], "style": "классика", "comment": "jkg;doighaiohg;adjkhv;hdgioghasdvio;aghvaghsdkhfuhkgfjwFVKwfkuwf", "material": ["дерево"], "typeLight": "верхний свет", "typeMaterial": "натуральные"}'),
	(3, 'dfsdfsdfsdsdfsdf', '34687336443', 'fsdfsdfdsfghgy@fdsfsd.df', '2023-07-05 13:22:40.919', '2023-07-16 07:17:51.421', true, false, '{"area": "44", "type": "sdfdsfdsf", "color": "dfsdfsdfsdfdsf", "light": ["dfsdfds dfsdfdsf dfsdfsdf"], "style": "fdfd sdffsdfsdfgsd", "comment": "sdfdsf sdfsdfdsfsd fdsfdfd sghgfg ghfghg", "material": ["fdfdfgdf dfdfdf"], "typeLight": "fsdfsdfsdf", "typeMaterial": "fdsfsdfsdf"}'),
	(11, 'Алексей Николаевич Терентьев', '+79617816823', 'kidgood@yandex.ru', '2023-07-16 07:17:04.792', '2023-07-16 07:17:04.792', NULL, false, '{"area": 170, "type": "квартира", "color": "спокойная", "light": [], "style": "минимализм", "comment": "", "material": [], "typeLight": "верхний свет", "typeMaterial": "натуральные"}'),
	(8, 'ASDGFJKGBFJKBbvxkbdjkb', '21543213154', 'ksbdjkvbsdbvbs@klhdf', '2023-07-16 06:41:42.355', '2023-07-17 07:22:16.487', false, false, '{"comment": "заказал звонок"}'),
	(6, 'еукеукек', '34943431619', 'sdfsdfsdf@fsdsds.sd', '2023-07-06 10:07:27.884', '2023-07-17 07:22:16.487', false, false, '{"comment": "заказал звонок"}'),
	(5, 'dsadsadsad', '3454343163', 'sdasdasd@sdsds.ss', '2023-07-06 09:33:17.82', '2023-07-17 07:22:16.487', false, false, '{}');


--
-- Data for Name: projects; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.projects VALUES
	(3, 'офис', 200, 'Омск', '["Текст описание проекта  Текст описание проекта Текст описание проекта Текст описание проекта Текст описание проекта Текст описание проекта Текст описание проекта Текст описание проекта Текст описание проекта Текст описание проекта Текст описание проекта Текст описание проекта Текст описание проекта Текст описание проекта", "Текст описание проекта Текст описание проекта Текст описание проекта Текст описание проекта Текст описание проекта Текст описание проекта Текст описание проекта Текст описание проекта Текст описание проекта Текст описание проекта Текст описание проекта Текст описание проекта Текст описание проекта Текст описание проекта"]', '2023-05-11 11:13:00', false, false, 'Эко дизайн', 'Насыщенная'),
	(2, 'апартаменты', 110, 'Тюмень', '["Текст описание проекта  Текст описание проекта Текст описание проекта Текст описание проекта Текст описание проекта Текст описание проекта Текст описание проекта Текст описание проекта Текст описание проекта Текст описание проекта Текст описание проекта Текст описание проекта Текст описание проекта Текст описание проекта", "Текст описание проекта Текст описание проекта Текст описание проекта Текст описание проекта Текст описание проекта Текст описание проекта Текст описание проекта Текст описание проекта Текст описание проекта Текст описание проекта Текст описание проекта Текст описание проекта Текст описание проекта Текст описание проекта"]', '2023-03-09 10:43:00', true, true, 'Классика', 'Спокойная'),
	(8, 'Двухуровневая квартира', 90, 'Тюмень', '["Текст описание проекта Текст описание проектаТекст описание проекта\nТекст описание проектаТекст описание проектаТекст описание проекта\nТекст описание проектаТекст описание проектаТекст описание проекта\nТекст описание проектаТекст описание проекта проектаТекст описание проекта\nпроектаТекст описание проекта проектаТекст описание проекта\n", "проекта проектаТекст описание проекта\nпроекта проектаТекст описание проекта\nпроекта проектаТекст описание проекта\nпроекта проектаТекст описание проекта\nпроекта проектаТекст описание проекта\nпроекта проектаТекст описание проекта"]', '2023-07-17 07:02:05.128', true, true, 'минимализм', 'спокойная'),
	(7, 'частный дом', 130, 'Тюмень', '["Текст описание проекта Текст описание проекта Текст описание проекта\nТекст описание проекта Текст описание проекта Текст описание проекта\nТекст описание проекта Текст описание проекта Текст описание проекта\nТекст описание проекта Текст описание проекта проекта Текст описание проекта\nпроекта Текст описание проекта проекта Текст описание проекта\n", "проекта проектаТекст описание проекта\nпроекта проектаТекст описание проекта\nпроекта проектаТекст описание проекта\nпроекта проектаТекст описание проекта\nпроекта проектаТекст описание проекта\nпроекта проектаТекст описание проекта"]', '2023-07-17 06:54:05.05', true, true, 'лофт', 'двухцветная'),
	(1, 'квартира-студия', 60, 'Тюмень', '["Текст описание проекта  Текст описание проекта Текст описание проекта Текст описание проекта Текст описание проекта Текст описание проекта Текст описание проекта Текст описание проекта Текст описание проекта Текст описание проекта Текст описание проекта Текст описание проекта Текст описание проекта Текст описание проекта", "Текст описание проекта Текст описание проекта Текст описание проекта Текст описание проекта Текст описание проекта Текст описание проекта Текст описание проекта Текст описание проекта Текст описание проекта"]', '2022-11-11 14:20:00', false, true, 'Лофт', 'Двухцветная');


--
-- Data for Name: reviews; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.reviews VALUES
	('о телефону все подробно рассказали, предложили несколько вариантов и я выбрала самый подходящий по цене и по интерьеру. ', 1, true, 13, '2023-07-12 09:57:27.597'),
	('По телефону все подробно рассказали, 
предложили несколько вариантов и я 
выбрала самый подходящий по цене и
по интерьеру. Вызвала замерщика, 
который замерил все и посоветовал 
какой выбрать ламинат. В итоге 
получилось очень хорошо и красиво!', 2, true, 3, '2023-07-12 09:14:34.983'),
	('По телефону все подробно рассказали, 
предложили несколько вариантов и я 
выбрала самый подходящий по цене и
по интерьеру. Вызвала замерщика, 
который замерил все и посоветовал 
какой выбрать ламинат. В итоге 
получилось очень хорошо и красиво!', 3, true, 4, '2023-07-12 09:16:30.875'),
	('По телефону все подробно рассказали, предложили несколько вариантов и я выбрала самый подходящий по цене и по интерьеру. Вызвала замерщика, который замерил все и посоветовал какой выбрать ламинат. В итоге получилось очень хорошо и красиво!', 1, true, 5, '2023-07-12 09:19:50.855'),
	('По телефону все подробно рассказали, предложили несколько вариантов и я выбрала самый подходящий по цене и по интерьеру. Вызвала замерщика, который замерил все и посоветовал какой выбрать ламинат. В итоге получилось очень хорошо и красиво!', 3, true, 6, '2023-07-12 09:23:05.8'),
	('По телефону все подробно рассказали, предложили несколько вариантов и я выбрала самый подходящий по цене и по интерьеру. Вызвала замерщика, который замерил все и посоветовал какой выбрать ламинат. В итоге получилось очень хорошо и красиво!', 2, true, 7, '2023-07-12 09:24:30.706'),
	('ывпаыв ваыв впыпуке укке укеапавпва пвап вапвапвапва павпвапвап вапвап вапапвпвк пуке апвап вапвапва рукнук нцкнпвапапвап ванувкнукрварвар  впвапв вапвап кнвапва пвапва', 3, false, 8, '2023-07-12 09:25:28.309'),
	('По телефону все подробно рассказали, предложили несколько вариантов и я выбрала самый подходящий по цене и по интерьеру. Вызвала замерщика, который замерил все и посоветовал какой выбрать ламинат. В итоге получилось очень хорошо и красиво! По телефону все подробно рассказали, предложили несколько вариантов и я выбрала самый подходящий по цене и по интерьеру. Вызвала замерщика, который замерил все и посоветовал какой выбрать ламинат. В итоге получилось очень хорошо и красиво!  ', 2, true, 12, '2023-07-12 09:57:05.889'),
	('dsadas sdasdsad sdasdas sdsadasd sda', 2, false, 15, '2023-07-13 05:46:41.845');


--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.users VALUES
	('Дарина', 'Шнайдер', 'Эдуардовна', '2023-06-26 13:45:59.772', '2023-06-26 13:45:59.772', '89827805567', 'shnaiderdarina@ya.ru', 2, true),
	('Иванов', 'Иван', 'Иваныч', '2023-06-27 14:06:35.136', '2023-06-27 14:06:35.136', '64163825548', 'dgfgdfg@yandex.ru', 4, false),
	('Ан', 'Юр', 'Мих', '2023-06-23 12:16:25.878', '2023-07-02 20:29:34.177', '89068212756', 'stasy57@yandex.ru', 1, true),
	('fsdfd', 'sdfs', 'sdfsd', '2023-06-27 14:11:50.854', '2023-07-05 11:39:52.837', '5468424659', 'sdfsdf@yandex.ru', 5, false),
	('Анастасия', 'юр', 'мих', '2023-06-27 14:04:09.41', '2023-07-13 10:09:17.845', '89068212756', 'bebright72@yandex.ru', 3, true),
	('dasdasdasd', 'asdasds', 'dadasdasdasd', '2023-07-16 08:39:24.14', '2023-07-16 08:45:41.722', '43423423423', 'sfasdasdasd@sdadasd', 11, false),
	('dasdasdasd', 'asdasds', 'dadasdasdasd', '2023-07-16 08:41:33.608', '2023-07-16 08:45:41.723', '43423423423', 'sfasdassds@sdadasd', 12, false),
	('fsdfsdf', 'dfddf', 'sdfsdfsdfsdf', '2023-07-16 08:24:18.287', '2023-07-16 08:45:41.723', '5354235235', 'dasdasd@yandex.ru', 9, false),
	('dfsdfsdfsd', 'dfsdfsdfsdf', 'sdfsdfsd', '2023-07-16 08:19:21.565', '2023-07-16 08:45:41.723', '434235235532', 'dasdasda@yandex.ru', 7, false),
	('fsdfsdf', 'fsdfsdfsdf', 'dsfsdffs', '2023-07-16 08:36:56.283', '2023-07-16 08:45:41.723', '5453453453453', 'fsdfsdfds@yandex.ru', 10, false),
	('vcvxcvcxv', 'xcvxcvxc', 'xcvxvxc', '2023-07-16 08:20:13.196', '2023-07-16 08:45:41.723', '5353463463453', 'dfsdfsdfsdf@yandex.ru', 8, false),
	('Алексей', 'Терентьев', 'Николаевич', '2023-07-16 07:01:49.705', '2023-07-16 08:45:41.724', '+79617816823', 'kidgood@yandex.ru', 6, true),
	('dsfsdfsdf', 'sdfsdfsd', 'dsfsdfsdfsdf', '2023-07-16 08:51:58.699', '2023-07-16 08:56:31.043', '545345345345', 'fsdfdsfsdf@yandex.ru', 13, false);


--
-- Name: accounts_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.accounts_id_seq', 1, true);


--
-- Name: clients_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.clients_id_seq', 4, true);


--
-- Name: orders_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.orders_id_seq', 11, true);


--
-- Name: projects_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.projects_id_seq', 8, true);


--
-- Name: reviews_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.reviews_id_seq', 26, true);


--
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.users_id_seq', 13, true);


--
-- Name: accounts unique_Account_login; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.accounts
    ADD CONSTRAINT "unique_Account_login" UNIQUE (login);


--
-- Name: users unique_Clients_email; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT "unique_Clients_email" UNIQUE (email);


--
-- Name: users unique_clients_id; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT unique_clients_id UNIQUE (id);


--
-- Name: list unique_list_email; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.list
    ADD CONSTRAINT unique_list_email UNIQUE (email);


--
-- Name: orders unique_orders_id; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.orders
    ADD CONSTRAINT unique_orders_id UNIQUE (id);


--
-- Name: projects unique_projects_id; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.projects
    ADD CONSTRAINT unique_projects_id UNIQUE (id);


--
-- Name: reviews unique_reviews_id; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.reviews
    ADD CONSTRAINT unique_reviews_id UNIQUE (id);


--
-- Name: users email-login; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT "email-login" FOREIGN KEY (email) REFERENCES public.accounts(login) MATCH FULL ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: messenger idUsers - idUserMess; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.messenger
    ADD CONSTRAINT "idUsers - idUserMess" FOREIGN KEY ("idUser") REFERENCES public.users(id) MATCH FULL ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: reviews user-review; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.reviews
    ADD CONSTRAINT "user-review" FOREIGN KEY ("userId") REFERENCES public.users(id) MATCH FULL ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: SCHEMA public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE USAGE ON SCHEMA public FROM PUBLIC;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

