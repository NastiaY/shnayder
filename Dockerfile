FROM postgres:13

COPY postgres-data/shnayder_bd_backup.sql /docker-entrypoint-initdb.d/

EXPOSE 5432