import React, {useEffect, useState} from 'react';
import './Styles/App.css';
import AppRouter from './Routes/AppRouter';
import {AuthContext} from './context/index';
import axios from 'axios';
import myAxi from './service/interceptor';

function App() {
  const [isAuth, setIsAuth] = useState(false);
  const [isLoad, setIsLoad] = useState(true);
  const [isUser, setIsUser] = useState({ role: false });

  useEffect(() => {
    axios.get(process.env.REACT_APP_API_URL + '/api/auth', { withCredentials: true })
      .then(res => {
        myAxi.get(`/user/${res.data.id}`)
          .then(result => {
            setIsUser({ ...result.data[0], ...res.data });
            setIsAuth(true);
            setIsLoad(false);
          })
      })
      .catch(err => {
        setIsLoad(false);
      });
  }, []);

  return (
    <AuthContext.Provider value={{
      isAuth,
      setIsAuth,
      isLoad,
      isUser,
      setIsUser
    }}>
      <AppRouter />
    </AuthContext.Provider>
  );

};

export default App;
