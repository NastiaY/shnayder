import React, {useContext, useEffect, useState} from 'react';
import {BrowserRouter, Navigate, Route, Routes} from 'react-router-dom';
import Footer from '../Pages/Footer/Footer';
import Header from '../Pages/Header/Header';
import {publicRoutes, privateRoutes} from './routes';
import Authorization from '../Pages/Modal/Authorization';
import Registration from '../Pages/Modal/Registration';
import ArrowGreen from '../components/UI/staticMedia/ArrowGreen.svg';
import cl from '../Pages/Main/Main.module.css';
import {AuthContext} from '../context';
import Loader from '../Pages/Other/Loader';
import Success from '../Pages/Modal/Success';
import MainAdmin from '../Pages/Admin/MainAdmin';
import PassRecovery from '../Pages/Modal/PassRecovery';

const AppRouter = () => {
    const { isAuth, isLoad, isUser } = useContext(AuthContext);
    const [scroll, setScroll] = useState(0);

    const handleScroll = () => {
        setScroll(window.scrollY);
    };

    const handleUp = () => {
        window.scrollTo({ top: 0, behavior: 'smooth' });
    };

    useEffect(() => {
        window.addEventListener('scroll', handleScroll);
        return () => window.removeEventListener('scroll', handleScroll);
    }, []);

    if (isLoad) {
        return <Loader />;
    };

    return (
        <BrowserRouter>
            < img src={ArrowGreen} alt='Наверх'
                className={scroll >= 1080 ? [cl.arrowOnTop, cl.act].join(' ') : cl.arrowOnTop}
                onClick={handleUp} />
            <Header />
            <Authorization />
            <Registration />
            <PassRecovery />
            <Success />
            <Routes>
                {publicRoutes.map(route =>
                    <Route
                        element={route.component}
                        path={route.path}
                        exact={route.exact}
                        key={route.path} />
                )}
                {isAuth &&
                    privateRoutes.map(route =>
                        <Route
                            element={route.component}
                            path={route.path}
                            exact={route.exact}
                            key={route.path} />)}
                {isUser.role &&
                    <Route path='/adminpanel/*' element={<MainAdmin />} />}
                <Route path='/*' element={<Navigate to='/' replace />} />
            </Routes>
            <Footer />
        </BrowserRouter >
    );
};

export default AppRouter;