import MainFull from '../Pages/Main/MainFull';
import Collaborate from '../Pages/Other/Collaborate';
import Projects from '../Pages/Other/Projects';
import Reviews from '../Pages/Other/Reviews';
import Brif from '../Pages/Other/Brif';
import UsersTable from '../Pages/Admin/UsersTable';
import OrdersTable from '../Pages/Admin/OrdersTable';
import ConstructorMessages from '../Pages/Admin/ConstructorMessages';
import ChatAdmin from '../Pages/Admin/ChatAdmin';
import Layout from '../Pages/Main/CompProject/Layout';
import Concept from '../Pages/Main/CompProject/Concept';
import Visual3D from '../Pages/Main/CompProject/Visual3D';
import DocProject from '../Pages/Main/CompProject/DocProject';
import Specific from '../Pages/Main/CompProject/Specific';
import Policy from '../Pages/Other/Policy';
import Designer from '../Pages/Other/Designer';
import Cost from '../Pages/Other/Cost';
import Contact from '../Pages/Other/Contact';
import SelectProject from '../Pages/Other/SelectProject';
import Unsubscribe from '../Pages/Other/Unsubscribe';
import Recovery from '../Pages/Other/Recovery';


export const publicRoutes = [
    {path: '/*', component: <MainFull/>, exact: true},
    {path: '/collaborate', component: <Collaborate/>, exact: true},
    {path: '/project', component: <Projects/>, exact: true},
    {path: '/project/:id', component: <SelectProject/>, exact: true},
    {path: '/reviews', component: <Reviews/>, exact: true},
    {path: '/policy', component: <Policy/>, exact: true},
    {path: '/aboutus', component: <Designer/>, exact: true},
    {path: '/cost', component: <Cost/>, exact: true},
    {path: '/contacts', component: <Contact/>, exact: true},
    {path: '/unsubscribe', component: <Unsubscribe/>, exact: true},
    {path: '/recovery/:link', component: <Recovery/>, exact: true}
]

export const privateRoutes = [
    {path: '/brif', component: <Brif/>, exact: true}
]

export const adminRoutes = [
    {path: '/users', component: <UsersTable/>, exact: true},
    {path: '/orders', component: <OrdersTable/>, exact: true},
    {path: '/messages', component: <ConstructorMessages/>, exact: true},
    {path: '/chat/*', component: <ChatAdmin/>, exact: true},
]

export const compRoutes = [
    {path: '/', component: <Layout/>, exact: true},
    {path: '/concept', component: <Concept/>, exact: true},
    {path: '/visual', component: <Visual3D/>, exact: true},
    {path: '/doc', component: <DocProject/>, exact: true},
    {path: '/spec', component: <Specific/>, exact: true}
]