import React, { useContext } from 'react';
import cl from './Components.module.css';
import { AuthContext } from '../context/index';
import moment from 'moment';


const ChatMsgFrame = ({ msg }) => {

    const { isUser } = useContext(AuthContext);

    return (
        <div className={msg.idUser === isUser.id ? cl.me : cl.forMe}>

            <span className={cl.msg}>{msg.message}</span>

            <span className={msg.idUser === isUser.id ? cl.time : cl.timeFor}>
                {moment(msg.createdAt).format('HH:mm  DD.MM.YYYY')}</span>

        </div>
    );
};

export default ChatMsgFrame;