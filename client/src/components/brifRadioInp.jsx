import React from 'react';
import cl from './Components.module.css';
import BrifState from '../Pages/Other/BrifState';


const BrifRadioInp = ({ children, checked, param }) => {

    return (
        <div className={cl.choiceWithImg}>
            <input type='radio'
                value={children}
                checked={checked === children}
                onChange={e => BrifState.setWishList({ [param]: e.target.value })} />
            <label>{children}</label>
        </div>
    );
};

export default BrifRadioInp;