import React from 'react';
import cl from './Components.module.css';
import MainH3black from './UI/hats/MainH3black';
import BrifRadioInp from './BrifRadioInp';

const BrifFotoChoice = ({children, urls, variable, wishList, parameter}) => (
    <div className={cl.blockChoice}>
        <MainH3black style={{ marginBottom: '23px' }}>
            {children}
        </MainH3black>

        <div className={cl.Choice}>
            {urls.map(item => {
                return (
                    <img src={item} key={item} alt='' className={cl.imgChoice}/>
                );
            })}
        </div>

        <div className={cl.Choice}>
            {variable.map(item => {
                return (
                    <BrifRadioInp checked={wishList} param={parameter} key={item}>
                        {item}
                    </BrifRadioInp>
                );
            })}
        </div>
    </div>
);

export default BrifFotoChoice;