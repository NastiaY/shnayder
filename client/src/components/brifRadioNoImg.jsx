import React from 'react';
import cl from './Components.module.css';
import BrifState from '../Pages/Other/BrifState';

const BrifRadioNoImg = ({ children, checked, param }) => {

    return (
        <div className={cl.choiceNoImg}>
            <input type='radio'
                value={children}
                checked={checked === children}
                onChange={e => {
                    BrifState.setWishList({ [param]: e.target.value });
                    param === 'typeLight' ?
                        BrifState.setWishList({ light: [] }) :
                        BrifState.setWishList({ material: [] })}} />
            <label>{children}</label>
        </div>
    );
};

export default BrifRadioNoImg;