import React, { useContext, useEffect, useState } from 'react';
import cl from './Components.module.css';
import { AuthContext } from '../context/index';
import ChatMsgFrame from './ChatMsgFrame';
import MsgInput from './UI/other/msgInput';
import SendMsgBtn from './UI/buttons/SendMsgBtn';
import myAxi from '../service/interceptor';
import socket from '../service/webSocket';
import { useParams } from 'react-router-dom';
import AdminState from '../Pages/Admin/AdminState';
import { observer } from 'mobx-react-lite';


const WindSelectUser = () => {

    const { isUser } = useContext(AuthContext);
    const [valMsg, setValMsg] = useState([]);
    const [inpValue, setInpVal] = useState('');

    const {id} = useParams();

    var now = new Date();

    async function AddNewMsg(e) {
        e.preventDefault();
        const newMsg = {
            createdAt: now.toISOString(),
            idRoom: parseInt(id, 10),
            idUser: isUser.id,
            message: inpValue,
            state: true
        };
        socket.emit('message', newMsg);
        await myAxi.post('/chat', newMsg);
        setInpVal('');
    };

    useEffect(() => {
        AdminState.setUseRoom(parseInt(id, 10));
        myAxi.get('/chat', { params: { id: id } })
            .then(res => {
                setValMsg(res.data);
                if (res.data.some(msg => msg.state === false)) {
                    myAxi.put(`/room?id=${id}`);
                    AdminState.upUserRoom(id, 0);
                }
            })
            .catch(err => {
                console.log(err);
            });
    }, [id]);

    useEffect(() => {
        socket.on('response', data => {
            if (data.idRoom === AdminState.useRoom) {
                setValMsg([...valMsg, {...data, state: true}]);
            };
        })
    }, [valMsg]);

    useEffect(() => {
        return () => {
            AdminState.setUseRoom(0);
        }
    }, []);

    return (
        <div className={cl.blockMsgAdmin}>

                <div className={cl.messegesAdmin}>

                    <div className={cl.allMsg}>
                        {(valMsg.map((post) => {
                            return <ChatMsgFrame msg={post} key={post.createdAt} />
                        }))}
                    </div>

                </div>
                

                <form onSubmit={AddNewMsg} className={cl.formSendMsgAdmin}>

                    <MsgInput type='text'
                        maxLength='500'
                        value={inpValue}
                        onChange={e => setInpVal(e.target.value)}
                        placeholder='Сообщение...' />

                    <SendMsgBtn disabled={!inpValue}/>

                </form>

            </div>
    );
};

export default observer(WindSelectUser);