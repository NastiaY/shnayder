import React from 'react';
import {NavLink} from 'react-router-dom';
import cl from './Components.module.css';
import moment from 'moment';
import 'moment/locale/ru';
import Title from './UI/hats/Title';

const titleProject = ({item}) => (
        <NavLink className={cl.titlePr} to={`/project/${item.id}`}>
            <img src={item.urls[2]} alt='' className={cl.imgProjectAva}/>
            <div className={cl.hatPr}> 
                <Title variant={'green16'} wrapper={'shrink'}>{item.title}</Title>
                <span className={cl.areaPr}>{`${item.area} кв.м`}</span>
            </div>
            <span className={cl.dataPr}>
                {moment(item.createdAt).locale('ru').format('MMMM, YYYY')}
            </span>
        </NavLink>
    );

export default titleProject;