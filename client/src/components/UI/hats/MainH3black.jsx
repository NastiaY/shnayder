import React from 'react';
import cl from './Hats.module.css';


const MainH3black = ({ children, ...props }) => {

    return (
        <span {...props} className={cl.mainH3black}>{children}</span>
    );
};

export default MainH3black;