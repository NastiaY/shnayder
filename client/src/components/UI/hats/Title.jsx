import React from 'react';
import cl from './Hats.module.css';

const Title = ({children, variant, wrapper, ...props}) => {
  const variantClass = {
    'gray14': cl.gray14,
    'gray14Capitalize': cl.gray14Capitalize,
    'gray16': cl.gray16,
    'black16': cl.black16,
    'black24': cl.black24,
    'black32': cl.black32,
    'white12': cl.white12,
    'green16': cl.green16
  };

  const variantWrapper = {
    'green': cl.greenWrapper,
    'shrink': cl.wrapperShrink
  };

  return (
    <div className={wrapper ? variantWrapper[wrapper] : null}>
      <span
          {...props}
          className={variant ? variantClass[variant] : null}
      >
          {children}
      </span>
    </div>
  );
};

export default Title;