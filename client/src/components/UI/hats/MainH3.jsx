import React from 'react';
import cl from './Hats.module.css';


const MainH3 = ({ children, classes, ...props }) => {

    return (
        <span {...props} className={cl.mainH3 }>{children}</span>
    );
};

export default MainH3;