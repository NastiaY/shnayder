import React from 'react';
import cl from './Hats.module.css';


const MainH2 = ({ children, ...props }) => {

    return (
        <span {...props} className={cl.mainH2}>{children}</span>
    );
};

export default MainH2;