import React from 'react';
import cl from './Hats.module.css';


const MainH1 = ({ children, ...props }) => {

    return (
        <span {...props} className={cl.mainH1}>{children}</span>
    );
};

export default MainH1;