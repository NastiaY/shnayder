import React from 'react';
import cl from './Hats.module.css';


const MainH4 = ({ children, ...props }) => {

    return (
        <span {...props} className={cl.mainH4}>{children}</span>
    );
};

export default MainH4;