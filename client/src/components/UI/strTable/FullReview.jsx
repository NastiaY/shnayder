import React, {useContext, useState} from 'react';
import cl from './Str.module.css';
import cn from 'classname';
import myAxi from '../../../service/interceptor';
import moment from 'moment';
import AddBlockBtn from '../buttons/AddBlockBtn';
import UnlockBtn from '../buttons/UnlockBtn';
import {AuthContext} from '../../../context';
import SmallText from '../other/smallText';
import basket from '../staticMedia/basket.svg';
import Title from '../hats/Title';

const FullReview = ({post, DelRev}) => {
    const { isUser } = useContext(AuthContext);

    const [dataPost, setDataPost] = useState(post);

    async function changedStatus(e, newStatus) {
        e.preventDefault();
        setDataPost({ ...dataPost, status: newStatus });
        const upParams = { status: newStatus };
        await myAxi.put(`/review?id=${post.id}`, upParams).catch(err => console.log(err))
    };


    return (
        <div className={dataPost.status ? cl.activeRev : cn(cl.activeRev, cl.blocked)}>
            <span>{post.message}</span>
            <div className={cl.footRev}>
                <div className={cl.pathFootRev}>
                    <Title variant={'gray14'}>
                        {`Автор ${post.firstName + ' ' + post.surName[0]}.`}
                    </Title>

                    {isUser.role && (
                        <SmallText>{post.email}</SmallText>
                    )}
                </div>
                <div className={cl.pathFootRev}>
                    <span className={cl.timeReview}>
                        {moment(post.createdAt).format('DD.MM.YYYY')}
                    </span>
                    {isUser.role && (
                        <div style={{ width: '140px' }}>
                            {dataPost.status ?
                                <AddBlockBtn onClick={e => changedStatus(e, false)}>Заблокировать</AddBlockBtn> :
                                <UnlockBtn onClick={e => changedStatus(e, true)}>Разблокировать</UnlockBtn>}
                        </div>
                    )}
                    {isUser.role && (
                        <form>
                            <button className={cl.basketBtn} onClick={e => DelRev(e, post.id)}>
                                <img src={basket} alt='удалить' />
                            </button>
                        </form>
                    )}
                </div>
            </div>
        </div>
    );
};

export default FullReview;