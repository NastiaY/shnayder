import React from 'react';
import cl from './Str.module.css';
import myAxi from '../../../service/interceptor';
import CellTable from '../other/cellTable';
import delList from '../staticMedia/delList.svg';
import AdminState from '../../../Pages/Admin/AdminState';
import { observer } from 'mobx-react-lite';


const StrList = ({ mail }) => {

    async function deleteEmailList(e) {
        e.preventDefault();
        if (window.confirm('Вы действительно хотите удалить email из рассылки?')) {
            await myAxi.delete(`/mail?email=${mail.email}`).catch(err => console.log(err));
            AdminState.delEmailList(mail.email);
        };
    };

    return (
        <div className={cl.formList}>

            <CellTable style={{ width: '330px' }}>{mail.email}</CellTable>

            <form className={cl.formList}>
                <button className={cl.brifBtn}>
                    <img src={delList} alt='Удалить из рассылки' onClick={e => deleteEmailList(e)} />
                </button>Удалить из рассылки
            </form>

        </div>
    );
};

export default observer(StrList);