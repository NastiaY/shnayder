import React, { useState } from 'react';
import cl from './Str.module.css';
import myAxi from '../../../service/interceptor';
import moment from 'moment';
import CellTable from '../other/cellTable';
import brif from '../staticMedia/brif.svg';
import basket from '../staticMedia/basket.svg';
import AdminState from '../../../Pages/Admin/AdminState';
import cloud from '../../../service/cloud';


const StrOrder = ({ order }) => {

    const [dataOrder, setDataOrder] = useState(order);
    const [valStatus, setValStatus] = useState(order.statusUser);
    const [visibleBrif, setVisibleBrif] = useState(false);
    const [urls, setUrls] = useState([]);
    const [sizeImg, setSizeImg] = useState([false, false, false, false, false, false]);

    let now = new Date();

    async function ChangeStatusReady(newStatus) {
        setDataOrder({ ...dataOrder, statusReady: newStatus, updatedAt: now.toISOString() });
        const upParams = { statusReady: newStatus, updatedAt: now.toISOString() };
        await myAxi.put(`/orders?id=${order.id}`, upParams).catch(err => console.log(err));
    };

    async function ChangeStatusUser(newStatus) {
        setValStatus(newStatus);
        setDataOrder({ ...dataOrder, statusUser: newStatus, updatedAt: now.toISOString() });
        const upParams = { statusUser: newStatus, updatedAt: now.toISOString() };
        await myAxi.put(`/orders?id=${order.id}`, upParams).catch(err => console.log(err));
    };

    async function deleteOrder(e) {
        e.preventDefault();
        if (window.confirm('Вы действительно хотите удалить заявку?')) {
            await myAxi.delete(`/orders?id=${order.id}`).catch(err => console.log(err));
            await cloud.deleteChildDirectory('orders', `${order.id}`);
            AdminState.delOrder(order.id);
        };
    };

    async function ChangeVisibleBrif(e, state) {
        e.preventDefault();
        let res = await cloud.getUrlsChild('orders', `${order.id}`);
        setUrls(res);
        setVisibleBrif(state);
    };

    return (
        <div className={cl.fulStrOrder}>

            <div className={cl.strAllOrder}>

                <form><button className={cl.brifBtn} onClick={e => ChangeVisibleBrif(e, !visibleBrif)}><img src={brif} alt='бриф' /></button></form>

                <CellTable style={{ width: '53px' }}>{order.id}</CellTable>

                <CellTable style={dataOrder.statusReady ? { width: '200px', background: '#2C615E30' } : { width: '200px' }}>{order.fio}</CellTable>

                <CellTable style={{ width: '170px' }}>{order.email}</CellTable>

                <CellTable style={{ width: '130px' }}>{order.phone}</CellTable>

                <div className={cl.statusUserOrder}>
                    <div>
                        <input type='radio'
                            checked={valStatus === true}
                            onChange={() => ChangeStatusUser(true)} />
                        <label>в работе</label>
                    </div>
                    <div>
                        <input type='radio'
                            checked={valStatus === null}
                            onChange={() => ChangeStatusUser(null)} />
                        <label>думает</label>
                    </div>
                    <div>
                        <input type='radio'
                            checked={valStatus === false}
                            onChange={() => ChangeStatusUser(false)} />
                        <label>отказ</label>
                    </div>
                </div>

                <CellTable style={{ width: '90px' }}>
                    {moment(dataOrder.createdAt).format('D.MM.YYYY')}</CellTable>

                <CellTable style={{ width: '90px' }}>
                    {moment(dataOrder.updatedAt).format('D.MM.YYYY')}</CellTable>

                <div className={cl.statusReadyOrder}>
                    <input type='checkbox'
                        checked={dataOrder.statusReady}
                        onChange={() => ChangeStatusReady(!dataOrder.statusReady)} />
                </div>

                <form><button className={cl.basketBtn} onClick={e => deleteOrder(e)}><img src={basket} alt='удалить' /></button></form>

            </div>

            {visibleBrif &&
                (<div className={cl.blockBrifOrder}>

                    <div className={cl.brifTextOrder}>

                        {order.brif.area ? <span>Площадь: {order.brif.area}</span> : null}
                        {order.brif.type ? <span>Тип помещения: {order.brif.type}</span> : null}
                        {order.brif.style ? <span>Стиль: {order.brif.style}</span> : null}
                        {order.brif.color ? <span>Гамма: {order.brif.color}</span> : null}
                        {order.brif.typeLight ? <span>Освещение: {order.brif.typeLight}</span> : null}
                        {order.brif.light ? <span>- {order.brif.light.map(i => i).join(', ')}</span> : null}
                        {order.brif.typeMaterial ? <span>Материалы: {order.brif.typeMaterial}</span> : null}
                        {order.brif.material ? <span>- {order.brif.material.map(i => i).join(', ')}</span> : null}
                        {order.brif.comment ? <span>Комментарий: {order.brif.comment}</span> : null}

                    </div>

                    {urls.map((item, ind) => {
                        return (
                            <img alt=''
                                src={item}
                                key={`imgOrder-${ind}`}
                                className={sizeImg[ind] ? [cl.imgFotoOrder, cl.full].join(' ') : cl.imgFotoOrder}
                                onClick={() => {
                                    let upSizeImg = [...sizeImg];
                                    upSizeImg[ind] = !upSizeImg[ind];
                                    setSizeImg(upSizeImg)
                                }} />
                        )
                    })}

                </div>)}
        </div>
    );
};

export default StrOrder;