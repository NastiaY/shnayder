import React, { useState } from 'react';
import cl from './Str.module.css';
import myAxi from '../../../service/interceptor';
import moment from 'moment';
import AddBlockBtn from '../buttons/AddBlockBtn';
import UnlockBtn from '../buttons/UnlockBtn';
import CellTable from '../other/cellTable';
import CellTableGrey from '../other/cellTableGrey';
import CellTableGreen from '../other/cellTableGreen';


const StrUsers = ({ user }) => {

    const [dataUser, setDataUser] = useState(user);

    let now = new Date();

    async function changedStatus(e, newStatus) {
        e.preventDefault();
        setDataUser({ ...dataUser, status: newStatus, updatedAt: now.toISOString() });
        const upParams = { status: newStatus, updatedAt: now.toISOString() };
        await myAxi.put(`/user?id=${user.id}`, upParams).catch(err => console.log(err));
    };


    return (
        <div className={cl.strAllOrder}>

            <CellTable style={{ width: '310px' }}>
                {user.surName + ' ' + user.firstName + ' ' + user.secondName}</CellTable>

            <CellTable style={{ width: '220px' }}>{user.email}</CellTable>


            <div style={{ width: '150px' }}>
                {dataUser.status ?
                    <CellTableGreen>Активен</CellTableGreen> :
                    <CellTableGrey>Заблокирован</CellTableGrey>}
            </div>

            <CellTable style={{ width: '120px' }}>
                {moment(dataUser.createdAt).format('D.MM.YYYY')}</CellTable>

            <CellTable style={{ width: '120px' }}>
                {moment(dataUser.updatedAt).format('D.MM.YYYY')}</CellTable>

            <div style={{ width: '190px' }}>
                {dataUser.status ?
                    <AddBlockBtn onClick={e => changedStatus(e, false)}>Заблокировать</AddBlockBtn> :
                    <UnlockBtn onClick={e => changedStatus(e, true)}>Разблокировать</UnlockBtn>}
            </div>

        </div>
    );
};

export default StrUsers;