import React from 'react';
import cl from './Btn.module.css';


const BtnOnlyOnline = ({ sort, onChange }) => {

    return (
        <div style={{ alignItems: 'center', display: 'flex' }}>
            <label className={cl.onlyOnline}>
                <input type='checkbox'
                    onChange={e => onChange(!sort)} />
                <div className={cl.ball}></div>
            </label>
        </div>
    );
};

export default BtnOnlyOnline;