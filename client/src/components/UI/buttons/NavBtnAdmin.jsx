import React from 'react';
import cl from './Btn.module.css';
import { NavLink } from 'react-router-dom';


const NavBtnAdmin = ({ children, ...props }) => {

    return (
        <NavLink {...props}
            className={({ isActive }) => isActive ? [cl.menuAdmin, cl.act].join(' ') : cl.menuAdmin}>{children}</NavLink>
    );
};

export default NavBtnAdmin;