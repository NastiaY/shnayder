import React from 'react';
import cl from './Btn.module.css';


const UnlockBtn = ({ children, ...props }) => {

    return (
        <form>
            <button {...props} className={cl.unlockBtn}>{children}</button>
        </form>
    );
};

export default UnlockBtn;