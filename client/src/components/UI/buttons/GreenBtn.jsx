import React from 'react';
import cl from './Btn.module.css';


const GreenBtn = ({ children, ...props }) => {

    return (
        <button {...props} className={cl.greenButton}>{children}</button>
    );
};

export default GreenBtn;