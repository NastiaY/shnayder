import React from 'react';
import cl from './Btn.module.css';
import { NavLink } from 'react-router-dom';


const MenuBtnBot = ({ children, ...props }) => {

    return (
        <NavLink {...props}
            className={({ isActive }) => isActive ? cl.MenuBot : [cl.MenuBot, cl.notAct].join(' ')}>{children}</NavLink>
    );
};

export default MenuBtnBot;