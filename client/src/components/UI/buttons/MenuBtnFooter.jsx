import React from 'react';
import cl from './Btn.module.css';
import { NavLink } from 'react-router-dom';


const MenuBtnFooter = ({ children, ...props }) => {

    return (
        <NavLink {...props} className={cl.footerMenuBtn}>{children}</NavLink>
    );
};

export default MenuBtnFooter;