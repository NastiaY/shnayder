import React from 'react';
import cl from './Btn.module.css';

const WhiteBtn = ({children, ...props}) => {

    return (
        <div style={{position: 'relative', width: 'max-content'}}>
            <button {...props} className={cl.whiteButton}>{children}</button>
            <span className={cl.lineTop}></span>
            <span className={cl.lineBottom}></span>
            <span className={cl.lineRight}></span>
            <span className={cl.lineLeft}></span>
        </div>
    );
};

export default WhiteBtn;