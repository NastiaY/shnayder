import React from 'react';
import cl from './Btn.module.css';
import { NavLink } from 'react-router-dom';


const GreenLink = ({ children, ...props }) => {

    return (
        <NavLink {...props} className={cl.greenLink}>{children}</NavLink>
    );
};

export default GreenLink;