import React from 'react';
import cl from './Btn.module.css';
import { NavLink } from 'react-router-dom';


const MenuBtnTop = ({ children, ...props }) => {

    return (
        <NavLink {...props}
            className={({ isActive }) => isActive ? cl.MenuTop : [cl.MenuTop, cl.notAct].join(' ')}>{children}</NavLink>
    );
};

export default MenuBtnTop;