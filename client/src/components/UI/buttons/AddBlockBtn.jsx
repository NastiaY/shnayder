import React from 'react';
import cl from './Btn.module.css';


const AddBlockBtn = ({ children, ...props }) => {

    return (
        <form>
            <button {...props} className={cl.addBlockBtn}>{children}</button>
        </form>
    );
};

export default AddBlockBtn;