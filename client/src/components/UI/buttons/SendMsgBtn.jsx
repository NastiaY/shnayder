import React from 'react';
import cl from './Btn.module.css';
import send from '../staticMedia/send_msg.svg';


const SendMsgBtn = ({ children, ...props }) => {

    return (
        <button {...props} className={cl.sendMsgBtn}><img src={send} alt='' /></button>
    );
};

export default SendMsgBtn;