import React from 'react';
import cl from './Other.module.css';
import { observer } from 'mobx-react-lite';
import BrifState from '../../../Pages/Other/BrifState';


const area = () => {

    return (
        <div className={cl.blockScale}>

            <span className={cl.scaleArea}
                style={{ left: `${BrifState.area * 9}%` }}>
                {BrifState.area == 10 ? 'от 300' : 'до ' + (BrifState.area * 30 + 20)} м2</span>

            <input type='range'
                min='0'
                max='10'
                value={BrifState.area}
                onChange={e => BrifState.setArea(e.target.value)} />

            <div className={cl.scaleText}>
                <span>до 20 м2</span>
                <span>более 300 м2</span>
            </div>

        </div>
    );
};

export default observer(area);