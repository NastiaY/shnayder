import React from 'react';
import cl from './Other.module.css';


const cellTableGreen = ({ children, ...props }) => {

    return (
        <span {...props} className={cl.cellTableGreen}>{children}</span>
    );
};

export default cellTableGreen;