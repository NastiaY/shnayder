import React from 'react';
import cl from './Other.module.css';


const smallText = ({ children, ...props }) => {

    return (
        <span {...props} className={cl.smallText}>{children}</span>
    );
};

export default smallText;