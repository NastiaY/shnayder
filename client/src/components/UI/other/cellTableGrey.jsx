import React from 'react';
import cl from './Other.module.css';


const cellTableGrey = ({ children, ...props }) => {

    return (
        <span {...props} className={cl.cellTableGrey}>{children}</span>
    );
};

export default cellTableGrey;