import React from 'react';
import cl from './Other.module.css';


const msgInput = ({ children, ...props }) => {

    return (
        <input {...props} className={cl.inputMsg}>{children}</input>
    );
};

export default msgInput;