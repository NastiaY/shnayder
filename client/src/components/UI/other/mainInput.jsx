import React from 'react';
import cl from './Other.module.css';


const mainInput = ({ children, ...props }) => {

    return (
        <input {...props} className={cl.input}>{children}</input>
    );
};

export default mainInput;