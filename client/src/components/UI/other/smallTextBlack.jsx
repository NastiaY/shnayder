import React from 'react';
import cl from './Other.module.css';


const smallTextBlack = ({ children, ...props }) => {

    return (
        <span {...props} className={cl.smallTextBlack}>{children}</span>
    );
};

export default smallTextBlack;