import React from 'react';
import cl from './Other.module.css';


const midlText = ({ children, ...props }) => {

    return (
        <span {...props} className={cl.midlText}>{children}</span>
    );
};

export default midlText;