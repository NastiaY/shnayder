import React from 'react';
import cl from './Other.module.css';


const cellTable = ({ children, ...props }) => {

    return (
        <span {...props} className={cl.cellTable}>{children}</span>
    );
};

export default cellTable;