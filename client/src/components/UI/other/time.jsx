import React from 'react';
import cl from './Other.module.css';
import { observer } from 'mobx-react-lite';
import BrifState from '../../../Pages/Other/BrifState';


const time = () => {

    return (
        <div className={cl.blockScale}>

            <div className={cl.timeChange} >

                {BrifState.area == 0 ? '' :
                    <span className={cl.lineChange}
                        style={{ width: `${BrifState.area * 10}%` }}></span>}

                <span className={cl.scaleTime}>
                    {BrifState.area == 10 ? 'от 12' : parseInt(BrifState.area) + 2} мес.</span>

                {BrifState.area == 10 ? '' :
                    <span className={cl.lineChange}
                        style={{ width: `${100 - (BrifState.area * 10)}%` }}></span>}

            </div>

            <div className={cl.scaleText}>
                <span>до 2-х мес.</span>
                <span>от 12 мес.</span>
            </div>

        </div>
    );
};

export default observer(time);