import React from 'react';
import cl from './Other.module.css';


const smallTextGreen = ({ children, ...props }) => {

    return (
        <span {...props} className={cl.smallTextGreen}>{children}</span>
    );
};

export default smallTextGreen;