import React from 'react';
import cl from './Components.module.css';
import Title from './UI/hats/Title';
import MidlText from './UI/other/midlText';

const StageWorking = ({stage, title, text}) => (
    <div className={cl.pointSt}>
        <span className={cl.numberSt}>{stage}</span>
        <Title variant={'green16'} wrapper={'shrink'} style={{width: '131px'}}>
            {title}
        </Title>
        <MidlText>{text}</MidlText>
    </div>
);

StageWorking.defaultProps = {
    text: 'Свяжитесь удобным способом с дизайнером, и Вы получите полную информацию об этапах дизайн-проекта, стоимости, сроках разработки и реализации'
};

export default StageWorking;