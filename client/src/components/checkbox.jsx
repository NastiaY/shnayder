import React from 'react';
import cl from './Components.module.css';
import {observer} from 'mobx-react-lite';

const Checkbox = ({
    children,
    checked,
    change,
    isDisabled = false,
    variant = 'normal',
    ...props
}) => {
    const variantClass = {
        'caps': cl.checkboxCapsLabel,
        'normal': cl.checkboxLabel
    };

    return (
        <div className={cl.choiceCheckbox}>
            <input type='checkbox'
                {...props}
                disabled={isDisabled}
                checked={checked}
                onChange={change}
            />
            <label className={variantClass[variant]}>
                {children}
            </label>
        </div>
    );
};

export default observer(Checkbox);