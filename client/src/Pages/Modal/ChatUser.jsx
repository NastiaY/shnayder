import React, { useState, useContext, useEffect } from 'react';
import cl from './Modal.module.css';
import close from '../../components/UI/staticMedia/closeModal.svg';
import StateModals from './StateModals';
import { observer } from 'mobx-react-lite';
import MsgInput from '../../components/UI/other/msgInput';
import SendMsgBtn from '../../components/UI/buttons/SendMsgBtn';
import { AuthContext } from '../../context';
import ChatMsgFrame from '../../components/ChatMsgFrame';
import myAxi from '../../service/interceptor';
import socket from '../../service/webSocket';


const ChatUser = () => {

    const [inpValue, setInpVal] = useState('');
    const { isUser, setIsAuth } = useContext(AuthContext);
    const [valMsg, setValMsg] = useState([]);
    const [onlineDis, setOnlineDis] = useState(true);

    var now = new Date();

    async function AddNewMsg(e) {
        e.preventDefault();
        const newMsg = {
            createdAt: now.toISOString(),
            idRoom: isUser.id,
            idUser: isUser.id,
            message: inpValue
        };
        await myAxi.post('/chat', newMsg);
        socket.emit('message', {
            ...newMsg,
            email: isUser.email,
            name: isUser.firstName + ' ' + isUser.secondName
        });
        setInpVal('');
    };

    useEffect(() => {
        socket.emit('userConnect', isUser.id);
        myAxi.get('/chat', { params: { id: isUser.id } })
            .then(res => setValMsg(res.data))
            .catch(err => {  
                setIsAuth(false);              
                console.log(err);
            });

        const handleUnload = () => {
            socket.emit('disconect', isUser.id);
        };
        window.addEventListener('beforeunload', handleUnload);
        return () => {
            window.removeEventListener('beforeunload', handleUnload);
            socket.emit('disconect', isUser.id);
        }
    }, []);

    useEffect(() => {
        socket.on('respOnlineUsers', data => {
            if (data.includes(2)) {
                setOnlineDis(false);
            } else {
                setOnlineDis(true);
            }
        });
    }, [onlineDis]);

    useEffect(() => {
        socket.on('response', data => {
            if (data.idRoom === isUser.id) {
                setValMsg([...valMsg, data]);
            };
        });
    }, [valMsg]);


    return (
        <div className={StateModals.chatUser ? cl.chatWind : [cl.chatWind, cl.notAct].join(' ')}>

            <img src={close} alt='закрыть' onClick={() => StateModals.setChatUser(false)} className={cl.imgClose} />

            {onlineDis &&
                <span className={cl.onlineDis}>Дарина Шнайдер:<br /><br /> - Меня сейчас нет онлайн, напишите и я обязательно отвечу.</span>}

            <div className={cl.messegesBlock}>

                <div className={cl.allMsgUser}>
                    {(valMsg.map((post) => {
                        return <ChatMsgFrame msg={post} key={post.createdAt} />
                    }))}
                </div>

            </div>


            <form onSubmit={AddNewMsg} className={cl.blockSendMsg}>

                <MsgInput type='text'
                    maxLength='500'
                    value={inpValue}
                    onChange={e => setInpVal(e.target.value)}
                    placeholder='Сообщение...' />

                <SendMsgBtn disabled={!inpValue} />

            </form>

        </div>
    );
};

export default observer(ChatUser);