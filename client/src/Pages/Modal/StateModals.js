import { makeAutoObservable } from 'mobx';


class StateModals {

    comeIn = false;
    regModal = false;
    successModal = false;
    addProject = false;
    chatUser = false;
    addOrderAdmin = false;
    passRecovery = false;

    fotoNewProject = [null, null, null, null, null, null];
    urlsFotoNewProject = [null, null, null, null, null, null];

    constructor() {
        makeAutoObservable(this);
    }

    setComeIn(data) {
        this.comeIn = data;
    };

    setRegModal(data) {
        this.regModal = data;
    };

    setSuccessModal(data) {
        this.successModal = data;
    };

    setAddProject(data) {
        this.addProject = data;
    };

    setChatUser(data) {
        this.chatUser = data;
    };

    setAddOrderAdmin(data) {
        this.addOrderAdmin = data;
    }
    setPassRecovery(data) {
        this.passRecovery = data;
    }

    setFotoNewProject(ind, foto) {
        const url = URL.createObjectURL(foto);
        this.fotoNewProject[ind] = foto;
        this.urlsFotoNewProject[ind] = url;
        
    };

    clearFotoNewProject() {
        this.fotoNewProject = [null, null, null, null, null, null];
        this.urlsFotoNewProject.map(item => URL.revokeObjectURL(item));
        this.urlsFotoNewProject = [null, null, null, null, null, null];
    };

    delFotoNewProject(ind) {
        URL.revokeObjectURL(this.urlsFotoNewProject[ind]);
        this.fotoNewProject[ind] = null;
        this.urlsFotoNewProject[ind] = null;
    };
};

export default new StateModals();