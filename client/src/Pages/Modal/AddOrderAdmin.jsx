import React, {useState} from 'react';
import {observer} from 'mobx-react-lite';
import cl from './Modal.module.css';
import Title from '../../components/UI/hats/Title';
import close from '../../components/UI/staticMedia/closeModal.svg';
import StateModals from './StateModals';
import MainInput from '../../components/UI/other/mainInput';
import GreenBtn from '../../components/UI/buttons/GreenBtn';
import axios from 'axios';
import AdminState from '../Admin/AdminState';

const AddOrderAdmin = () => {
    let now = new Date();

    const [order, setOrder] = useState({ fio: '', phone: '', email: '', statusUser: true });
    const [brif, setBrif] = useState({ type: '', style: '', color: '', area: 0, typeLight: '', light: [], typeMaterial: '', material: [], comment: '' });

    async function AddNewOrder(e) {
        e.preventDefault();
        if (window.confirm('Вы уверены что все заполнили и добавили?')) {
            try {
                const newOrder = {
                    createdAt: now.toISOString(), 
                    updatedAt: now.toISOString(),
                    brif: brif,
                    ...order
                }
                const res = await axios.post(process.env.REACT_APP_API_URL + '/api/orders', newOrder);
                AdminState.addNewOrder({...newOrder, id: res.data})
                setOrder({ fio: '', phone: '', email: '', statusUser: true });
                setBrif({ type: '', style: '', color: '', area: 0, typeLight: '', light: [], typeMaterial: '', material: [], comment: '' })
                StateModals.setAddOrderAdmin(false);
            } catch (err) {
                console.log(err);
            }
        };
    };

    return (
        <div className={StateModals.addOrderAdmin ? cl.fonModal : [cl.fonModal, cl.notAct].join(' ')}>
            <div className={cl.LoginWind}>
                <img src={close} alt='закрыть' onClick={() => StateModals.setAddOrderAdmin(false)} className={cl.imgClose} />
                <Title variant={'green16'} wrapper={'shrink'}>{'Добавить заявку'}</Title>
                <form onSubmit={AddNewOrder}>
                    <MainInput type='text'
                        maxLength='255'
                        value={order.fio}
                        onChange={e => setOrder({ ...order, fio: e.target.value })}
                        placeholder='* ФИО или название компании' />
                    <MainInput type='number'
                        maxLength='20'
                        value={order.phone}
                        onChange={e => setOrder({ ...order, phone: e.target.value })}
                        placeholder='* Телефон' />
                    <MainInput type='email'
                        maxLength='255'
                        value={order.email}
                        onChange={e => setOrder({ ...order, email: e.target.value })}
                        placeholder='* Email' />
                    <div className={cl.statusUserOrderAdmin}>
                        <div>
                            <input type='radio'
                                checked={order.statusUser === true}
                                onChange={() => setOrder({ ...order, statusUser: true })} />
                            <label>{'в работе'}</label>
                        </div>
                        <div>
                            <input type='radio'
                                checked={order.statusUser === null}
                                onChange={() => setOrder({ ...order, statusUser: null })} />
                            <label>{'думает'}</label>
                        </div>
                        <div>
                            <input type='radio'
                                checked={order.statusUser === false}
                                onChange={() => setOrder({ ...order, statusUser: false })} />
                            <label>{'отказ'}</label>
                        </div>
                    </div>
                    <div className={cl.blockBrifOrderAdmin}>
                        <div className={cl.halfBlockBrif}>
                            <MainInput type='number'
                                maxLength='10'
                                value={brif.area}
                                onChange={e => setBrif({ ...brif, area: e.target.value })}
                                placeholder='* Площадь в кв.м.' />
                            <MainInput type='text'
                                maxLength='50'
                                value={brif.type}
                                onChange={e => setBrif({ ...brif, type: e.target.value })}
                                placeholder='Тип' />
                            <MainInput type='text'
                                maxLength='50'
                                value={brif.style}
                                onChange={e => setBrif({ ...brif, style: e.target.value })}
                                placeholder='Стиль' />
                            <MainInput type='text'
                                maxLength='50'
                                value={brif.color}
                                onChange={e => setBrif({ ...brif, color: e.target.value })}
                                placeholder='Цветовая гамма' />
                        </div>
                        <div className={cl.halfBlockBrif}>
                            <MainInput type='text'
                                maxLength='50'
                                value={brif.typeLight}
                                onChange={e => setBrif({ ...brif, typeLight: e.target.value })}
                                placeholder='Тип освещения' />
                            <MainInput type='text'
                                maxLength='50'
                                value={brif.light}
                                onChange={e => setBrif({ ...brif, light: [e.target.value] })}
                                placeholder='Приборы освещения' />
                            <MainInput type='text'
                                maxLength='50'
                                value={brif.typeMaterial}
                                onChange={e => setBrif({ ...brif, typeMaterial: e.target.value })}
                                placeholder='Тип материалов' />
                            <MainInput type='text'
                                maxLength='50'
                                value={brif.material}
                                onChange={e => setBrif({ ...brif, material: [e.target.value] })}
                                placeholder='Материалы' />
                        </div>
                    </div>
                    <textarea className={cl.commentOrder}
                        maxLength='255'
                        value={brif.comment}
                        onChange={e => setBrif({ ...brif, comment: e.target.value })}
                        placeholder='Комментарий (максимум 255 символов)' />
                    <GreenBtn>{'добавить заявку'}</GreenBtn>
                </form>
            </div>
        </div>
    );
};

export default observer(AddOrderAdmin);