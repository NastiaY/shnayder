import React, {useState} from 'react';
import {observer} from 'mobx-react-lite';
import cl from './Modal.module.css';
import axios from 'axios';
import Title from '../../components/UI/hats/Title';
import MainInput from '../../components/UI/other/mainInput';
import GreenBtn from '../../components/UI/buttons/GreenBtn';
import close from '../../components/UI/staticMedia/closeModal.svg';
import StateModals from './StateModals';
import SmallText from '../../components/UI/other/smallText';
import MainH4 from '../../components/UI/hats/MainH4';
import back from '../../components/UI/staticMedia/back.svg';

const PassRecovery = () => {
    const [value, setValue] = useState({ Log: '', AlertL: '', success: '' });

    const NotCopy = (e) => {
        e.preventDefault();
    };

    async function Login(e) {
        e.preventDefault();
        await axios.post(process.env.REACT_APP_API_URL + '/api/rec', { email: value.Log }, { withCredentials: true })
            .then(res => {
                setValue({ ...value, AlertL: '', success: res.data });
                setTimeout(() => {
                    setValue({ ...value, Log: '', success: '' });
                    StateModals.setPassRecovery(false);
                }, 5000);
            })
            .catch(err => {
                console.log(err.response.status);
                if (err.response.status === 409) {
                    setValue({ ...value, AlertL: err.response.data });
                } else {
                    console.log(err);
                }
            })
    };

    const comeBack = (e) => {
        e.preventDefault();
        StateModals.setComeIn(true);
        StateModals.setPassRecovery(false);
    };

    return (
        <div className={StateModals.passRecovery ? cl.fonModal : [cl.fonModal, cl.notAct].join(' ')}>
            <div className={cl.LoginWind}>
                <img src={close} alt='закрыть' onClick={() => StateModals.setPassRecovery(false)} className={cl.imgClose} />
                <img src={back} alt='назад' onClick={e => comeBack(e)} className={cl.imgBack}/>
                <Title variant={'green16'} wrapper={'shrink'}>
                    {'Восстановление доступа'}
                </Title>
                <form onSubmit={Login}>
                    <div>
                        {value.success ?
                            <MainH4>{value.success}</MainH4> :
                            <MainInput type='email'
                                maxLength='255'
                                value={value.Log}
                                onCopy={NotCopy}
                                onChange={e => setValue({ ...value, Log: e.target.value })}
                                placeholder='Ваш email'
                                style={value.AlertL ? { boxShadow: 'inset 0px -1px 0px #E93E3E' } : {}} />}
                        {value.AlertL &&
                            <span className={cl.notvalid}>{value.AlertL}</span>}
                    </div>
                    <SmallText>
                        {'На вашу почту будет отправлена ссылка для восстановления доступа.'}
                    </SmallText>
                    <GreenBtn disabled={(!value.Log)} type='submit'>{'Отправить'}</GreenBtn>
                </form>
            </div>
        </div>
    );
};

export default observer(PassRecovery);