import React from 'react';
import {observer} from 'mobx-react-lite';
import cl from './Modal.module.css';
import Title from '../../components/UI/hats/Title';
import close from '../../components/UI/staticMedia/closeModal.svg';
import StateModals from './StateModals';

const Success = () => (
    <div className={StateModals.successModal ? cl.fonModal : [cl.fonModal, cl.notAct].join(' ')}>
        <div className={cl.LoginWind}>
            <img src={close} alt='закрыть' onClick={() => StateModals.setSuccessModal(false)} className={cl.imgClose}/>
            <Title variant={'green16'} wrapper={'shrink'}>
                {'Ваша заявка успешно отправлена'}
            </Title>
            <span>{'Дизайнер свяжется с Вами в ближайшее время.'}</span>
        </div>
    </div>
);

export default observer(Success);