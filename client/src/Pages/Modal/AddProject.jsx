import React, {useState} from 'react';
import {observer} from 'mobx-react-lite';
import cl from './Modal.module.css';
import Title from '../../components/UI/hats/Title';
import close from '../../components/UI/staticMedia/closeModal.svg';
import StateModals from './StateModals';
import MainInput from '../../components/UI/other/mainInput';
import del from '../../components/UI/staticMedia/del.svg';
import addFoto from '../../components/UI/staticMedia/addFoto.svg';
import GreenBtn from '../../components/UI/buttons/GreenBtn';
import servService from '../../service/servService';

const AddProject = () => {
    let now = new Date();

    const [project, setProject] = useState({ title: '', area: 0, city: '', description: ['', ''], createdAt: now.toISOString(), statusGallery: true, type: true, style: '', color: '' });
    const descriptionVisual = ['самое большое', '1160 х 547', 'аватар (653 х 547)', '477 х 547', '653 х 547', 'план помещения']
    const [load, setLoad] = useState(false);

    async function AddNewProject(e) {
        e.preventDefault();
        if (window.confirm('Вы уверены что все заполнили и добавили?')) {
            try {
                setLoad(true);
                await servService.addProject(project);
                setProject({ title: '', area: 0, city: '', description: ['', ''], createdAt: now.toISOString(), statusGallery: true, type: true, style: '', color: '' });
                StateModals.clearFotoNewProject();
                StateModals.setAddProject(false);
                setLoad(false);
            } catch (err) {
                console.log(err);
            }
        };
    };

    return (
        load ? (
            <div className={StateModals.addProject ? cl.fonModal : [cl.fonModal, cl.notAct].join(' ')}>
                <div className={cl.addProjectWind}>
                    <Title variant={'green16'} wrapper={'shrink'}>
                        {'Идет загрузка проекта, подождите...'}
                    </Title>
                </div>
            </div>
            ) : (
            <div className={StateModals.addProject ? cl.fonModal : [cl.fonModal, cl.notAct].join(' ')}>
                <div className={cl.addProjectWind}>
                    <img src={close} alt='закрыть' onClick={() => StateModals.setAddProject(false)} className={cl.imgClose} />
                    <Title variant={'green16'} wrapper={'shrink'}>{'Добавить проект'}</Title>
                    <form onSubmit={AddNewProject}>
                        <div className={cl.dataNewProject}>
                            <MainInput type='text'
                                maxLength='255'
                                value={project.title}
                                onChange={e => setProject({ ...project, title: e.target.value })}
                                placeholder='* Название проекта (Офис, Квартира-студия и т.д.)' />
                            <MainInput type='number'
                                maxLength='5'
                                value={project.area}
                                onChange={e => setProject({ ...project, area: e.target.value })}
                                placeholder='* Площадь (в кв.м., только число)' />
                            <MainInput type='text'
                                maxLength='255'
                                value={project.city}
                                onChange={e => setProject({ ...project, city: e.target.value })}
                                placeholder='* Город (с большой буквы и без г.)' />
                            <MainInput type='text'
                                maxLength='30'
                                value={project.createdAt}
                                onChange={e => setProject({ ...project, createdAt: e.target.value })}
                                placeholder='* Дата (год-месяц-день, время в 24 часовом формате)' />
                            <div className={cl.checkProjectStyle}>
                                <div>
                                    <input type='checkbox'
                                        checked={project.statusGallery}
                                        onChange={() => setProject({ ...project, statusGallery: !project.statusGallery })} />
                                    <label>{'В Галерею'}</label>
                                </div>
                                <div>
                                    <input type='checkbox'
                                        checked={project.type}
                                        onChange={() => setProject({ ...project, type: !project.type })} />
                                    <label>{'Жилой - вкл, Коммерческий - выкл'}</label>
                                </div>
                            </div>
                            <MainInput type='text'
                                maxLength='50'
                                value={project.style}
                                onChange={e => setProject({ ...project, style: e.target.value })}
                                placeholder='* Стиль' />
                            <MainInput type='text'
                                maxLength='50'
                                value={project.color}
                                onChange={e => setProject({ ...project, color: e.target.value })}
                                placeholder='* Цветовая гамма' />
                            <textarea className={cl.descriptionProject}
                                maxLength='400'
                                value={project.description[0]}
                                onChange={e => setProject({ ...project, description: [e.target.value, project.description[1]] })}
                                placeholder='* Описание верхнее' />

                            <textarea className={cl.descriptionProject}
                                maxLength='500'
                                value={project.description[1]}
                                onChange={e => setProject({ ...project, description: [project.description[0], e.target.value] })}
                                placeholder='* Описание нижнее' />
                        </div>
                        <div className={cl.dataNewProject}>
                            <span>{'* Добавьте фотографии, визуализации или планировку. Только в формате png!'}</span>
                            <div className={cl.gridVisual}>
                                {StateModals.fotoNewProject.map((item, ind) => {
                                    return (
                                        <div key={`addprojectfoto-${ind}`} >
                                            <label htmlFor={`order-${ind}`}>
                                                <img src={StateModals.urlsFotoNewProject[ind] ? StateModals.urlsFotoNewProject[ind] : addFoto} alt='' className={cl.imageNewProject} />
                                                {item && <img src={del}
                                                    alt='удалить'
                                                    className={cl.delVisual}
                                                    onClick={e => (e.preventDefault(), StateModals.delFotoNewProject(ind))} />}
                                                <p>{descriptionVisual[ind]}</p>
                                            </label>
                                            {!item && <input type='file'
                                                accept='.png'
                                                id={`order-${ind}`}
                                                onChange={e => StateModals.setFotoNewProject(ind, e.target.files[0])} />}
                                        </div>
                                    )
                                })}
                            </div>
                            <GreenBtn style={{ marginTop: 'auto', marginLeft: 'auto' }}>
                                {'добавить проект'}
                            </GreenBtn>
                        </div>
                    </form>
                </div>
            </div>
        )
    );
};

export default observer(AddProject);