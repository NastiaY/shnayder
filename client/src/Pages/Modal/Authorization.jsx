import React, {useState, useContext} from 'react';
import {observer} from 'mobx-react-lite';
import cn from 'classname';
import cl from './Modal.module.css';
import {AuthContext} from '../../context/index';
import axios from 'axios';
import myAxi from '../../service/interceptor';
import Title from '../../components/UI/hats/Title';
import MainInput from '../../components/UI/other/mainInput';
import GreenBtn from '../../components/UI/buttons/GreenBtn';
import close from '../../components/UI/staticMedia/closeModal.svg';
import StateModals from './StateModals';
import openEye from '../../components/UI/staticMedia/openEye.svg';
import closeEye from '../../components/UI/staticMedia/closeEye.svg';
import SmallText from '../../components/UI/other/smallText';
import WhiteBtn from '../../components/UI/buttons/WhiteBtn';

const Authorization = () => {
    const {setIsUser, setIsAuth} = useContext(AuthContext);

    const [value, setValue] = useState({Log: '', Pas: '', AlertP: '', AlertL: ''});
    const [visible, setVisible] = useState(false);

    const NotCopy = event => {
        event.preventDefault();
    };

    async function Login(e) {
        e.preventDefault();
        await axios.post(process.env.REACT_APP_API_URL + '/api/login', {Login: value.Log.toLowerCase(), Pass: value.Pas}, {withCredentials: true})
            .then(res => {
                myAxi.get(`/user/${res.data.id}`)
                .then(result => {
                    setIsUser({...result.data[0], ...res.data});
                    setIsAuth(true);
                    StateModals.setComeIn(false);
                    setValue({Log: '', Pas: '', AlertP: '', AlertL: ''});
                })
                .catch(err => {
                    console.log(err);
                })          
            })
            .catch(err => {
                if (err.response?.status === 402) {
                    setValue({...value, AlertP: 'Неверный пароль'});
                    setIsAuth(false);
                } else if (err.response?.status === 401) {
                    setValue({...value, AlertL: 'Неверный email'});
                    setIsAuth(false);
                } else {
                    setValue({...value, AlertP: 'Заблокирован'});
                    setIsAuth(false);
                }
            })
    };

    const registr = (e) => {
        e.preventDefault();
        StateModals.setComeIn(false);
        StateModals.setRegModal(true);
    };

    const recovery = () => {
        StateModals.setComeIn(false);
        StateModals.setPassRecovery(true);
    };

    return (
        <div className={StateModals.comeIn ? cl.fonModal : cn(cl.fonModal, cl.notAct)}>
            <div className={cl.LoginWind}>
                <img src={close} alt='закрыть' onClick={() => StateModals.setComeIn(false)} className={cl.imgClose}/>
                <Title variant={'green16'} wrapper={'shrink'}>{'Добро пожаловать'}</Title>
                <form onSubmit={Login}>
                    <div>
                        <MainInput type='email' 
                            maxLength='255'
                            value={value.Log}
                            onCopy={NotCopy}
                            onChange={e => setValue({...value, Log: e.target.value, AlertL: ''})}
                            placeholder='Ваш email'
                            style={value.AlertL ? { boxShadow: 'inset 0px -1px 0px #E93E3E' } : {}} />
                        {value.AlertL &&
                            <span className={cl.notvalid}>{'Аккаунт с таким email не зарегистрирован'}</span>}
                    </div>
                    <div className={cl.password}>
                        <MainInput type={visible ? 'text' : 'password'} 
                            maxLength='30'
                            value={value.Pas}
                            onCopy={NotCopy}
                            onChange={e => setValue({...value, Pas: e.target.value.replace(/[^\w\.]/g, ''), AlertP: ''})}
                            placeholder='Пароль'
                            style={value.AlertP === 'Неверный пароль' ? { boxShadow: 'inset 0px -1px 0px #E93E3E' } : {}} />
                        <img src={visible ? openEye : closeEye} alt='сделать видимым' style={{cursor: 'pointer'}}
                            onClick={() => setVisible(!visible)}/>
                        {value.AlertP === 'Неверный пароль' &&
                            <span className={cl.notvalid}>{'Неверный пароль'}</span>}
                        {value.AlertP === 'Заблокирован' &&
                            <span className={cl.notvalid}>{'Ваш аккаунт заблокирован'}</span>}
                    </div>
                    <div className={cl.registration}>
                        <GreenBtn disabled={(!value.Log, !value.Pas)} type='submit'>{'Войти'}</GreenBtn>
                        <WhiteBtn onClick={e => registr(e)} type='button'>{'Создать аккаунт'}</WhiteBtn>
                        <SmallText style={{cursor: 'pointer'}} onClick={() => recovery()}>{'Забыли пароль?'}</SmallText>
                    </div>
                </form>
            </div>
        </div>
    );
};

export default observer(Authorization);