import React, {useContext, useState} from "react";
import cl from './Modal.module.css';
import StateModals from './StateModals';
import Title from '../../components/UI/hats/Title';
import {observer} from "mobx-react-lite";
import close from '../../components/UI/staticMedia/closeModal.svg';
import GreenBtn from '../../components/UI/buttons/GreenBtn';
import axios from 'axios';
import MainInput from '../../components/UI/other/mainInput';
import SmallText from '../../components/UI/other/smallText';
import {NavLink} from 'react-router-dom';
import {AuthContext} from "../../context";
import openEye from '../../components/UI/staticMedia/openEye.svg';
import closeEye from '../../components/UI/staticMedia/closeEye.svg';
import Checkbox from '../../components/Checkbox';
import back from '../../components/UI/staticMedia/back.svg';

const Registration = () => {
    const {setIsAuth, setIsUser} = useContext(AuthContext);

    const [valueAcc, setValueAcc] = useState({Login: '', Pass: ''});
    const [valueUser, setValueUser] = useState({firstName: '', surName: '', secondName: '', phone: ''});
    const [valAlert, setValAlert] = useState(false);
    const [agree, setAgree] = useState({list: true, personeInfo: false});
    const [visible, setVisible] = useState(false);

    const NotCopy = event => {
        event.preventDefault();
    };

    let now = new Date();

    async function Reg(e) {
        e.preventDefault();
        const NewUser = {
            createdAt: now.toISOString(),
            updatedAt: now.toISOString(),
            email: valueAcc.Login.toLowerCase(),
            ...valueUser
        };
        await axios.post(process.env.REACT_APP_API_URL + '/api/reg', valueAcc, { withCredentials: true })
            .then(res => {
                axios.post(process.env.REACT_APP_API_URL + '/api/user', NewUser, { withCredentials: true })
                    .then(async(res) => {
                        await axios.post(process.env.REACT_APP_API_URL + '/api/login', valueAcc, { withCredentials: true })
                        .then(result => {
                            setIsUser({...result.data, email: valueAcc.Login.toLowerCase(), firstName: valueUser.firstName, surName: valueUser.surName, secondName: valueUser.secondName, phone: valueUser.phone});
                            setIsAuth(true);
                        });
                        setValueAcc({ Login: '', Pass: '' });
                        setValueUser({ firstName: '', surName: '', secondName: '', phone: '' });
                        setValAlert(false);
                        setAgree({ list: true, personeInfo: false });
                        StateModals.setRegModal(false);
                        if (agree.list) {
                            await axios.post(process.env.REACT_APP_API_URL + '/api/mail', {email: valueAcc.Login}, { withCredentials: true })
                        };
                    })
                    .catch(err => {
                        console.log(err);
                    })
            })
            .catch(err => {
                setValAlert(true);
            })
    };

    const comeBack = (e) => {
        e.preventDefault();
        StateModals.setComeIn(true);
        StateModals.setRegModal(false);
    };

    return (
        <div className={StateModals.regModal ? cl.fonModal : [cl.fonModal, cl.notAct].join(' ')}>
            <div className={cl.LoginWind}>
                <img src={close} alt='закрыть' onClick={() => StateModals.setRegModal(false)} className={cl.imgClose}/>
                <img src={back} alt='назад' onClick={e => comeBack(e)} className={cl.imgBack}/>
                <Title variant={'green16'} wrapper={'shrink'}>{'Заполните форму'}</Title>
                <form onSubmit={Reg}>
                    <div>
                        <MainInput type='email'
                            maxLength='255'
                            value={valueAcc.Login}
                            onCopy={NotCopy}
                            onChange={e => setValueAcc({...valueAcc, Login: e.target.value})}
                            placeholder='* Ваш email'
                            style={valAlert ? { boxShadow: 'inset 0px -1px 0px #E93E3E' } : {}} />

                        {valAlert &&
                            <span className={cl.notvalid}>{'Этот email уже зарегистрирован'}</span>}
                    </div>
                    <div className={cl.password}>
                        <MainInput type={visible ? 'text' : 'password'}
                            maxLength='30'
                            minLength='8'
                            value={valueAcc.Pass}
                            onCopy={NotCopy}
                            onChange={e => setValueAcc({...valueAcc, Pass: e.target.value.replace(/[^\w\.]/g, '')})}
                            placeholder='* Пароль' />

                        <img src={visible ? openEye : closeEye} alt='сделать видимым'
                            onClick={() => setVisible(!visible)}/>
                    </div>
                    <MainInput type='text'
                        maxLength='255'
                        value={valueUser.firstName}
                        onChange={e => setValueUser({ ...valueUser, firstName: e.target.value.replace(/[^A-za-zА-яа-я_-\s]/g, '') })}
                        placeholder='* Имя' />
                    <MainInput type='text'
                        maxLength='255'
                        value={valueUser.surName}
                        onChange={e => setValueUser({...valueUser, surName: e.target.value.replace(/[^A-za-zА-яа-я_-\s]/g, '')})}
                        placeholder='* Фамилия' />
                    <MainInput type='text'
                        maxLength='255'
                        value={valueUser.secondName}
                        onChange={e => setValueUser({...valueUser, secondName: e.target.value.replace(/[^A-za-zА-яа-я_-\s]/g, '')})}
                        placeholder='* Отчество' />
                    <MainInput type='text'
                        maxLength='50'
                        value={valueUser.phone}
                        onChange={e => setValueUser({...valueUser, phone: e.target.value.replace(/[^-+.e\d]/g, '')})}
                        placeholder='* Телефон' />
                    <div className={cl.agreeStyle}>
                        <Checkbox
                            checked={agree.list}
                            change={() => setAgree({...agree, list: !agree.list})}
                        >
                            {'Подписаться на рассылку'}
                        </Checkbox>
                        <Checkbox
                            checked={agree.personeInfo}
                            change={() => setAgree({...agree, personeInfo: !agree.personeInfo})}
                        >
                            {'Согласен на обработку персональных данных'}
                        </Checkbox>
                    </div>
                    <div className={cl.registration}>
                        <GreenBtn
                            disabled={(!Object.values({...valueUser, ...valueAcc}).every(Boolean) || !agree.personeInfo)}
                            type='submit'>{'отправить'}
                        </GreenBtn>
                        <NavLink to='/policy' style={{ textDecoration: 'none' }}>
                            <SmallText>{'Политика конфеденциальности'}</SmallText>
                        </NavLink>
                    </div>
                </form>
            </div>
        </div>
    );
};

export default observer(Registration);