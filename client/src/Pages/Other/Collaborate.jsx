import React, { useEffect, useState } from 'react';
import cl from './OtherPages.module.css';
import MainH2 from '../../components/UI/hats/MainH2';
import GreenBtn from '../../components/UI/buttons/GreenBtn';
import MainH3 from '../../components/UI/hats/MainH3';
import cloud from '../../service/cloud';


const Collaborate = () => {

    const [urls, setUrls] = useState([]);
    const [urlStatic, setUrlStatic] = useState('');

    useEffect(() => {
        cloud.getUrls('partners').then(res => setUrls(res));
        cloud.getOneUrl('static', 'partners.png').then(res => setUrlStatic(res))
    }, []);

    return (
        <div className={cl.fonContact}>

            <img src={urlStatic} alt='' className={cl.imgContact} />

            <div className={cl.cotentContact}>

                <div className={cl.titleBrif}>

                    <div className={cl.hatCost}>
                        <MainH3>ДАВАЙТЕ РАБОТАТЬ! </MainH3>
                        <MainH2>сОТРУДНИЧЕСТВО</MainH2>
                    </div >

                    <form><GreenBtn onClick={e => (cloud.getDoc(e, 'contract_partner.docx'))}>скачать договор</GreenBtn></form>

                </div>

                <div className={cl.textCollab}>

                    <span style={{ display: 'block', width: '577px' }}>
                        Если вы хотите обратиться с предложением о сотрудничестве или по другим
                        вопросам, отправьте нам, пожалуйста, письмо на почту:&nbsp;

                        <a href='mailto:shnaiderdarina@ya.ru'
                            target='_blank'
                            rel='noopener noreferrer'>shnaiderdarina@ya.ru</a>.

                        Присылайте предложения, содержащие полезную и доступную информацию
                        с примерами работ и ценами. Не присылайте шаблонную презентацию и
                        ссылку на ваш сайт.<br /><br />
                        Рекомендация: обратиться в личном письме. Опишите, какие выгоды
                        несет ваше предложение для моих клиентов и какие ваши конкурентные
                        преимущества.</span>

                    <div className={cl.partners}>
                        {urls.map(url => (
                            <img src={url} alt='' key={url} className={cl.brandsCollab} />))}
                    </div>

                </div>

            </div>

        </div>
    );
};

export default Collaborate;