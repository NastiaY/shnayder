import React, { useEffect, useState } from 'react';
import cl from './OtherPages.module.css';
import MainH3 from '../../components/UI/hats/MainH3';
import MainH2 from '../../components/UI/hats/MainH2';
import cloud from '../../service/cloud';
import { NavLink } from 'react-router-dom';
import MainInput from '../../components/UI/other/mainInput';
import GreenBtn from '../../components/UI/buttons/GreenBtn';
import SmallText from '../../components/UI/other/smallText';
import StateModals from '../Modal/StateModals';
import axios from 'axios';


const Contact = () => {

    let now = new Date();

    const [valCustomer, setValCustomer] = useState({ fio: '', phone: '', email: '', createdAt: now.toISOString(), updatedAt: now.toISOString() });
    const [url, setUrl] = useState('')

    useEffect(() => {
        cloud.getOneUrl('static', 'contacts.png').then(res => setUrl(res));
    }, [])

    async function sendCalc(e) {
        e.preventDefault();
        StateModals.setSuccessModal(true);
        try {
            const newOrder = { ...valCustomer, brif: {comment: 'заказал звонок'} }
            await axios.post(process.env.REACT_APP_API_URL + '/api/orders', newOrder);
            setValCustomer({ fio: '', phone: '', email: '', createdAt: now.toISOString(), updatedAt: now.toISOString() });
            setTimeout(() => StateModals.setSuccessModal(false), 3000);
        } catch (err) {
            console.log(err);
        };
    };

    return (
        <div className={cl.fonContact}>

            <img src={url} alt='' className={cl.imgContact} />

            <div className={cl.cotentContact}>

                <div className={cl.hatCost}>
                    <MainH3>ДАВАЙТЕ РАБОТАТЬ! </MainH3>
                    <MainH2>КОНТАКТЫ</MainH2>
                </div>

                <div className={cl.dataContact}>

                    <div className={cl.dataUserContact}>

                        <div className={cl.strContact}>
                            <span className={cl.boldTextContact}>Телефон</span>
                            <span>+7 982 780 55 67</span>
                        </div>

                        <div className={cl.strContact}>
                            <span className={cl.boldTextContact}>Адрес</span>
                            <span>г. Тюмень, ул. Республики, 211</span>
                        </div>

                        <div className={cl.strContact}>
                            <span className={cl.boldTextContact}>Email</span>
                            <span>shnaiderdarina@ya.ru</span>
                        </div>

                        <div className={cl.strContact}>
                            <span className={cl.boldTextContact}>График</span>
                            <span>понедельник - суббота,<br/>
                                по предварительной записи
                            </span>
                        </div>

                    </div>

                    <form onSubmit={e => sendCalc(e)} className={cl.dataUserContact}>


                        <MainInput type='text'
                            maxLength='255'
                            value={valCustomer.fio}
                            onChange={e => setValCustomer({ ...valCustomer, fio: e.target.value.replace(/[^A-za-zА-яа-я_-\s]/g, '') })}
                            placeholder='* ФИО (Иванов Иван Иванович)' />

                        <MainInput type='text'
                            maxLength='50'
                            value={valCustomer.phone}
                            onChange={e => setValCustomer({ ...valCustomer, phone: e.target.value.replace(/[^-+.e\d]/g, '') })}
                            placeholder='* Ваш телефон' />

                        <MainInput type='email'
                            maxLength='255'
                            value={valCustomer.email}
                            onChange={e => setValCustomer({ ...valCustomer, email: e.target.value })}
                            placeholder='* E-mail' />

                        <div className={cl.btnsCollab}>

                            <GreenBtn
                                disabled={(!valCustomer.fio + !valCustomer.phone + !valCustomer.email)}>
                                заказать звонок</GreenBtn>

                            <NavLink to='/policy' style={{ textDecoration: 'none' }}><SmallText >Политика конфеденциальности</SmallText></NavLink>

                        </div>

                    </form>

                </div>

            </div>

        </div>
    );
};

export default Contact;