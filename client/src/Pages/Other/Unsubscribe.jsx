import React, { useEffect, useState } from 'react';
import cl from './OtherPages.module.css';
import MainH3black from '../../components/UI/hats/MainH3black';


const Unsubscribe = () => {

    return (
        <div className={cl.fonBrif}>
            <MainH3black>Вы успешно отписались от рассылки.</MainH3black>
        </div>
    );
};

export default Unsubscribe;