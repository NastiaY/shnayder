import { makeAutoObservable } from 'mobx';


class BrifState {

    area = 5;
    userFoto = [null, null, null, null, null, null];
    urlsFotoUser = [null, null, null, null, null, null];
    urlsType = [];
    urlsColor = [];
    urlsStyle = [];
    wishList = {
        type: 'квартира',
        style: 'минимализм',
        color: 'спокойная',
        area: this.area * 30 + 20,
        typeLight: 'верхний свет',
        light: [],
        typeMaterial: 'натуральные',
        material: [],
        comment: ''
    };

    constructor() {
        makeAutoObservable(this);
    };

    setArea(data) {
        this.area = data;
    };

    setUserFoto(ind, foto) {
        this.userFoto[ind] = foto;
        const url = URL.createObjectURL(foto);
        this.urlsFotoUser[ind] = url;
    };

    delUserFoto(ind) {
        URL.revokeObjectURL(this.urlsFotoUser[ind]);
        this.urlsFotoUser[ind] = null;
        this.userFoto[ind] = null;
    };

    setWishList(data) {
        this.wishList = { ...this.wishList, ...data };
    };

    setUrlsType(data) {
        this.urlsType = data;
    };

    setUrlsColor(data) {
        this.urlsColor = data;
    };

    setUrlsStyle(data) {
        this.urlsStyle = data;
    };
    setWishListLight(data) {
        if (this.wishList.light.includes(data)) {
            let filt = this.wishList.light.filter(el => el !== data);
            this.wishList = { ...this.wishList, light: filt };
        } else {
            this.wishList.light.push(data);
        };
    };

    setWishListMaterial(data) {
        if (this.wishList.material.includes(data)) {
            let filt = this.wishList.material.filter(el => el !== data);
            this.wishList = { ...this.wishList, material: filt };
        } else {
            this.wishList.material.push(data);
        };
    };

    newBrifState() {
        this.area = 5;
        this.userFoto = [null, null, null, null, null, null];
        for (let i of this.urlsFotoUser) {
            URL.revokeObjectURL(i);
        }
        this.urlsFotoUser = [null, null, null, null, null, null];
        this.wishList = {
            type: 'квартира',
            style: 'минимализм',
            color: 'спокойная',
            area: this.area * 30 + 20,
            typeLight: 'верхний свет',
            light: [],
            typeMaterial: 'натуральные',
            material: [],
            comment: ''
        };
    };
    
};

export default new BrifState();