import React, {useContext, useEffect, useState} from 'react';
import cl from './OtherPages.module.css';
import {useNavigate, useParams} from 'react-router-dom';
import servService from '../../service/servService';
import MainH1 from '../../components/UI/hats/MainH1';
import SmallTextGreen from '../../components/UI/other/smallTextGreen';
import moment from 'moment';
import GreenBtn from '../../components/UI/buttons/GreenBtn';
import {AuthContext} from '../../context';
import myAxi from '../../service/interceptor';
import ProjectsState from './ProjectsState';
import cloud from '../../service/cloud';
import Checkbox from '../../components/Checkbox';

const SelectProject = () => {
    const params = useParams();
    const [project, setProject] = useState();
    const {isUser} = useContext(AuthContext);

    const router = useNavigate();

    useEffect(() => {
        servService.getOneProject(params.id).then(res => setProject(res));
    }, []);

    const ChangeStatus = () => {
        setProject({...project, statusGallery: !project.statusGallery});
        myAxi.put(`/projects?id=${project.id}`, {statusGallery: !project.statusGallery})
            .then(res => {
                if (!project.statusGallery) {
                    ProjectsState.setProjectGallery(project);
                } else {
                    ProjectsState.delProject(project);
                };
            })
            .catch(err => console.log(err));
    };

    const delProject = (e) => {
        e.preventDefault();
        if (window.confirm('Вы действительно хотите удалить проект?')) {
            ProjectsState.delProject(project);
            myAxi.delete(`/projects?id=${project.id}`)
                .then(res => {
                    cloud.deleteChildDirectory('projects', `${project.id}`);
                    router('/project');
                })
                .catch(err => console.log(err));
        };
    };

    return (
        <div style={{ width: '100%' }}>
            {project ?
                <div className={cl.fonSelectProject} >
                    <div className={cl.titleFotoPr}>
                        <img src={project.urls[0]} alt='' />
                        <span>{project.title}, {project.city}</span>
                    </div>
                    <div className={cl.contentProject}>
                        <div className={cl.terms}>
                            <div className={cl.titleBrif}>
                                <MainH1>{project.title}</MainH1>
                                {isUser.role &&
                                    <div className={cl.btnsCollab}>
                                        <Checkbox
                                            checked={project.statusGallery}
                                            change={() => ChangeStatus()}
                                        >
                                            {'В Галерею'}
                                        </Checkbox>
                                        <form>
                                            <GreenBtn onClick={e => delProject(e)}>
                                                {'удалить проект'}
                                            </GreenBtn>
                                        </form>
                                    </div>}
                            </div>
                            <div className={cl.titleBrif}>
                                <span style={{ width: '600px' }}>{project.description[0]}</span>
                                <div className={cl.dataProject}>
                                    <SmallTextGreen>{`Локация: ${project.city}`}</SmallTextGreen>
                                    <SmallTextGreen>
                                        {`Год реализации: ${moment(project.сreatedAt).format('YYYY')}`}
                                    </SmallTextGreen>
                                    <SmallTextGreen>{`Площадь: ${project.area} кв.м.`}</SmallTextGreen>
                                    <SmallTextGreen>{`Стиль: ${project.style}`}</SmallTextGreen>
                                    <SmallTextGreen>{`Цветовая гамма: ${project.color}`}</SmallTextGreen>
                                </div>
                            </div>
                        </div>
                        <div className={cl.gridProject}>
                            <img src={project.urls[1]} alt='' />
                            <img src={project.urls[2]} alt='' />
                            <img src={project.urls[3]} alt='' />
                            <div><span>{project.description[1]}</span></div>
                            <img src={project.urls[4]} alt='' />
                            <img src={project.urls[5]} alt='' />
                        </div>
                    </div>
                </div> : null}
        </div>
    );
};

export default SelectProject;