import React, {useContext, useEffect} from 'react';
import {AuthContext} from '../../../context';
import {observer} from 'mobx-react-lite';
import cl from '../OtherPages.module.css';
import MainH2 from '../../../components/UI/hats/MainH2';
import Area from '../../../components/UI/other/area';
import Time from '../../../components/UI/other/time';
import MainH4 from '../../../components/UI/hats/MainH4';
import MainH3black from '../../../components/UI/hats/MainH3black';
import GreenBtn from '../../../components/UI/buttons/GreenBtn';
import BrifState from '../BrifState';
import addFoto from '../../../components/UI/staticMedia/addFoto.svg';
import cloud from '../../../service/cloud';
import del from '../../../components/UI/staticMedia/del.svg';
import BrifRadioNoImg from '../../../components/BrifRadioNoImg';
import BrifFotoChoice from '../../../components/BrifFotoChoice';
import StateModals from '../../Modal/StateModals';
import WhiteBtn from '../../../components/UI/buttons/WhiteBtn';
import ChatUser from '../../Modal/ChatUser';
import servService from '../../../service/servService';
import Checkbox from '../../../components/Checkbox';

const Brif = () => {
    let now = new Date();

    const {isUser} = useContext(AuthContext);

    const variable = {
        type: ['квартира', 'частный дом', 'кафе/ресторан', 'офис', 'бутик'],
        style: ['минимализм', 'лофт', 'классика', 'эко дизайн', 'ар деко'],
        color: ['спокойная', 'насыщенная', 'монохром', 'двухцветная', 'светлая'],
        typeLight: ['верхний свет', 'настенное/декоративное'],
        typeMaterial: ['натуральные', 'необычные', 'современные'],
        topLight: ['люстры', 'подвесные светильники', 'трековые системы', 'встраиваемые светильники'],
        fallLight: ['скрытые подсветки', 'бра', 'торшеры', 'ночники'],
        material_1: ['дерево', 'камень', 'ткани'],
        material_2: ['серебро', 'экзотические породы древесины'],
        material_3: ['стекло', 'металл', 'бетон'],
    };

    useEffect(() => {
        cloud.getUrlsChild('brif', 'type').then(res => BrifState.setUrlsType(res));
        cloud.getUrlsChild('brif', 'color').then(res => BrifState.setUrlsColor(res));
        cloud.getUrlsChild('brif', 'style').then(res => BrifState.setUrlsStyle(res));
    }, []);

    async function SendOrder(e) {
        e.preventDefault();
        StateModals.setSuccessModal(true);
        try {
            const newOrder = {
                fio: `${isUser.firstName} ${isUser.secondName} ${isUser.surName}`,
                phone: isUser.phone,
                email: isUser.email,
                createdAt: now.toISOString(),
                updatedAt: now.toISOString(),
                brif: BrifState.wishList
            };
            await servService.addOrder(newOrder);
            BrifState.newBrifState();
            setTimeout(() => StateModals.setSuccessModal(false), 3000);
        } catch (err) {
            console.log(err);
        };
    };


    return (
        <div className={cl.fonBrif}>

            <ChatUser />

            <div className={cl.titleBrif}>
                <MainH2>{'ПОЖЕЛАНИЯ К ПРОЕКТУ'}</MainH2>
                {!isUser.role &&
                    <form>
                        <GreenBtn onClick={e => (e.preventDefault(), StateModals.setChatUser(!StateModals.chatUser))}>
                            {'чат с дизайнером'}
                        </GreenBtn>
                    </form>}
            </div>

            <div className={cl.topBlockBrif}>
                <div className={cl.terms}>

                    <MainH3black style={{ marginBottom: '20px' }}>{'РАССЧИТАТЬ СРОКИ РЕАЛИЗАЦИИ'}</MainH3black>
                    <MainH4>{'ПЛОЩАДЬ ПОМЕЩЕНИЯ'}</MainH4>
                    <Area />
                    <MainH4>{'СРОКИ'}</MainH4>
                    <Time />

                </div>

                <div className={cl.terms}>
                    <span>{'Вы можете прикрепить понравившиеся вам интерьеры или план помещений при его наличии'}</span>

                    <div className={cl.gridUserFoto}>
                        {BrifState.userFoto.map((item, ind) => {
                            return (
                                <div key={`addorderfoto-${ind}`} >
                                    <label htmlFor={`project-${ind}`}>
                                        <img src={BrifState.urlsFotoUser[ind] ? BrifState.urlsFotoUser[ind] : addFoto} alt='' className={cl.imageUser} />
                                        {item && (<img src={del}
                                            alt='удалить'
                                            className={cl.delFile}
                                            onClick={e => (e.preventDefault(), BrifState.delUserFoto(ind))} />)}
                                    </label>

                                    {!item && (<input type='file'
                                        accept='.jpg, .jpeg, .png,'
                                        id={`project-${ind}`}
                                        onChange={e => BrifState.setUserFoto(ind, e.target.files[0])} />)}
                                </div>
                            )
                        })}
                    </div>
                </div>
            </div>

            <BrifFotoChoice
                urls={BrifState.urlsType}
                variable={variable.type}
                wishList={BrifState.wishList.type}
                parameter='type'
            >
                {'Для какого помещения Вам нужен дизайн-проект?'}
            </BrifFotoChoice>

            <BrifFotoChoice
                urls={BrifState.urlsStyle}
                variable={variable.style}
                wishList={BrifState.wishList.style}
                parameter='style'
            >
                {'В каком стиле вы хотите интерьер?'}
            </BrifFotoChoice>

            <BrifFotoChoice
                urls={BrifState.urlsColor}
                variable={variable.color}
                wishList={BrifState.wishList.color}
                parameter='color'
            >
                {'Какая цветовая гамма Вам наиболее близка?'}
            </BrifFotoChoice>

            <div className={cl.terms}>

                <MainH3black>{'Предпочтение в освещении помещений'}</MainH3black>

                <div className={cl.choiceWithoutFoto}>
                    {variable.typeLight.map(item => {
                        return (
                            <BrifRadioNoImg key={item}
                                checked={BrifState.wishList.typeLight}
                                param='typeLight'
                            >
                                {item}
                            </BrifRadioNoImg>
                        )
                    })}
                </div>

                <div className={cl.choiceCheckboxLight}>
                    <div className={cl.blockLight}>
                        {variable.topLight.map(item => {
                            return (
                                <Checkbox key={item}
                                    variant={'caps'}
                                    checked={BrifState.wishList.light.includes(item)}
                                    change={() => BrifState.setWishListLight(item)}
                                    disabled={BrifState.wishList.typeLight !== 'верхний свет'}
                                >
                                    {item}
                                </Checkbox>
                            )
                        })}
                    </div>

                    <div className={cl.blockLight}>
                        {variable.fallLight.map(item => {
                            return (
                                <Checkbox key={item}
                                    variant={'caps'}
                                    checked={BrifState.wishList.light.includes(item)}
                                    change={() => BrifState.setWishListLight(item)}
                                    disabled={BrifState.wishList.typeLight !== 'настенное/декоративное'}
                                >
                                    {item}
                                </Checkbox>
                            )
                        })}
                    </div>
                </div>

            </div>

            <div className={cl.terms}>

                <MainH3black>{'Предпочтение материалов'}</MainH3black>

                <div className={cl.choiceWithoutFoto}>
                    {variable.typeMaterial.map(item => {
                        return <BrifRadioNoImg checked={BrifState.wishList.typeMaterial} param='typeMaterial' key={item}>{item}</BrifRadioNoImg>
                    })}
                </div>

                <div className={cl.choiceCheckboxMaterial}>
                    <div className={cl.blockMaterial}>
                        {variable.material_1.map(item => {
                            return (
                                <Checkbox key={item}
                                    variant={'caps'}
                                    style={{width: '121px'}}
                                    checked={BrifState.wishList.material.includes(item)}
                                    change={() => BrifState.setWishListMaterial(item)}
                                    disabled={BrifState.wishList.typeMaterial !== 'натуральные'}
                                >
                                    {item}
                                </Checkbox>
                            )
                        })}
                    </div>

                    <div className={cl.blockMaterial}>
                        {variable.material_2.map(item => {
                            return (
                                <Checkbox key={item}
                                    variant={'caps'}
                                    style={{width: '121px'}}
                                    checked={BrifState.wishList.material.includes(item)}
                                    change={() => BrifState.setWishListMaterial(item)}
                                    disabled={BrifState.wishList.typeMaterial !== 'необычные'}
                                >
                                    {item}
                                </Checkbox>
                            )
                        })}
                    </div>

                    <div className={cl.blockMaterial}>
                        {variable.material_3.map(item => {
                            return (
                                <Checkbox key={item}
                                    variant={'caps'}
                                    style={{width: '121px'}}
                                    checked={BrifState.wishList.material.includes(item)}
                                    change={() => BrifState.setWishListMaterial(item)}
                                    disabled={BrifState.wishList.typeMaterial !== 'современные'}
                                >
                                    {item}
                                </Checkbox>
                            )
                        })}
                    </div>
                </div>

            </div>

            <div className={cl.terms}>
                <MainH3black>{'Что для Вас является самым важным в интерьере?'}</MainH3black>

                <textarea maxLength='255'
                    className={cl.brifComment}
                    value={BrifState.wishList.comment}
                    onChange={e => BrifState.setWishList({ comment: e.target.value })}
                    placeholder='Например, эргономичность, простор и тд. (максимум 255 символов)' />
            </div>

            <div className={cl.titleBrif}>
                <form onSubmit={e => SendOrder(e)}>
                    <GreenBtn>
                        {'отправить'}
                    </GreenBtn>
                </form>

                <form>
                    <WhiteBtn onClick={e => (cloud.getDoc(e, 'contract.docx'))}>
                        {'Скачать договор'}
                    </WhiteBtn>
                </form>
            </div>
        </div>
    );
};

export default observer(Brif);