import { makeAutoObservable } from 'mobx';


class ProjectsState {

    projects = [];
    progectGallery = [];

    constructor() {
        makeAutoObservable(this);
    };

    setProjects(data) {
        this.projects = data;
    };

    addProject(data) {
        this.projects = [data, ...this.projects];
    };

    setProjectGallery(data) {
        this.progectGallery = data;
    };

    delProject(data) {
        this.projects = this.projects.filter(item => item.id !== data.id);
    };

};

export default new ProjectsState();