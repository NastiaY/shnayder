import React from 'react';
import cl from './OtherPages.module.css';
import MainH3 from '../../components/UI/hats/MainH3';
import MainH2 from '../../components/UI/hats/MainH2';
import MainH3black from '../../components/UI/hats/MainH3black';


const Cost = () => {

    return (
        <div className={cl.fonDesigner}>

            <div className={cl.hatCost}>
                <MainH3>ВЫБЕРИТЕ ТАРИФ, КОТОРЫЙ ПОДХОДИТ ВАМ</MainH3>
                <MainH2>Тарифы</MainH2>
            </div>

            <div className={cl.cotentCost}>

                <div className={cl.blockCost}>

                    <MainH2>Базовый</MainH2>

                    <MainH3black>Что входит</MainH3black>

                    <ol >
                        <li className={cl.blockLiCost}>Два планировочных решения</li>
                        <li className={cl.blockLiCost}>Разработка концепции</li>
                        <li className={cl.blockLiCost}>3D-визуализация всех помещений</li>
                        <li className={cl.blockLiCost}>Рабочая документация (альбом чертежей)</li>
                    </ol>

                    <span className={cl.textCost}>1500 Р/кв.м</span>
                
                </div>

                <div className={cl.blockCost}>

                    <MainH2>СТАНДАРТНЫЙ</MainH2>

                    <MainH3black>Что входит</MainH3black>

                    <ol >
                        <li className={cl.blockLiCost}>Два планировочных решения</li>
                        <li className={cl.blockLiCost}>Разработка концепции</li>
                        <li className={cl.blockLiCost}>3D-визуализация всех помещений</li>
                        <li className={cl.blockLiCost}>Рабочая документация (альбом чертежей)</li>
                        <li className={cl.blockLiCost}>Спецификация мебели и материалов</li>
                    </ol>

                    <span className={cl.textCost}>2500 Р/кв.м</span>
                
                </div>

                <div className={cl.blockCost}>

                    <MainH2>ПРЕМИУМ</MainH2>

                    <MainH3black>Что входит</MainH3black>

                    <ol >
                        <li className={cl.blockLiCost}>Два планировочных решения</li>
                        <li className={cl.blockLiCost}>Разработка концепции</li>
                        <li className={cl.blockLiCost}>3D-визуализация всех помещений</li>
                        <li className={cl.blockLiCost}>Рабочая документация (альбом чертежей)</li>
                        <li className={cl.blockLiCost}>Спецификация мебели и материалов</li>
                        <li className={cl.blockLiCost}>Авторский надзор</li>
                    </ol>

                    <span className={cl.textCost}>3500 Р/кв.м</span>
                
                </div>

            </div>

        </div>
    );
};

export default Cost;