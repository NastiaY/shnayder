import React, {useContext, useEffect, useState} from 'react';
import axios from 'axios';
import cl from './OtherPages.module.css';
import MainH2 from '../../components/UI/hats/MainH2';
import MainH3black from '../../components/UI/hats/MainH3black';
import MainH3 from '../../components/UI/hats/MainH3';
import GreenBtn from '../../components/UI/buttons/GreenBtn';
import {AuthContext} from '../../context';
import myAxi from '../../service/interceptor';
import FullReview from '../../components/UI/strTable/FullReview';
import StateModals from '../Modal/StateModals';
import Title from '../../components/UI/hats/Title';

const Reviews = () => {
    let now = new Date();

    const {isUser, isAuth, setIsAuth} = useContext(AuthContext);

    const [review, setReview] = useState([]);
    const [valInput, setValInput] = useState('');
    const [Offset, setOffset] = useState(0);
    const [totalCount, setTotalCount] = useState(0);

    useEffect(() => {
        axios.get(process.env.REACT_APP_API_URL + '/api/review', {
            params: isUser.role ? { limit: 5, offset: Offset } : {status: true, limit: 5, offset: Offset}
        })
            .then(res => {
                setOffset(Offset + 5);
                setTotalCount(res.headers['total-count']);
                setReview(res.data);
            })
            .catch(err => {
                console.log(err);
            })
    }, []);

    const getReview = (e) => {
        e.preventDefault();
        axios.get(process.env.REACT_APP_API_URL + '/api/review', {
            params: isUser.role ? {limit: 5, offset: Offset} : {status: true, limit: 5, offset: Offset}
        })
            .then(res => {
                setOffset(Offset + 5);
                setReview([...review, ...res.data]);
            })
            .catch(err => {
                console.log(err);
            })
    };

    async function addReview(e) {
        e.preventDefault();
        const NewReview = {
            createdAt: now.toISOString(),
            message: valInput,
            userId: isUser.id,
            status: true
        };
        await myAxi.post('/review', NewReview)
            .then(res => {
                setReview([{
                    ...NewReview,
                    id: res.data,
                    firstName: isUser.firstName,
                    surName: isUser.surName,
                    email: isUser.email
                }, ...review]);
                setValInput('');
            })
            .catch(err => {
                setIsAuth(false);
                console.log(err)
            });

    };

    const Login = (e) => {
        e.preventDefault();
        StateModals.setComeIn(true);
    };

    async function deleteReview(e, id) {
        e.preventDefault();
        if (window.confirm('Вы действительно хотите удалить комментарий?')) {
            await myAxi.delete(`/review?id=${id}`).catch(err => console.log(err));
            let corRev = review.filter(post => post.id !== id);
            setReview(corRev);
            setOffset(Offset - 1);
            setTotalCount(totalCount - 1);
        };
    };


    return (
        <div className={cl.fonDesigner}>
            <div className={cl.hatDesigner}>
                <MainH3>{'Отзывы'}</MainH3>
                <MainH2>{'Что о нас говорят'}</MainH2>
            </div>
            {isAuth ? (
                <form onSubmit={addReview} className={cl.formRev}>
                    <Title variant={'gray14'}>{isUser.firstName + ' ' + isUser.surName[0] + '.'}</Title>
                    <textarea className={cl.brifComment}
                        style={{height: '130px'}}
                        maxLength='500'
                        value={valInput}
                        onChange={e => setValInput(e.target.value)}
                        placeholder='Отзыв... (максимум 500 символов)' />

                    <GreenBtn disabled={!valInput}>{'разместить отзыв'}</GreenBtn>
                </form>
            ) : (
                <div className={cl.formRev}>
                    <MainH3black>{'Войдите в аккаунт чтобы оставить отзыв'}</MainH3black>
                    <GreenBtn onClick={e => Login(e)}>{'ВОЙТИ'}</GreenBtn>
                </div>
            )}
            <div className={cl.contentReview}>
                {review.map((post, ind) => {
                    return <FullReview key={`${post.id} + ${ind}`} post={post} DelRev={deleteReview} />
                })}
            </div>
            {Offset < totalCount && (
                <form>
                    <button className={cl.lastRev} onClick={e => getReview(e)}>
                        {'Еще...'}
                    </button>
                </form>
            )}
        </div>
    );
};

export default Reviews;