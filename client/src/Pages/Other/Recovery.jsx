import React, {useState} from 'react';
import {useParams} from 'react-router-dom';
import cl from './OtherPages.module.css';
import MainH3black from '../../components/UI/hats/MainH3black';
import MainInput from '../../components/UI/other/mainInput';
import GreenBtn from '../../components/UI/buttons/GreenBtn';
import axios from 'axios';
import Title from '../../components/UI/hats/Title';
import openEye from '../../components/UI/staticMedia/openEye.svg';
import closeEye from '../../components/UI/staticMedia/closeEye.svg';

const Recovery = () => {
    const [newPass, setNewPass] = useState('');
    const [resp, setResp] = useState({alert: '', success: ''});
    const [visible, setVisible] = useState(false);

    const NotCopy = e => {
        e.preventDefault();
    };
    const params = useParams();

    const UpdatePass = (e) => {
        e.preventDefault();
        axios.put(process.env.REACT_APP_API_URL + '/api/rec', {link: params.link, pass: newPass}, { withCredentials: true })
            .then(res => {
                setNewPass('');
                setResp({alert: '', success: res.data})
            })
            .catch(err => {
                if (err.response.status === 409) {
                    setResp({...resp, alert: err.response.data})
                } else {
                    console.log(err.response)
                }
            })
    };

    return (
        <div className={cl.fonBrif}>
            <MainH3black>{'Введите новый пароль'}</MainH3black>
            <form onSubmit={UpdatePass} style={{ display: 'grid', gap: '60px' }}>
                <div>
                    {resp.success ?
                        <Title variant={'green16'} wrapper={'shrink'}>{resp.success}</Title> :
                        <div>
                            <MainInput
                                type={visible ? 'text' : 'password'}
                                minLength='8'
                                maxLength='30'
                                value={newPass}
                                onCopy={NotCopy}
                                onChange={e => setNewPass(e.target.value)}
                                placeholder='* Новый пароль. (Минимум 8 символов)'
                                style={resp.alert ? { boxShadow: 'inset 0px -1px 0px #E93E3E', width: '580px' } : { width: '580px' }} />

                            <img src={visible ? openEye : closeEye} alt='сделать видимым' style={{ cursor: 'pointer' }}
                                onClick={() => setVisible(!visible)} />
                        </div>
                    }
                    {resp.alert &&
                        <span className={cl.notvalidLink}>{resp.alert}</span>}
                </div>
                <GreenBtn disabled={(!newPass)} type='submit'>{'Отправить'}</GreenBtn>
            </form>
        </div>
    );
};

export default Recovery;