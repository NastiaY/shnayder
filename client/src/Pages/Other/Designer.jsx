import React, { useEffect, useState } from 'react';
import cl from './OtherPages.module.css';
import MainH3 from '../../components/UI/hats/MainH3';
import MainH2 from '../../components/UI/hats/MainH2';
import cloud from '../../service/cloud';


const Designer = () => {

    const [urlDis, setUrlDis] = useState('');
    const [urlSert, setUrlSert] = useState('');

    useEffect(() => {
        cloud.getOneUrl('static', 'designer.png').then(res => setUrlDis(res));
        cloud.getOneUrl('static', 'sert_dis.png').then(res => setUrlSert(res));
    }, []);

    return (
        <div className={cl.fonDesigner}>

            <div className={cl.hatDesigner}>
                <MainH3>ПАРУ СЛОВ О ДИЗАЙНЕРЕ</MainH3>
                <MainH2>ДАВАЙТЕ ЗНАКОМИТЬСЯ</MainH2>
            </div>

            <div className={cl.cotentDesigner}>

                <img src={urlDis} alt='Дарина Шнайдер' className={cl.imgDis} />

                <div className={cl.descriptionDesigner}>

                    <span>Меня зовут Шнайдер Дарина, с 2021 года я реализую
                        дизайн-проекты красивые и функциональные интерьеры.</span>

                    <ul>
                        <li className={cl.blockLiCost}>Прошла обучение в крупнейшей профильной онлайн-школе дизайна “Contented”.</li>
                        <li className={cl.blockLiCost}>Участвую в конкурсах и интенсивах по 3D-графике в ведущей школе России по моделированию 3D CLUB.</li>
                        <li className={cl.blockLiCost}>Работаю в строительной компании ведущим дизайнером с 2020 года.</li>
                    </ul>

                    <p>Мой подход строится на тесном взаимодействии с вами -
                        слушаю ваши пожелания, выявляю потребности и работаю
                        вместе с вами, чтобы создать идеальное пространство
                        для вас и вашей семьи. <br /><br />
                        Цель - преобразовать и украсить
                        пространство, отразить ваш стиль, вкусы, ценности и достижения.</p>

                    <img src={urlSert} alt='' className={cl.imgSert} />
                </div>

            </div>

        </div>
    );
};

export default Designer;