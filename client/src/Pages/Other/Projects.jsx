import React, { useContext, useEffect, useRef, useState } from 'react';
import cl from './OtherPages.module.css';
import TitleProject from '../../components/titleProject';
import GreenBtn from '../../components/UI/buttons/GreenBtn';
import { AuthContext } from '../../context';
import StateModals from '../Modal/StateModals';
import { observer } from 'mobx-react-lite';
import PrState from './ProjectsState';
import servService from '../../service/servService';


const Project = () => {

    const { isUser } = useContext(AuthContext);

    const [valFilter, setValFilter] = useState('все');

    const [isProjectsLoad, setProjectsLoad] = useState(false);
    const [Offset, setOffset] = useState(0);
    const [totalCount, setTotalCount] = useState(0);

    const LastElement = useRef();
    const observer = useRef();

    useEffect(() => {
        if (isProjectsLoad) return;
        if (observer.current) observer.current.disconnect();
        let callback = (entries, observer) => {
            if (entries[0].isIntersecting && Offset < totalCount) {
                setOffset(Offset + 10);
            };
        };
        observer.current = new IntersectionObserver(callback);
        observer.current.observe(LastElement.current);
    }, [isProjectsLoad]);

    useEffect(() => {
        setProjectsLoad(true);
        servService.getOnlyProjects({ params: { limit: 10, offset: Offset } })
            .then(res => {
                setTotalCount(res[0]);
                PrState.setProjects([...PrState.projects, ...res[1]]);
                setProjectsLoad(false);
            })
            .catch(err => {
                console.log(err);
                setProjectsLoad(false);
            });
    }, [Offset]);

    useEffect(() => {
        return () => {
            PrState.setProjects([]);
        };
    }, []);

    const Search = (type) => {
        setValFilter(type);
        if (type === 'все') {
            PrState.setProjects([]);
            return setOffset(0);
        };
        servService.getOnlyProjects({ params: { type: type } })
            .then(res => {
                PrState.setProjects(res[1]);
                setTotalCount(0);
            })
            .catch(err => console.log(err))
    };


    return (
        <div className={cl.fonProjects}>

            <div className={cl.filterProject}>

                <div>
                    <input type='radio'
                        checked={valFilter === 'все'}
                        onChange={() => Search('все')} />
                    <label>Все</label>
                </div>

                <div>
                    <input type='radio'
                        checked={valFilter === true}
                        onChange={() => Search(true)} />
                    <label>Жилые</label>
                </div>

                <div>
                    <input type='radio'
                        checked={valFilter === false}
                        onChange={() => Search(false)} />
                    <label>Коммерческие</label>
                </div>

                {isUser.role &&
                    <form><GreenBtn style={{ width: '100%' }}
                        onClick={e => (e.preventDefault(), StateModals.setAddProject(true))}>Добавить проект</GreenBtn></form>}

            </div>

            <div className={cl.tableProject}>

                {PrState.projects.map(pr => {
                    return <TitleProject key={pr.id} item={pr} />
                })}

                {Offset < totalCount ?
                    <div ref={LastElement} className={cl.lastElementProjects} /> :
                    <div ref={LastElement} />}
            </div>

        </div>
    );
};

export default observer(Project);