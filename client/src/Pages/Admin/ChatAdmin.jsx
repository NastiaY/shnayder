import React, {useContext, useEffect, useMemo, useState} from 'react';
import {observer} from 'mobx-react-lite';
import {NavLink, Route, Routes} from 'react-router-dom';
import cl from './Admin.module.css';
import Title from '../../components/UI/hats/Title';
import AdminState from './AdminState';
import {AuthContext} from '../../context';
import WindSelectUser from '../../components/WindSelectUser';
import myAxi from '../../service/interceptor';
import socket from '../../service/webSocket';
import BtnOnlyOnline from '../../components/UI/buttons/BtnOnlyOnline';
import SmallText from '../../components/UI/other/smallText';
import cn from 'classname';

const ChatAdmin = () => {
    const { isUser } = useContext(AuthContext);
    const [usersOnline, setUsersOnline] = useState([]);
    const [sort, setSort] = useState(false);

    useEffect(() => {
        socket.emit('userConnect', isUser.id);
        myAxi.get('/room').then(res => AdminState.setUsersRoom(res.data));
    }, []);

    useEffect(() => {
        socket.on('respOnlineUsers', (data) => {
            setUsersOnline(data);
        });
    }, [usersOnline]);

    useEffect(() => {
        socket.on('response', data => {
            if (data.idRoom !== AdminState.useRoom) {
                AdminState.addNewCountState(data.idRoom, 1);
            };
        })
    }, []);

    useEffect(() => {
        const handleUnload = () => {
            socket.emit('disconect', isUser.id);
        };
        window.addEventListener('beforeunload', handleUnload);
        return () => {
            window.removeEventListener('beforeunload', handleUnload);
            socket.off('response');
            socket.off('respOnlineUsers');
            socket.emit('disconect', isUser.id);
        };
    }, []);

    const FilterOnline = useMemo(() => {
        if (sort) {
            return (AdminState.usersRoom.filter(user => usersOnline.includes(user.idRoom)));
        } else {
            return AdminState.usersRoom;
        };
    }, [AdminState.usersRoom, usersOnline, sort]);


    return (
        <div className={cl.fonChatAdmin}>
            <div className={cl.UserRoom}>
                <div className={cl.sidebar}>
                    <Title variant={'green16'} wrapper={'shrink'}>{'Пользователи'}</Title>
                    <BtnOnlyOnline sort={sort} onChange={setSort} />
                </div>
                <div className={cl.listUsers}>
                    {FilterOnline.map((user) => {
                        return <NavLink to={`/adminpanel/chat/${user.idRoom}`}
                            key={user.email}
                            className={({isActive}) => isActive ? cn(cl.usersChatRoom, cl.act) : cl.usersChatRoom}>
                            <div className={cl.dataNameUserChat}>
                                <span>{user.firstName + ' ' + user.secondName + ' ' + user.surName}</span>
                                <SmallText>{user.email}</SmallText>
                            </div>
                            <div className={cl.dataUserChat}>
                                <span className={user.state != 0 ? cl.counter : cl.counterNull}>{user.state}</span>
                                {usersOnline.includes(user.idRoom) &&
                                    <span className={cl.onlineTag}>{'в сети'}</span>}
                            </div>
                        </NavLink>
                    })}
                </div>
            </div>
            <Routes>
                <Route path={`/:id`} element={<WindSelectUser />} exact={true} />
            </Routes>
        </div>
    );
};

export default observer(ChatAdmin);