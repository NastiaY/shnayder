import React, { useEffect, useRef, useState } from 'react';
import cl from './Admin.module.css';
import MainH3black from '../../components/UI/hats/MainH3black';
import myAxi from '../../service/interceptor';
import StrUsers from '../../components/UI/strTable/StrUsers';
import MainInput from '../../components/UI/other/mainInput';
import find from '../../components/UI/staticMedia/find.svg';
import Title from '../../components/UI/hats/Title';

const UsersTable = () => {
    const [valueUsers, setValueUsers] = useState([]);
    const [Offset, setOffset] = useState(0);
    const [totalCount, setTotalCount] = useState(0);
    const [isUserLoad, setUserLoad] = useState(false);

    const [searchValue, setSearchValue] = useState('');

    const LastElement = useRef();
    const observer = useRef();

    useEffect(() => {
        if (isUserLoad) return;
        if (observer.current) observer.current.disconnect();
        let callback = (entries, observer) => {
            if (entries[0].isIntersecting && Offset < totalCount) {
                setOffset(Offset + 20);
            };
        };
        observer.current = new IntersectionObserver(callback);
        observer.current.observe(LastElement.current);
    }, [isUserLoad]);

    useEffect(() => {
        setUserLoad(true);
        myAxi.get('/users', {params: {limit: 20, offset: Offset}})
            .then(res => {
                setTotalCount(res.headers['total-count']);
                setValueUsers([...valueUsers, ...res.data]);
                setUserLoad(false);
            })
            .catch(err => {
                console.log(err);
                setUserLoad(false);
            })
    }, [Offset]);

    const Search = (e) => {
        e.preventDefault();
        if (searchValue === '') {
            setValueUsers([]);
            return setOffset(0);
        };
        myAxi.get('/users', {params: {email: searchValue}})
            .then(res =>  {
                setValueUsers(res.data);
                setTotalCount(0);
            })
            .catch(err => console.log(err))
    };

    return (
        <div className={cl.contentAdmin}>
            <div className={cl.TitleUsers}>
                <MainH3black>{'Список пользователей'}</MainH3black>
                <div className={cl.search}>
                    <MainInput type='email'
                        value={searchValue}
                        onChange={e => setSearchValue(e.target.value)}
                        placeholder='* Искать по email'
                        maxLength='255' />
                    <form><button onClick={e => Search(e)}><img src={find} alt='Найти' /></button></form>
                </div>
            </div>
            <div className={cl.UserTable}>
                <div className={cl.Title}>
                    <Title
                        variant={'white12'}
                        wrapper={'green'}
                        style={{width: '310px'}}>{'ФИО'}</Title>
                    <Title
                        variant={'white12'}
                        wrapper={'green'}
                        style={{width: '220px'}}>{'email'}</Title>
                    <Title
                        variant={'white12'}
                        wrapper={'green'}
                        style={{width: '150px'}}>{'Статус'}</Title>
                    <Title
                        variant={'white12'}
                        wrapper={'green'}
                        style={{width: '120px'}}>{'Создан'}</Title>
                    <Title
                        variant={'white12'}
                        wrapper={'green'}
                        style={{width: '120px'}}>{'Изменен'}</Title>
                </div>
                <div className={cl.scrollTable}>
                    {valueUsers.map(user => {
                        return <StrUsers key={user.id} user={user} />
                    })}
                    {Offset < totalCount ?
                        <div ref={LastElement} className={cl.lastElement} /> :
                        <div ref={LastElement} />}
                </div>
            </div>
        </div>
    );
};

export default UsersTable;