import React from 'react';
import cl from './Admin.module.css';
import { Route, Routes } from 'react-router-dom';
import { adminRoutes } from '../../Routes/routes';
import NavBtnAdmin from '../../components/UI/buttons/NavBtnAdmin';


const MainAdmin = () => {

    return (
        <div className={cl.fonAdmin}>

            <div className={cl.navBarAdmin}>
                <NavBtnAdmin to='/adminpanel/users'>пользователи</NavBtnAdmin>
                <NavBtnAdmin to='/adminpanel/orders'>заявки</NavBtnAdmin>
                <NavBtnAdmin to='/adminpanel/messages'>рассылка</NavBtnAdmin>
                <NavBtnAdmin to='/adminpanel/chat'>чат</NavBtnAdmin>
            </div>

            <Routes>
                {adminRoutes.map(route =>
                    <Route
                        element={route.component}
                        path={route.path}
                        exact={route.exact}
                        key={route.path} />)}
            </Routes>

        </div>
    );
};

export default MainAdmin;