import React, {useEffect, useState} from 'react';
import {observer} from 'mobx-react-lite';
import cl from './Admin.module.css';
import GreenBtn from '../../components/UI/buttons/GreenBtn';
import MainInput from '../../components/UI/other/mainInput';
import Title from '../../components/UI/hats/Title';
import addFoto from '../../components/UI/staticMedia/addFoto.svg';
import del from '../../components/UI/staticMedia/del.svg';
import myAxi from '../../service/interceptor';
import cloud from '../../service/cloud';
import AdminState from './AdminState';
import StrList from '../../components/UI/strTable/StrList';

const ConstructorMessages = () => {
    const [dataList, setDataList] = useState({
        subject: 'Заголовок',
        logo: 'https://firebasestorage.googleapis.com/v0/b/shnayder-df141.appspot.com/o/static%2FlogoMini.png?alt=media&token=c687c667-a3ad-4145-b1c8-2670506c872c',
        img: [],
        text: 'Текст рассылки',
        btn: ['', 'кнопка']
    });
    const [load, setLoad] = useState(false);

    const setImgList = (foto) => {
        const url = URL.createObjectURL(foto);
        setDataList({...dataList, img: [url, foto]});
    };

    const delImgList = (e) => {
        e.preventDefault();
        URL.revokeObjectURL(dataList.img[0]);
        setDataList({...dataList, img: []});
    };

    async function sendMailAll(e) {
        e.preventDefault();
        if (window.confirm('Вы уверены что все заполнили и добавили?')) {
            try {
                setLoad(true);
                const urlImg = await cloud.addNewMailImg('list', dataList.img[1]);
                const data = { ...dataList, img: urlImg }
                await myAxi.post('/mailing', data).then(res => console.log(res.data));
                setDataList({
                    subject: 'Заголовок',
                    logo: 'https://firebasestorage.googleapis.com/v0/b/shnayder-df141.appspot.com/o/static%2FlogoMini.png?alt=media&token=c687c667-a3ad-4145-b1c8-2670506c872c',
                    img: [],
                    text: 'Текст рассылки',
                    btn: ['', 'кнопка']
                });
                setLoad(false);
            } catch (err) {
                console.log(err);
            }
        };
    };

    useEffect(() => {
        myAxi.get('/mail').then(res => AdminState.setMailList(res.data)).catch(err => console.log(err));
    }, []);

    return load ?
        <Title variant={'green16'} wrapper={'shrink'}>{'Идет отправка...'}</Title> : (
        <div className={cl.contentAdmin}>
            <div className={cl.fonList}>
                <div className={cl.ListMenu}>
                    <Title variant={'green16'} wrapper={'shrink'}>{'Конструктор рассылки'}</Title>
                    <MainInput type='text'
                        maxLength='100'
                        value={dataList.subject}
                        onChange={e => setDataList({ ...dataList, subject: e.target.value })}
                        placeholder='* Заголовок' />
                    <textarea type='text'
                        className={cl.textList}
                        maxLength='500'
                        value={dataList.text}
                        onChange={e => setDataList({ ...dataList, text: e.target.value })}
                        placeholder='* Текст письма' />
                    <MainInput type='text'
                        maxLength='30'
                        value={dataList.btn[1]}
                        onChange={e => setDataList({ ...dataList, btn: [dataList.btn[0], e.target.value] })}
                        placeholder='* Содержание кнопки' />
                    <MainInput type='text'
                        value={dataList.btn[0]}
                        onChange={e => setDataList({ ...dataList, btn: [e.target.value, dataList.btn[1]] })}
                        placeholder='* Ссылка на кнопке' />
                    <div className={cl.addImgBlock}>
                        <label htmlFor='imglist'>
                            <img src={dataList.img[0] ? dataList.img[0] : addFoto} alt='' className={cl.imageList} />
                            {dataList.img[0] && <img src={del}
                                alt='удалить'
                                className={cl.delimglist}
                                onClick={e => delImgList(e)} />}
                        </label>
                    {!dataList.img[0] && <input type='file'
                        accept='.jpg, .jpeg, .png'
                        id='imglist'
                        onChange={e => setImgList(e.target.files[0])} />}
                    </div>
                    <form onClick={e => sendMailAll(e)}>
                        <GreenBtn disabled={!dataList.btn[0]}>
                            {'отправить всем'}
                        </GreenBtn>
                    </form>
                </div>
                <div className={cl.wrapperMessage}>
                    <div className={cl.contentMessage}>
                        <img src={dataList.img[0]} alt='' className={cl.imgMessage} />
                        <span style={{padding: '30px', fontSize: '20px', fontWeight: '600'}}>{dataList.subject}</span>
                        <span style={{padding: '0px 30px'}}>{dataList.text}</span>
                        <a disabled={true} href={dataList.btn[0]} className={cl.linkMessage}>
                            {dataList.btn[1]}
                        </a>
                        <div style={{display: 'flex', justifyContent: 'space-between', gap: '30px', width: '100%', padding: '30px'}}>
                            <img src={dataList.logo} alt='SHNAYDER interior design' style={{width: '147px'}} />
                            <div style={{ display: 'flex', gap: '5px', flexDirection: 'column', alignItems: 'flex-end'}}>
                                <a href='http://localhost:3000' style={{color: '#949494'}}>
                                    {'© 2023 “SHNAYDER interior design”'}
                                </a>
                                <a href='http://localhost:5000/api/mail?email=${to}' style={{textDecoration: 'none', fontSize: '12px', color: '#949494'}}>
                                    {'Отписаться'}
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className={cl.tableList}>
                {AdminState.mailList.map(item => {
                    return <StrList key={item.email} mail={item} />
                })}
            </div>
        </div>
    );
};

export default observer(ConstructorMessages);