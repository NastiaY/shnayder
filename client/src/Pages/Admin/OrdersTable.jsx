import React, {useEffect, useRef, useState} from 'react';
import {observer} from 'mobx-react-lite';
import cl from './Admin.module.css';
import MainInput from '../../components/UI/other/mainInput';
import MainH3black from '../../components/UI/hats/MainH3black';
import find from '../../components/UI/staticMedia/find.svg';
import myAxi from '../../service/interceptor';
import AdminState from './AdminState';
import StrOrder from '../../components/UI/strTable/StrOrder';
import GreenBtn from '../../components/UI/buttons/GreenBtn';
import StateModals from '../Modal/StateModals';
import Title from '../../components/UI/hats/Title';

const OrdersTable = () => {
    const [searchValue, setSearchValue] = useState('');
    const [isOrderLoad, setOrderLoad] = useState(false);
    const [Offset, setOffset] = useState(0);
    const [totalCount, setTotalCount] = useState(0);

    const LastElement = useRef();
    const observer = useRef();

    useEffect(() => {
        if (isOrderLoad) return;
        if (observer.current) observer.current.disconnect();
        let callback = (entries, observer) => {
            if (entries[0].isIntersecting && Offset < totalCount) {
                setOffset(Offset + 20);
            };
        };
        observer.current = new IntersectionObserver(callback);
        observer.current.observe(LastElement.current);
    }, [isOrderLoad]);

    useEffect(() => {
        setOrderLoad(true);
        myAxi.get('/orders', {params: {limit: 20, offset: Offset}})
            .then(res => {
                setTotalCount(res.headers['total-count']);
                AdminState.setOrders([...AdminState.orders, ...res.data]);
                setOrderLoad(false);
            })
            .catch(err => {
                console.log(err);
                setOrderLoad(false);
            });
    }, [Offset]);

    useEffect(() => {
        return () => {
            AdminState.setOrders([]); 
        };
    }, []);

    const Search = (e) => {
        e.preventDefault();
        if (searchValue === '') {
            AdminState.setOrders([]);
            return setOffset(0);
        };
        myAxi.get('/orders', {params: {email: searchValue}})
            .then(res =>  {
                AdminState.setOrders(res.data);
                setTotalCount(0);
            })
            .catch(err => console.log(err))
    };

    return (
        <div className={cl.contentAdmin}>
            <div className={cl.TitleUsers}>
                <MainH3black>{'Список заявок'}</MainH3black>
                <div className={cl.searchOrder}>
                    <MainInput type='email'
                        value={searchValue}
                        onChange={e => setSearchValue(e.target.value)}
                        placeholder='* Искать по email'
                        maxLength='255' />
                    <form><button onClick={e => Search(e)} className={cl.searchOrderBtn}><img src={find} alt='Найти' /></button></form>
                    <form><GreenBtn onClick={e => (e.preventDefault(), StateModals.setAddOrderAdmin(true))}>Добавить заявку</GreenBtn></form>
                </div>
            </div>
            <div className={cl.UserTable}>
                <div className={cl.TitleOrder}>
                    <Title
                        variant={'white12'}
                        wrapper={'green'}
                        style={{width: '53px'}}>{'№'}</Title>
                    <Title
                        variant={'white12'}
                        wrapper={'green'}
                        style={{width: '200px'}}>{'ФИО'}</Title>
                    <Title
                        variant={'white12'}
                        wrapper={'green'}
                        style={{width: '170px'}}>{'email'}</Title>
                    <Title
                        variant={'white12'}
                        wrapper={'green'}
                        style={{width: '130px'}}>{'телефон'}</Title>
                    <Title
                        variant={'white12'}
                        wrapper={'green'}
                        style={{width: '221px'}}>{'Статус'}</Title>
                    <Title
                        variant={'white12'}
                        wrapper={'green'}
                        style={{width: '90px'}}>{'Создан'}</Title>
                    <Title
                        variant={'white12'}
                        wrapper={'green'}
                        style={{width: '90px'}}>{'Изменен'}</Title>
                    <Title
                        variant={'white12'}
                        wrapper={'green'}
                        style={{width: '63px'}}>{'готов'}</Title>
                </div>
                <div className={cl.scrollTable}>
                    {AdminState.orders.map(order => {
                        return <StrOrder key={order.id} order={order} />
                    })}
                    {Offset < totalCount ?
                        <div ref={LastElement} className={cl.lastElement} /> :
                        <div ref={LastElement} />}
                </div>
            </div>
        </div>
    );
};

export default observer(OrdersTable);