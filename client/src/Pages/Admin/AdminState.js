import { makeAutoObservable } from 'mobx';
import myAxi from '../../service/interceptor';


class AdminState {

    orders = [];
    usersRoom = [];
    useRoom = 0;
    mailList = [];

    constructor() {
        makeAutoObservable(this);
    };

    setOrders(data) {
        this.orders = data;
    };

    addNewOrder(data) {
        this.orders = [data, ...this.orders];
    }

    delOrder(id) {
        this.orders = this.orders.filter(item => item.id !== id);
    };

    setUsersRoom(data) {
        this.usersRoom = data;
    };

    upUserRoom(id, count) {
        let userRoom = this.usersRoom.find(room => room.idRoom == id );
        let ind = this.usersRoom.indexOf(userRoom);
        this.usersRoom[ind]= {...userRoom, state: count}
    };

    addNewCountState(id, count) {
        let userRoom = this.usersRoom.find(room => room.idRoom === id );
        if (!userRoom) {
            myAxi.get('/room').then(res => this.setUsersRoom(res.data));
        } else {
            let ind = this.usersRoom.indexOf(userRoom);
            this.usersRoom[ind]= {...userRoom, state: parseInt(userRoom.state) + count}
        }
        
    };

    setUseRoom(data) {
        this.useRoom = data;    
    };

    setMailList(data) {
        this.mailList = data;
    };

    delEmailList(email) {
        this.mailList = this.mailList.filter(item => item.email !== email);
    };

};

export default new AdminState();