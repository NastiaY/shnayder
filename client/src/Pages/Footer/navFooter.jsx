import React from 'react';
import cl from './Footer.module.css';
import MenuBtnFooter from '../../components/UI/buttons/MenuBtnFooter';


const NavFooter = () => {

    const handleUp = () => {
        window.scrollTo({ top: 0, behavior: 'smooth' });
    };

    return (
        <div className={cl.navBarFooter}>
            <MenuBtnFooter to='/aboutus' onClick={handleUp}>дизайнер</MenuBtnFooter>
            <MenuBtnFooter to='/project' onClick={handleUp}>проекты</MenuBtnFooter>
            <MenuBtnFooter to='/cost' onClick={handleUp}>стоимость</MenuBtnFooter>
            <MenuBtnFooter to='/collaborate' onClick={handleUp}>сотрудничество</MenuBtnFooter>
            <MenuBtnFooter to='/contacts' onClick={handleUp}>контакты</MenuBtnFooter>
            <MenuBtnFooter to='/reviews' onClick={handleUp}>отзывы</MenuBtnFooter>
        </div>
    );
};

export default NavFooter;