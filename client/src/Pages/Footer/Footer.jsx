import React, {useState} from 'react';
import {NavLink} from 'react-router-dom';
import cl from './Footer.module.css';
import logo from '../../components/UI/staticMedia/logoShBig.svg';
import NavFooter from './NavFooter';
import WhiteBtn from '../../components/UI/buttons/WhiteBtn';
import MainH3black from '../../components/UI/hats/MainH3black';
import MidlText from '../../components/UI/other/midlText';
import Telegram from '../../components/UI/staticMedia/telegram.svg';
import WhatsApp from '../../components/UI/staticMedia/whatsapp.svg';
import MainInput from '../../components/UI/other/mainInput';
import axios from 'axios';
import Title from '../../components/UI/hats/Title';
import vk from '../../components/UI/staticMedia/vk.svg';

const Footer = () => {
    const [email, setEmail] = useState('');
    const [response, setRes] = useState('');

    async function Subscribe(e) {
        e.preventDefault();
        await axios.post(process.env.REACT_APP_API_URL + '/api/mail', { email: email }, { withCredentials: true })
            .then(() => {
                setRes('Спасибо что Вы с нами!');
                setEmail('');
            })
            .catch(err => {
                console.log(err);
                setRes('Вы уже подписаны!');
            })
        setTimeout(() => setRes(''), 4000);
    };

    const handleUp = () => {
        window.scrollTo({ top: 0, behavior: 'smooth' });
    };

    return (
        <footer>
            <div className={cl.menuFooter}>
                <div className={cl.logoFooter}>
                    <NavLink to='/' onClick={handleUp}>
                        <img src={logo} alt='Шнайдер интерьер дизайн' />
                    </NavLink>
                    <span>{'“SHNAYDER interior design” ©2023. Все права защищены.'}</span>
                </div>
                <NavFooter />
            </div>
            <div className={cl.menuFooter}>
                <div className={cl.contactFooter}>
                    <div className={cl.navBarFooter}>
                        <span>{'+7 982 780 55 67'}</span>
                        <span>{'shnayder.interior@gmail.com'}</span>
                    </div>
                    <div className={cl.socioFooter}>
                        <a href='https://t.me/mmixcom'
                            target='_blank'
                            rel='noopener noreferrer'>
                            <img src={Telegram} alt='Telegram' />
                        </a>
                        <a href='https://wa.me/+79827805567'
                            target='_blank'
                            rel='noopener noreferrer'>
                            <img src={WhatsApp} alt='WhatsApp' />
                        </a>
                        <a href='https://vk.com/public221572369'
                            target='_blank'
                            rel='noopener noreferrer'>
                            <img src={vk} alt='VK' />
                        </a>
                    </div>
                </div>
                <div className={cl.textFooter}>
                    <MainH3black>{'Оставайтесь на связи'}</MainH3black>
                    <MidlText>
                        {'Эксклюзивный контент, советы, тенденции, акционные предложения и много другое.'}
                    </MidlText>
                    <form onSubmit={Subscribe}>
                        {response ?
                            <Title variant={'green16'} wrapper={'shrink'}>{response}</Title> :
                            <MainInput type='email'
                                maxLength='255'
                                value={email}
                                onChange={e => setEmail(e.target.value)}
                                placeholder='* E-mail' />}
                        <WhiteBtn disabled={!email}>{'подписаться'}</WhiteBtn>
                    </form>
                </div>
            </div>
        </footer>
    );
};

export default Footer;