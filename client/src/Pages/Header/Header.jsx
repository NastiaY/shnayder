import React, { useContext, useState } from 'react';
import cl from './Header.module.css';
import WhiteBtn from '../../components/UI/buttons/WhiteBtn';
import { NavLink, useNavigate } from 'react-router-dom';
import logo from '../../components/UI/staticMedia/logoMini.svg';
import NavHeader from './NavHeader';
import StateModals from '../Modal/StateModals';
import { AuthContext } from '../../context';
import Out from '../../components/UI/staticMedia/OutBlack.svg';
import myAxi from '../../service/interceptor';
import MenuBtnTop from '../../components/UI/buttons/MenuBtnTop';
import MenuBtnBot from '../../components/UI/buttons/MenuBtnBot';
import AddProject from '../Modal/AddProject';
import AddOrderAdmin from '../Modal/AddOrderAdmin';


const Header = () => {

    const [stateMenu, setMenu] = useState(true);
    const { isUser, isAuth, setIsUser, setIsAuth } = useContext(AuthContext);

    const router = useNavigate();

    const Login = (e) => {
        e.preventDefault();
        StateModals.setComeIn(true);
    }

    async function LogOut(e) {
        e.preventDefault();
        await myAxi.post('/logout')
            .then(() => {
                setIsUser({});
                setIsAuth(false);
                router('/');
            })
            .catch(err => {
                console.error(err);
            });
    };

    return (
        <header>

            <AddProject/>
            
            <AddOrderAdmin />
            
            <div className={cl.headerShort}>

                <div className={cl.onMain}>

                    <div className={cl.humburger} onClick={() => setMenu(!stateMenu)}>
                        <span className={stateMenu ? cl.humTop : cl.humTopAct}></span>
                        <span className={stateMenu ? cl.humMid : cl.humMidAct}></span>
                        <span className={stateMenu ? cl.humBot : cl.humBotAct}></span>
                    </div>

                    <NavLink to='/' className={({ isActive }) => isActive ? [cl.main, cl.active].join(' ') : cl.main}>на главную</NavLink>

                </div>

                <NavLink to='/' className={cl.headerLogo}><img src={logo} alt='logoMini' /></NavLink>

                {isAuth ?
                    <div className={cl.loginBar}>

                        <MenuBtnTop to='/brif'>КОНСТРУКТОР</MenuBtnTop>
                        
                        {isUser.role &&
                            <MenuBtnBot to='/adminpanel/users'>Администрирование</MenuBtnBot>}
                        
                        <img src={Out} alt='Выйти' onClick={e => LogOut(e)} />
                    
                    </div>
                    : <WhiteBtn onClick={e => Login(e)}>ВОЙТИ</WhiteBtn>}

            </div>

            <NavHeader setMenu={setMenu} stateMenu={stateMenu} />

        </header>
    );
};

export default Header;