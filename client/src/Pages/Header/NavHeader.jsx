import React from 'react';
import cl from './Header.module.css';
import MenuTop from '../../components/UI/buttons/MenuBtnTop';
import MenuBot from '../../components/UI/buttons/MenuBtnBot';


const NavHeader = ({ setMenu, stateMenu }) => {

    return (
        <div className={stateMenu ? [cl.navBarHeader, cl.notAct].join(' ') : cl.navBarHeader} onClick={() => setMenu(true)}>
            
            <span></span>

            <div className={cl.navbar}>
                <MenuTop to='/aboutus'>дизайнер</MenuTop>
                <MenuTop to='/project'>проекты</MenuTop>
                <MenuTop to='/cost'>стоимость</MenuTop>
            </div>

            <div className={cl.navbar}>
                <MenuBot to='/reviews'>отзывы</MenuBot>
                <MenuBot to='/collaborate'>сотрудничество</MenuBot>
                <MenuBot to='/contacts'>контакты</MenuBot>
            </div>
            
        </div>
    );
};

export default NavHeader;