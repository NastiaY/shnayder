import React, {useContext, useEffect, useState} from 'react';
import axios from 'axios';
import {AuthContext} from '../../context';
import {observer} from 'mobx-react-lite';
import {NavLink} from 'react-router-dom';
import cl from './Main.module.css';
import MainH2 from '../../components/UI/hats/MainH2';
import MainH3 from '../../components/UI/hats/MainH3';
import Plus from '../../components/UI/staticMedia/Plus.svg';
import Title from '../../components/UI/hats/Title';

const ReviewsMini = () => {
    const {isAuth} = useContext(AuthContext);

    const [review, setReview] = useState([]);

    useEffect(() => {
        axios.get(process.env.REACT_APP_API_URL + '/api/review', {params: {limit: 10, status: true}})
            .then(res => setReview(res.data))
            .catch(err => console.log(err));

    }, []);

    const handleUp = () => {
        window.scrollTo({top: 0, behavior: 'smooth'});
    };

    return (
        <div className={cl.gallery}>
            <NavLink to='/reviews' style={{textDecoration: 'none'}} onClick={handleUp}>
                <div className={cl.hat}>
                    <MainH3>{'Отзывы'}</MainH3>
                    <MainH2>{'Что говорят клиенты'}</MainH2>
                </div>
            </NavLink>
            <div className={cl.reviewMainBlock}>
                <div className={cl.blockScrollReview}>
                    {isAuth && (
                        <NavLink to='/reviews' className={cl.reviewMini}>
                            <img src={Plus} alt='Написать отзыв' />
                        </NavLink>
                    )}
                    {review.map(post => {
                        return (
                            <NavLink key={post.id} className={cl.reviewMini} to='/reviews'>
                                <span className={cl.textReviewMini}>{post.message}</span>
                                <Title variant={'gray14'} style={{marginLeft: 'auto'}}>
                                    {`Автор ${post.firstName + ' ' + post.surName[0]}.`}
                                </Title>
                            </NavLink>
                        )
                    })}
                </div>
            </div>
        </div>
    );
};

export default observer(ReviewsMini);