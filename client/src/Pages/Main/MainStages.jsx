import React, { useEffect, useState } from 'react';
import cl from './Main.module.css';
import MainH2 from '../../components/UI/hats/MainH2';
import MainH3 from '../../components/UI/hats/MainH3';
import StageWorking from '../../components/StageWorking';
import cloud from '../../service/cloud';

const MainStages = () => {
    const [urlStatic, setUrlStatic] = useState('');

    useEffect(() => {
        cloud.getOneUrl('static', 'static_comp_1.svg').then(res => setUrlStatic(res));
    }, []);

    return (
        <div className={cl.gallery}>
            <div className={cl.hat}>
                <MainH3>{'этапы работы'}</MainH3>
                <MainH2>{'Прозрачность на каждом этапе'}</MainH2>
            </div>
            <div className={cl.fotoStages}>
                <img src={urlStatic} alt='' className={cl.imgFotoStages}/>
                <div className={cl.infoStages}>
                    <StageWorking
                        stage={'01'}
                        title={'Обращение\nк дизайнеру'}
                    />
                    <StageWorking
                        stage={'02'}
                        title={'Разработка проекта'}
                    />
                    <StageWorking
                        stage={'03'}
                        title={'Ремонт, авторский надзор'}
                    />
                    <StageWorking
                        stage={'04'}
                        title={'Новоселье'}
                    />
                </div>
            </div>
        </div>
    );
};

export default MainStages;