import React from 'react';
import cl from './Comp.module.css';
import MainH3black from '../../../components/UI/hats/MainH3black';
import MidlText from '../../../components/UI/other/midlText';
import CompState from './CompState';


const Specific = () => {

    return (
        <div className={cl.mainLay}>

            <img src={CompState.compositionUrls[4]} alt='' className={cl.imgLayout} />

            <div className={cl.infoLay}>

                <div className={cl.block}>
                    <MainH3black>спецификация</MainH3black>
                    <MidlText>В спецификации указываются основные
                        параметры материалов и мебели, которые
                        содержат фабрику, модель, габариты, материал,
                        цвет и артикул изделий. При этом каждая
                        позиция сопровождается иллюстрациями.
                    </MidlText>
                </div>

            </div>

        </div>
    );
};

export default Specific;