import React from 'react';
import cl from './Comp.module.css';
import MainH3black from '../../../components/UI/hats/MainH3black';
import MidlText from '../../../components/UI/other/midlText';
import CompState from './CompState';


const Visual3D = () => {

    return (
        <div className={cl.mainLay}>

            <img src={CompState.compositionUrls[2]} alt='' className={cl.imgLayout} />

            <div className={cl.infoLay}>

                <div className={cl.block}>
                    <MainH3black>Визуализация помещений</MainH3black>
                    <MidlText>Детализация и дальнейшая проработка интерьера:
                        корректируется масштаб и размер мебели,
                        прорабатывается геометрия потолков, дополняются
                        как инженерно-технические предметы (свет, розетки,
                        выключатели, быт.техника), так и элементы декора
                        с текстилем - арт-объекты (вазы, картины, статуи),
                        шторы, подушки, техника и тд. Настраивается текстура
                        материалов отделки.<br/><br/>
                        Результат - качественная визуализация, которая
                        должна с высокой точностью показать будущий
                        интерьер.</MidlText>
                </div>

            </div>

        </div>
    );
};

export default Visual3D;