import React from 'react';
import cl from './Comp.module.css';
import MidlText from '../../../components/UI/other/midlText';
import MainH3black from '../../../components/UI/hats/MainH3black';
import CompState from './CompState';
import { observer } from 'mobx-react-lite';


const Layout = () => {

    return (
        <div className={cl.mainLay}>

                <img src={CompState.compositionUrls[0]} alt='' className={cl.imgLayout}/>

            <div className={cl.infoLay}>

                <div className={cl.block}>
                    <MainH3black>Планировочное решение</MainH3black>
                    <MidlText>На основе технического задания предлагаются
                        варианты планировочного решения.
                        Из представленных решений вы выбираете
                        понравившиеся, вносите свои комментарии
                        и дополнения. В итоге получается идеальное
                        решение проектируемого помещения, где
                        учтены все ваши потребности.</MidlText>
                </div>

                <div className={cl.block}>
                    <MainH3black>Что входит</MainH3black>
                    <ol>
                        <li className={cl.blockLi}>Обмер помещения</li>
                        <li className={cl.blockLi}>Составление технического задания</li>
                        <li className={cl.blockLi}>Планировочное решение</li>
                    </ol>
                </div>

            </div>

        </div>
    );
};

export default observer(Layout);