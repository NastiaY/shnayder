import { makeAutoObservable } from 'mobx';


class CompState {

    compositionUrls = ['', '', '', '', ''];

    constructor() {
        makeAutoObservable(this);
    };

    setComp(ind, url) {
        this.compositionUrls[ind] = url;
    };

};

export default new CompState();