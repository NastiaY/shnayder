import React from 'react';
import cl from './Comp.module.css';
import MainH3black from '../../../components/UI/hats/MainH3black';
import MidlText from '../../../components/UI/other/midlText';
import CompState from './CompState';


const Concept = () => {

    return (
        <div className={cl.mainLay}>

            <div className={cl.infoLayLeft}>

                <div className={cl.block}>
                    <MainH3black>концепция дизайн-проекта</MainH3black>
                    <MidlText>На основе брифа и собранных материалов,
                        дизайнер готовит концепцию оформления
                        гостиной, кухни, столовой, спальни, гостевых,
                        детских. Это дает возможность еще на стадии
                        технического задания определить цвета,
                        материалы, наполнение и атмосферу
                        интерьеров, сократить время дизайна.</MidlText>
                </div>

                <div className={cl.block}>
                    <MainH3black>Что входит</MainH3black>
                    <ol>
                        <li className={cl.blockLi}>Коллажи помещений</li>
                        <li className={cl.blockLi}>Подбор материалов, цветовых решений</li>
                    </ol>
                </div>

            </div>

            <img src={CompState.compositionUrls[1]} alt='' className={cl.imgLayout} />

        </div>
    );
};

export default Concept;