import React from 'react';
import cl from './Comp.module.css';
import MainH3black from '../../../components/UI/hats/MainH3black';
import MidlText from '../../../components/UI/other/midlText';
import CompState from './CompState';


const DocProject = () => {

    return (
        <div className={cl.mainLay}>

            <div className={cl.infoLayLeft}>

                <div className={cl.block}>
                    <MainH3black>строительные чертежи</MainH3black>
                    <MidlText>После подбора наполнения и материалов
                        для ремонта составляется альбом чертежей
                        и комплект документов, по которым строители
                        могут приступать к работе.</MidlText>
                </div>

                <div className={cl.block}>
                    <MainH3black>Что входит</MainH3black>
                    <ol>
                        <li className={cl.blockLi}>Обмерный план</li>
                        <li className={cl.blockLi}>План демонтажных/монтажных работ</li>
                        <li className={cl.blockLi}>План расстановки мебели</li>
                        <li className={cl.blockLi}>План сантехники</li>
                        <li className={cl.blockLi}>План электрики/размещение розеток и выключателей</li>
                        <li className={cl.blockLi}>План потолков/напольных покрытий</li>
                        <li className={cl.blockLi}>Развертки стен</li>
                    </ol>
                </div>

            </div>

            <img src={CompState.compositionUrls[3]} alt='' className={cl.imgLayout} />

        </div>
    );
};

export default DocProject;