import React, { useEffect } from 'react';
import cl from './Main.module.css';
import SlideShow from './SlideShow';
import MainGallery from './MainGallery';
import MainStages from './MainStages';
import MainComp from './MainComp';
import ReviewsMini from './ReviewsMini';
import OrderProject from './OrderProject';
import cloud from '../../service/cloud';
import CompState from './CompProject/CompState';


const MainFull = () => {

    useEffect(() => {
        async function load() {
            await cloud.getOneUrl('static', 'layout.png').then(res => CompState.setComp(0, res));
            await cloud.getOneUrl('static', 'concept.png').then(res => CompState.setComp(1, res));
            await cloud.getOneUrl('static', '3d_visual.png').then(res => CompState.setComp(2, res));
            await cloud.getOneUrl('static', 'doc_project.png').then(res => CompState.setComp(3, res));
            await cloud.getOneUrl('static', 'specific.png').then(res => CompState.setComp(4, res));
        }
        load();
    }, [])

    return (
        <div className={cl.mainFon}>
            <SlideShow />
            <div className={cl.content}>
                <MainGallery />
                <MainStages />
                <MainComp />
                <OrderProject />
                <ReviewsMini />
            </div>
        </div>
    );
};

export default MainFull;