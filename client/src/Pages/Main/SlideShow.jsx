import React, { useEffect, useState } from 'react';
import cl from './Main.module.css';
import cloud from '../../service/cloud';


const SlideShow = () => {

    const [indSlide, setIndSlide] = useState(0);

    const [urls, setUrls] = useState([]);

    const nextSlide = () => {
        setIndSlide(indSlide === urls.length - 1 ? 0 : indSlide + 1);
    };

    useEffect(() => {
        cloud.getUrls('slides').then(res => setUrls(res));
    }, [])

    useEffect(() => {
        const interval = setInterval(() => {
            nextSlide();
        }, 4000);
        return () => clearInterval(interval);
    }, [indSlide]);
    

    return (
        <div className={cl.slideShow}>

            <ul className={cl.dots}>

                {urls.map((url, ind) => (
                    <li className={ind === indSlide ? [cl.dot, cl.select].join(' ') : cl.dot}
                        value={ind}
                        key={ind}
                        onClick={() => setIndSlide(ind)}></li>
                ))}

            </ul>

            <div className={cl.slides}>

                {urls.map((url, ind) => (
                    <div className={ind === indSlide ? [cl.slide, cl.act].join(' ') : cl.slide}
                        key={url}>
                        <img src={url} alt='' className={cl.imgSlides}/>
                    </div>
                ))}

            </div>

        </div>
    );
};

export default SlideShow;