import React, { useContext, useState } from 'react';
import cl from './Main.module.css';
import MainInput from '../../components/UI/other/mainInput';
import MainH3black from '../../components/UI/hats/MainH3black';
import GreenBtn from '../../components/UI/buttons/GreenBtn';
import SmallText from '../../components/UI/other/smallText';
import SmallTextBlack from '../../components/UI/other/smallTextBlack';
import { observer } from 'mobx-react-lite';
import { NavLink, useNavigate } from 'react-router-dom';
import Time from '../../components/UI/other/time';
import Area from '../../components/UI/other/area';
import BrifState from '../Other/BrifState';
import MainH4 from '../../components/UI/hats/MainH4';
import StateModals from '../Modal/StateModals';
import { AuthContext } from '../../context';
import MenuBtnTop from '../../components/UI/buttons/MenuBtnTop';
import axios from 'axios';
import WhiteBtn from '../../components/UI/buttons/WhiteBtn';


const OrderProject = () => {

    let now = new Date();

    const [valCustomer, setValCustomer] = useState({ fio: '', phone: '', email: '', createdAt: now.toISOString(), updatedAt: now.toISOString() });

    const { isAuth, isUser } = useContext(AuthContext);

    const handleUp = () => {
        window.scrollTo({ top: 0, behavior: 'smooth' });
    };

    const Login = (e) => {
        e.preventDefault();
        StateModals.setComeIn(true);
    };

    async function sendCalc(e) {
        e.preventDefault();
        StateModals.setSuccessModal(true);
        try {
            const newOrder = { ...valCustomer, brif: { area: BrifState.area * 30 + 20 } }
            await axios.post(process.env.REACT_APP_API_URL + '/api/orders', newOrder);
            setValCustomer({ fio: '', phone: '', email: '', createdAt: now.toISOString(), updatedAt: now.toISOString() });
            BrifState.setArea(5);
            setTimeout(() => StateModals.setSuccessModal(false), 3000);
        } catch (err) {
            console.log(err);
        };
    };

    return (
        <div className={cl.OrderBlock}>

            <form onSubmit={e => sendCalc(e)} className={cl.formOrder}>

                <div className={cl.blockMiniBrif}>

                    <MainH3black style={{ width: '180px' }}>РАССЧИТАТЬ СРОКИ РЕАЛИЗАЦИИ</MainH3black>

                    <MainH4>ПЛОЩАДЬ ПОМЕЩЕНИЯ</MainH4>

                    <Area />

                    <MainH4>СРОКИ</MainH4>

                    <Time />

                </div>

                <div className={cl.blockMiniBrif}>

                    <MainH3black>ОБСУДИМ ПРОЕКТ?</MainH3black>

                    <SmallTextBlack style={{ marginBottom: '29px' }}>
                        Введите свои актуальные данные, дизайнер
                        свяжется с Вами в ближайшее время</SmallTextBlack>

                    <MainInput type='text'
                        maxLength='255'
                        value={valCustomer.fio}
                        onChange={e => setValCustomer({ ...valCustomer, fio: e.target.value.replace(/[^A-za-zА-яа-я_-\s]/g, '') })}
                        placeholder='* ФИО (Иванов Иван Иванович)' />

                    <MainInput type='text'
                        maxLength='50'
                        value={valCustomer.phone}
                        onChange={e => setValCustomer({ ...valCustomer, phone: e.target.value.replace(/[^-+.e\d]/g, '') })}
                        placeholder='* Ваш телефон' />

                    <MainInput type='email'
                        maxLength='255'
                        value={valCustomer.email}
                        onChange={e => setValCustomer({ ...valCustomer, email: e.target.value })}
                        placeholder='* E-mail' />

                    <div className={cl.blockSend}>

                        <GreenBtn
                            disabled={(!valCustomer.fio + !valCustomer.phone + !valCustomer.email)}>
                            отправить</GreenBtn>

                        <NavLink to='/policy' style={{ textDecoration: 'none' }}><SmallText >Политика конфеденциальности</SmallText></NavLink>

                    </div>

                </div>

            </form>

            {!isAuth ? (

                <div className={cl.authHat}>
                    <MainH3black>Войдите в аккаунт</MainH3black>
                    <span >чтобы воспользоваться расширенным конструктором проекта,
                        оставить отзыв,<br /> скачать форму договора и спросить совета дизайнера!</span>
                    <WhiteBtn onClick={e => Login(e)}>ВОЙТИ</WhiteBtn>
                </div>

            ) : (

                <div className={cl.welcome}>
                    <MainH3black>Добро пожаловать {isUser.firstName} {isUser.secondName}!</MainH3black>
                    <span>Переходите</span>
                    <MenuBtnTop to='/brif' onClick={handleUp}>В КОНСТРУКТОР</MenuBtnTop>
                    <span>и воспользуйтесь расширенными опциями!</span>

                </div>

            )}
            
        </div>
    );
};

export default observer(OrderProject);