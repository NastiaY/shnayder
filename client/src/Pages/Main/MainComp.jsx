import React, { useState } from 'react';
import cl from './Main.module.css';
import MainH2 from '../../components/UI/hats/MainH2';
import MainH3 from '../../components/UI/hats/MainH3';
import GreenLink from '../../components/UI/buttons/GreenLink';
import { Route, Routes } from 'react-router-dom';
import { compRoutes } from '../../Routes/routes';


const MainComp = () => {

    const [activeMenu, setActiveMenu] = useState(0);

    return (
        <div className={cl.gallery}>

            <div className={cl.hat} style={{ height: '210px', padding: '0px 0px 43px' }}>
                <MainH3>состав проекта</MainH3>
                <MainH2>Что входит в дизайн-проект</MainH2>

                <div className={cl.menuStages}>
                    <GreenLink onClick={() => setActiveMenu(0)} to='/'>Планировка</GreenLink>
                    <GreenLink onClick={() => setActiveMenu(203)} to='/concept'>Концепция</GreenLink>
                    <GreenLink onClick={() => setActiveMenu(460)} to='/visual'>3D-визуализация</GreenLink>
                    <GreenLink onClick={() => setActiveMenu(734)} to='/doc'>Документация</GreenLink>
                    <GreenLink onClick={() => setActiveMenu(979)} to='/spec'>Спецификация</GreenLink>
                </div>
            </div>

            <div className={cl.lineMini} style={{ left: `${activeMenu}px` }}></div>

            <Routes>
                {compRoutes.map(route =>
                    <Route
                        element={route.component}
                        path={route.path}
                        exact={route.exact}
                        key={route.path} />)}
            </Routes>
        </div>
    );
};

export default MainComp;