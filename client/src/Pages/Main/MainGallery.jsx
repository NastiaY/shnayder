import React, { useEffect, useState } from 'react';
import cl from './Main.module.css';
import MainH2 from '../../components/UI/hats/MainH2';
import MainH3 from '../../components/UI/hats/MainH3';
import MainH1 from '../../components/UI/hats/MainH1';
import GreenBtn from '../../components/UI/buttons/GreenBtn';
import { NavLink } from 'react-router-dom';
import PrState from '../Other/ProjectsState';
import servService from '../../service/servService';
import { observer } from 'mobx-react-lite';


const MainGallery = () => {

    const [indPrjct, setIndPrjct] = useState(0);

    useEffect(() => {
        servService.getProjectsGallery();
    }, []);

    return (
        <div className={cl.gallery}>

            <NavLink to='/project' style={{ textDecoration: 'none' }}>
                <div className={cl.hat}>
                    <MainH3>проекты</MainH3>
                    <MainH2>Галерея дизайн-проектов</MainH2>
                </div>
            </NavLink>

            {PrState.progectGallery.length > 0 &&

                <div className={cl.slideGallery}>

                    <div className={cl.infoGallery}>

                        <div className={cl.titelGallery}>

                            <MainH3>Реализованные проекты</MainH3>

                            <div className={cl.infoPrjct}>
                                <MainH1>{PrState.progectGallery[indPrjct].title}<br/>г. {PrState.progectGallery[indPrjct].city}</MainH1>
                            </div>

                        </div>

                        <NavLink to={`/project/${PrState.progectGallery[indPrjct].id}`}
                            style={{ textDecoration: 'none' }}><GreenBtn>смотреть проект</GreenBtn></NavLink>

                        <ul className={cl.dotsGal}>

                            {PrState.progectGallery.map((prjct, ind) => (
                                <li className={ind === indPrjct ? [cl.dotGal, cl.select].join(' ') : cl.dotGal}
                                    value={ind}
                                    key={ind}
                                    onClick={() => setIndPrjct(ind)}></li>
                            ))}

                        </ul>

                    </div>

                    <img src={PrState.progectGallery[indPrjct].urls[2]} alt='' className={cl.imgGallery} />

                </div>
            }

        </div>
    );
};

export default observer(MainGallery);