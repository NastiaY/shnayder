import firebase from 'firebase/compat/app';
import 'firebase/compat/storage';


class CloudService {

  constructor() {
    this.firebaseConfig = {
      apiKey: 'AIzaSyC_k0DZM0bWY2O-ZZWWbeFAAdnQmZXpPqg',
      authDomain: 'shnayder-df141.firebaseapp.com',
      projectId: 'shnayder-df141',
      storageBucket: 'shnayder-df141.appspot.com',
      messagingSenderId: '176196237702',
      appId: '1:176196237702:web:edfe27cee2b6302366d407'
    };

    firebase.initializeApp(this.firebaseConfig);

    this.stor = firebase.storage();
  };

  async getUrls(dir) {
    try {
      const res = await this.stor.ref().child(dir).listAll();
      let prom = res.items.map((item) => {
        return item.getDownloadURL();
      });
      return await Promise.all(prom);
    } catch (err) {
      console.log(err);
      return [];
    };
  };

  async getUrlsChild(dir, ch) {
    try {
      const res = await this.stor.ref().child(dir).child(ch).listAll();
      let prom = res.items.map((item) => {
        return item.getDownloadURL();
      });
      return await Promise.all(prom);
    } catch (err) {
      console.log(err);
      return [];
    };
  };

  async getOneUrl(dir, namefile) {
    try {
      const res = await this.stor.ref().child(dir).listAll();
      const fileRef = res.items.find(item => item.name === namefile);
      if (fileRef) {
        return await fileRef.getDownloadURL();
      };
    } catch (err) {
      console.log(err);
      return '';
    };
  };

  async getDoc(e, namefile) {
    e.preventDefault();
    try {
      const res = await this.stor.ref().child('docs').listAll();
      const fileRef = res.items.find(item => item.name === namefile);
      if (fileRef) {
        const downloadUrl = await fileRef.getDownloadURL();
        const link = document.createElement('a');
        link.href = downloadUrl;
        link.download = namefile;
        link.click();
        link.remove();
      };
    } catch (err) {
      console.log(err);
    };
  };

  async deleteChildDirectory(dir, ch) {
    try {
      const chRef = this.stor.ref().child(dir).child(ch);
      const files = await chRef.listAll();
      const delPromises = files.items.map((item) => {
        return item.delete();
      });
      await Promise.all(delPromises);
      await chRef.delete();
      console.log('Папка успешно удалена');
    } catch (err) {
      console.log(err);
    };
  };

  async addNewProject(dir, ch, arrayFoto) {
    try {
      const chRef = this.stor.ref().child(dir).child(ch);
      const upPromises = arrayFoto.map((file, ind) => {
        return chRef.child(`${ind + 1}.png`).put(file);
      });
      await Promise.all(upPromises);
    } catch(err) {
      console.log(err);
    };
  };

  async addNewOrder(dir, ch, arrayFoto) {
    try {
      const chRef = this.stor.ref().child(dir).child(ch);
      const upPromises = arrayFoto.map(file => {
        if (file) return chRef.child(file.name).put(file);
      });
      await Promise.all(upPromises);
    } catch(err) {
      console.log(err);
    };
  };

  async addNewMailImg(dir, img) {
    try {
      const image = await this.stor.ref().child(dir).child(img.name).put(img);
      const urlImg = await image.ref.getDownloadURL();
      return urlImg;
    } catch(err) {
      console.log(err);
    };
  };

};

export default new CloudService();