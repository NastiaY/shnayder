import socketIo from 'socket.io-client';

const socket = socketIo.connect(process.env.REACT_APP_API_URL, {
    secure: true,
    reconnection: true,
    rejectUnauthorized: false
});

export default socket;