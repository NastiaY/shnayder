import myAxi from './interceptor';
import ProjectsState from '../Pages/Other/ProjectsState';
import cloud from './cloud';
import axios from 'axios';
import AdminState from '../Pages/Admin/AdminState';
import StateModals from '../Pages/Modal/StateModals';
import BrifState from '../Pages/Other/BrifState';


class servService {

    async getProjects() {
        try {
            let res = await axios.get(process.env.REACT_APP_API_URL + '/api/projects');
            let dataPr = [];
            for (let item of res.data) {
                let result = await cloud.getUrlsChild('projects', `${item.id}`);
                dataPr = [{ ...item, urls: result }, ...dataPr];
            };
            ProjectsState.setProjects(dataPr);
        } catch (err) {
            console.log(err);
        };
    };

    async getOnlyProjects(data) {
        let res = await axios.get(process.env.REACT_APP_API_URL + '/api/projects', data);
        let dataPr = [];
        for (let item of res.data) {
            let result = await cloud.getUrlsChild('projects', `${item.id}`);
            dataPr = [{ ...item, urls: result }, ...dataPr];
        };
        return [res.headers['total-count'], dataPr];
    };

    async getProjectsGallery() {
        try {
            let res = await axios.get(process.env.REACT_APP_API_URL + '/api/projects');
            let dataPr = [];
            for (let item of res.data) {
                if (item.statusGallery) {
                    let result = await cloud.getUrlsChild('projects', `${item.id}`);
                    dataPr = [{ ...item, urls: result }, ...dataPr];
                };
            };
            ProjectsState.setProjectGallery(dataPr);
        } catch (err) {
            console.log(err);
        };
    };

    async getOneProject(id) {
        try {
            let res = await axios.get(process.env.REACT_APP_API_URL + `/api/projects`, { params: { id: id } });
            let result = await cloud.getUrlsChild('projects', `${id}`);
            return { ...res.data[0], urls: result };
        } catch (err) {
            console.log(err);
        };
    };

    async getOrders() {
        try {
            let res = await myAxi.get('/orders');
            for (let item of res.data) {
                let result = await cloud.getUrlsChild('orders', `${item.id}`);
                AdminState.setOrders({ ...item, urls: result });
            };
        } catch (err) {
            console.log(err);
        };
    };

    async addProject(project) {
        try {
            const NewProject = { ...project, description: JSON.stringify(project.description) };
            const res = await myAxi.post(`/projects`, NewProject);
            await cloud.addNewProject('projects', `${res.data}`, StateModals.fotoNewProject);
            const result = await cloud.getUrlsChild('projects', `${res.data}`);
            ProjectsState.addProject({ ...project, id: res.data, urls: result });
        } catch (err) {
            console.log(err);
        };
    };

    async addOrder(order) {
        try {
            const res = await axios.post(process.env.REACT_APP_API_URL + '/api/orders', order);
            await cloud.addNewOrder('orders', `${res.data}`, BrifState.userFoto);
        } catch (err) {
            console.log(err);
        };
    };
};

export default new servService();