import axios from 'axios';


export const API_URL = `${process.env.REACT_APP_API_URL}/api`;

const myAxi = axios.create({
    withCredentials: true,
    baseURL: API_URL,
});

myAxi.interceptors.request.use((config) => {
    return config;
});

myAxi.interceptors.response.use((config) => {
    return config;
}, async (error) => {
    const origRequest = error.config;
    if (error.response.status == 401 && error.config && !error.config._isRetry) {
        origRequest._isRetry = true;
        try {
            await axios.get(`${API_URL}/auth`, { withCredentials: true})
            return myAxi.request(origRequest);
        } catch(err) {
            console.log('Вы не авторизованы')
        }
    }
});

export default myAxi;