const knexConfig = require('../knexfile.js');
const knex = require('knex')(knexConfig.development);

class ReviewController {

    async getReview(req, res) {
        const { status, limit, offset } = req.query;

        let total = await knex('reviews').count('id');

        let result = knex('reviews')
            .select('reviews.createdAt', 'reviews.message', 'reviews.status', 'reviews.id', 'users.firstName', 'users.surName', 'users.email')
            .join('users', 'users.id', '=', 'reviews.userId')
            .orderBy('createdAt', 'desc');

        if (status) {
            result = result.where('reviews.status', status);
            total = await knex('reviews').count('id').where('status', status);
        };
        if (offset) {
            result = result.offset(offset);
        };
        if (limit) {
            result = result.limit(limit);
        };

        result.then(rows => {
            res.header('total-count', total[0].count)
            return res.status(200).json(rows);
        })
            .catch((err) => {
                res.status(500).send(err);
            });
    };

    addReview(req, res) {
        if (req.body) {
            knex('reviews')
                .returning('id')
                .insert(req.body)
                .then(id => {
                    res.status(200).send(`${id[0].id}`);
                })
                .catch((err) => {
                    res.status(500).send(err);
                });
        } else {
            res.status(400).send('Bad Request. Данные отсутствуют')
        };
    };

    updateReview(req, res) {
        const { id } = req.query;
        const upParams = req.body;

        if (id && upParams) {
            knex('reviews')
                .where('id', id)
                .update(upParams)
                .then(() => {
                    res.status(200).send('Отзыв успешно обновлен');
                })
                .catch((err) => {
                    res.status(500).send(err);
                });
        } else {
            res.status(400).send('Bad Request. Данные отсутствуют')
        };
    };

    delReview(req, res) {
        const { id } = req.query;

        if (id) {
            knex('reviews')
                .delete()
                .where('id', id)
                .then(() => {
                    res.status(201).send(`Отзыв успешно удален`);
                })
                .catch((err) => {
                    res.status(500).send(err);
                });
        } else {
            res.status(400).send('Bad Request. Данные отсутствуют')
        };
    };
};

module.exports = new ReviewController();