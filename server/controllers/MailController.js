require('dotenv').config();
const knexConfig = require('../knexfile.js');
const knex = require('knex')(knexConfig.development);
const MailServ = require('../service/MailServ.js');

class MailController {

    async getEmail(req, res) {
        knex('list')
            .then(rows => {
                res.status(200).send(rows);
            })
            .catch(err => {
                res.status(500).send(err);
            });
    };

    async sendEmail(req, res) {
        try {
            const rows = await knex('list');
            for (let row of rows) {
                await MailServ.sendList(row.email, req.body);
            }
            res.status(200).send('Письма успешно отправлены');
        } catch (err) {
            res.status(500).send(err);
        }
    };

    addEmail(req, res) {
        const { email } = req.body;

        let smallEmail = email.toLowerCase();

        knex('list')
            .where('email', smallEmail)
            .then(rows => {
                if (!rows.length) {
                    knex('list')
                        .insert({ email: smallEmail })
                        .then(() => {
                            res.status(200).send('Email успешно добавлен');
                            MailServ.sendMail(smallEmail);
                        })
                        .catch((err) => {
                            res.status(500).send(err);
                        });
                } else {
                    res.status(409).send('Email уже существует');
                }
            })
            .catch((err) => {
                res.status(500).send(err);
            });
    };

    updateEmail(req, res) {

    };

    deleteEmail(req, res) {
        const { email } = req.query;

        knex('list')
            .delete()
            .where('email', email)
            .then(() => {
                res.status(200).send('Email успешно удален');
            })
            .catch((err) => {
                res.status(500).send(err);
            })
    };

    delEmail(req, res) {
        const { email } = req.params;

        knex('list')
            .delete()
            .where('email', email)
            .then(() => {
                res.redirect(`${process.env.FRONT_URL}/unsubscribe`);
            })
            .catch((err) => {
                res.status(500).send(err);
            })
    };
};

module.exports = new MailController();