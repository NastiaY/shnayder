require('dotenv').config();
const knexConfig = require('../knexfile.js');
const knex = require('knex')(knexConfig.development);
const bcrypt = require('bcryptjs');
const TokenServ = require('../service/TokenServ.js');


class AuthController {

    registration(req, res) {

        const { Login, Pass } = req.body;
        
        let smallLogin = Login.toLowerCase();

        knex('accounts').where('login', smallLogin)
            .then(rows => {
                if (rows.length) {
                    res.status(409).send('Аккаунт таким email уже существует')
                } else {
                    bcrypt.hash(Pass, 10, (err, hash) => {
                        knex('accounts')
                            .insert({ login: smallLogin, password: hash })
                            .then(() => {
                                res.status(201).send('Аккаунт успешно создан')
                            });
                    });
                };
            })
            .catch(err => {
                res.status(500).send(err);
            })
    };

    login(req, res) {

        const { Login, Pass } = req.body;

        let smallLogin = Login.toLowerCase();

        knex('accounts')
            .where('login', smallLogin)
            .select('id', 'password', 'role', 'status')
            .join('users', 'users.email', '=', 'accounts.login')
            .then(async rows => {
                if (rows.length) {
                    if (!rows[0].status) return res.status(403).send('Аккаунт заблокирован');
                    if (await bcrypt.compare(Pass, rows[0].password)) {
                        const { access_token, refresh_token } = TokenServ.generateTokens({ id: rows[0].id, role: rows[0].role });
                        res.cookie('refresh_token', refresh_token, {
                            httpOnly: true,
                            sameSite: 'strict',
                            maxAge: 30 * 24 * 60 * 60 * 1000
                        })
                            .cookie('access_token', access_token, {
                                httpOnly: true,
                                sameSite: 'strict',
                                maxAge: 15 * 60 * 1000
                            });
                        return res.status(200).json({ id: rows[0].id, role: rows[0].role});
                    } else {
                        res.status(402).send(`Неверный пароль`);
                    }
                } else {
                    res.status(401).send(`Неверный email`);
                }
            })
            .catch(err => {
                res.status(500).send(err);
            })
    };

    async authenticateToken(req, res) {

        const refreshToken = req.cookies.refresh_token;

        if (!refreshToken) {
            return res.status(401).send('Токен аутентификации отсутствует');
        };

        const userData = TokenServ.validateRefreshToken(refreshToken);

        if (!userData) {
            return res.status(401).send('Неверный токен аутентификации');
        };

        const status = await knex('users').where('id', userData.id).select('status');

        if (status[0].status) {

            const { access_token, refresh_token } = TokenServ.generateTokens({ id: userData.id, role: userData.role });

            res.cookie('refresh_token', refresh_token, {
                httpOnly: true,
                sameSite: 'strict',
                maxAge: 30 * 24 * 60 * 60 * 1000
            })
                .cookie('access_token', access_token, {
                    httpOnly: true,
                    sameSite: 'strict',
                    maxAge: 15 * 60 * 1000
                });
            return res.status(200).json({ id: userData.id, role: userData.role });
        } else {
            res.clearCookie('refresh_token');
            res.clearCookie('access_token');
            return res.status(403).send('Аккаунт заблокирован');
        }
        
    };

    async logout(req, res) {
        try {
            res.clearCookie('refresh_token');
            res.clearCookie('access_token');
            return res.status(200).send('Вы успешно вышли');
        } catch (err) {
            res.status(500).send(err);
        }
    };
}

module.exports = new AuthController();