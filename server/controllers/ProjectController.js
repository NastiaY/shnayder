const knexConfig = require('../knexfile.js');
const knex = require('knex')(knexConfig.development);


class ProjectController {

    async getProjects(req, res) {

        const { id, type, limit, offset } = req.query;

        let result = knex('projects').orderBy('id', 'asc');

        let total = await knex('projects').count('id');

        if (id) {
            result = result.where('id', id);
        };
        if (type) {
            result = result.where('type', type);
        };
        if (offset) {
            result = result.offset(offset);
        };
        if (limit) {
            result = result.limit(limit);
        };

        result.then(rows => {
            res.header('total-count', total[0].count)
            return res.status(200).json(rows);
        })
            .catch((err) => {
                res.status(500).send(err);
            });
    };

    addProject(req, res) {

        if (req.body) {
            knex('projects')
                .returning('id')
                .insert(req.body)
                .then(id => {
                    res.status(200).send(`${id[0].id}`);
                })
                .catch((err) => {
                    res.status(500).send(err);
                });
        } else {
            res.status(400).send('Bad Request. Данные отсутствуют')
        };
    };

    updateProject(req, res) {

        const { id } = req.query;
        const upParams = req.body;

        if (id && upParams) {
            knex('projects')
                .where('id', id)
                .update(upParams)
                .then(() => {
                    res.status(200).send('Проект успешно обновлен');
                })
                .catch((err) => {
                    res.status(500).send(err);
                });
        } else {
            res.status(400).send('Bad Request. Данные отсутствуют')
        };
    };

    delProject(req, res) {

        const { id } = req.query;

        if (id) {
            knex('projects')
                .delete()
                .where('id', id)
                .then(() => {
                    res.status(201).send(`Проект успешно удален`);
                })
                .catch((err) => {
                    res.status(500).send(err);
                });
        } else {
            res.status(400).send('Bad Request. Данные отсутствуют')
        };
    };
};

module.exports = new ProjectController();