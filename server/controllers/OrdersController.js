const knexConfig = require('../knexfile.js');
const knex = require('knex')(knexConfig.development);
const MailServ = require('../service/MailServ.js');


class OrdersController {

    async getOrders(req, res) {

        const { email, limit, offset } = req.query;

        let result = knex('orders').orderBy('id', 'desc');

        let total = await knex('orders').count('id');

        if (email) {
            result = result.where('email', email);
        };
        if (offset) {
            result = result.offset(offset);
        };
        if (limit) {
            result = result.limit(limit);
        };

        result.then(rows => {
            res.header('total-count', total[0].count)
            return res.status(200).json(rows);
        })
            .catch((err) => {
                res.status(500).send(err);
            });
    };

    addOrder(req, res) {

        if (req.body) {
            knex('orders')
                .returning('id')
                .insert(req.body)
                .then(id => {
                    MailServ.sendOrderMail(id[0].id, req.body);
                    res.status(200).send(`${id[0].id}`);
                })
                .catch((err) => {
                    res.status(500).send(err);
                });
        } else {
            res.status(400).send('Bad Request. Данные отсутствуют')
        };
    };

    updateOrder(req, res) {

        const { id } = req.query;
        const upParams = req.body;

        if (id && upParams) {
            knex('orders')
                .where('id', id)
                .update(upParams)
                .then(() => {
                    res.status(200).send('Заявка успешно обновлена');
                })
                .catch((err) => {
                    res.status(500).send(err);
                });
        } else {
            res.status(400).send('Bad Request. Данные отсутствуют')
        };
    };

    delOrder(req, res) {

        const { id } = req.query;

        if (id) {
            knex('orders')
                .delete()
                .where('id', id)
                .then(() => {
                    res.status(201).send(`Заявка успешно удалена`);
                })
                .catch((err) => {
                    res.status(500).send(err);
                });
        } else {
            res.status(400).send('Bad Request. Данные отсутствуют')
        };
    };
};

module.exports = new OrdersController();