const knexConfig = require('../knexfile.js');
const knex = require('knex')(knexConfig.development);

class ChatController {

    getChat(req, res) {
        const { id } = req.query;

        if (id) {
            knex('messenger')
                .where('idRoom', id)
                .select('*')
                .orderBy('createdAt', 'asc')
                .then(rows => {
                    res.status(200).json(rows)
                })
                .catch(err => {
                    res.status(500).send(err);
                })
        } else {
            res.status(400).send('Bad Request. Данные отсутствуют');
        };
    };


    postMsg(req, res) {
        if (req.body) {
            knex('messenger').insert(req.body)
                .then(() => {
                    res.status(200).send('Сообщение успешно сохранено');
                })
                .catch(err => {
                    res.status(500).send(err);
                })
        } else {
            res.status(400).send('Bad Request. Данные отсутствуют');
        };
    };

    getAllRoom(req, res) {
        knex('messenger')
        .select('idRoom', 'email', 'firstName', 'surName', 'secondName')
        .sum({state: knex.raw('case when state = false then 1 else 0 end')})
        .join('users', 'users.id', '=', 'messenger.idRoom')
        .where('users.status', '!=', false)
        .groupBy('idRoom', 'email', 'firstName', 'surName', 'secondName')
        .then(rows => {
            res.status(200).json(rows)
        })
        .catch(err => {
            res.status(500).send(err);
        })
    };

    updateMsg(req, res) {
        const { id } = req.query; 

        knex('messenger')
        .where('idRoom', id)
        .where('state', false)
        .update({state: true})
        .then(() => {
            res.status(200).send('Сообщения успешно обновлены');
        })
        .catch(err => {
            res.status(500).send(err);
        })
    };
};

module.exports = new ChatController();