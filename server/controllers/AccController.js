require('dotenv').config();
const knexConfig = require('../knexfile.js');
const knex = require('knex')(knexConfig.development);
const bcrypt = require('bcryptjs');
const MailServ = require('../service/MailServ.js');
const uuid = require('uuid');


class AccController {

    postEmail(req, res) {

        const { email } = req.body;

        let smallLogin = email.toLowerCase();

        knex('accounts')
            .where('login', smallLogin)
            .then(rows => {
                if (rows.length) {
                    const resetLink = uuid.v4();
                    knex('accounts')
                        .where('login', smallLogin)
                        .update({ link: resetLink })
                        .then(() => {
                            MailServ.sendLinkReset(smallLogin, resetLink);
                            res.status(200).send('Письмо отправлено');
                        })
                } else {
                    res.status(409).send('Аккаунт с таким email не зарегистрирован');
                }
            })
            .catch(err => {
                res.status(500).send(err);
            })
    };

    updatePass(req, res) {

        const { link, pass } = req.body;

        knex('accounts')
            .where('link', link)
            .then(rows => {
                if (rows.length) {
                    bcrypt.hash(pass, 10, (err, hash) => {
                        knex('accounts')
                            .update({ link: null, password: hash })
                            .then(() => {
                                res.status(200).send('Пароль успешно обновлен.');
                            });
                    })
                } else {
                    res.status(409).send('Ссылка устарела.');
                }
            })
            .catch(err => {
                res.status(500).send(err);
            });
    };
};

module.exports = new AccController();