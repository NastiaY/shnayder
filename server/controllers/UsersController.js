const knexConfig = require('../knexfile.js');
const knex = require('knex')(knexConfig.development);


class UsersController {

    async getUsers(req, res) {

        const { email, limit, offset } = req.query;

        let result = knex('users')
            .orderBy('surName', 'asc')
            .orderBy('id', 'asc');

        let total = await knex('users').count('id');

        if (email) {
            result = result.where('email', email);
        };
        if (offset) {
            result = result.offset(offset);
        };
        if (limit) {
            result = result.limit(limit);
        };

        result.then(rows => {
            res.header('total-count', total[0].count)
            return res.status(200).json(rows);
        })
            .catch((err) => {
                res.status(500).send(err);
            });
    };

    async getUser(req, res) {

        const id = req.params.id;

        if (id) {
            knex('users')
                .select('firstName', 'surName', 'secondName', 'email', 'phone')
                .where('id', id)
                .then(rows => {
                    return res.status(200).json(rows);
                })
                .catch((err) => {
                    res.status(500).send(err);
                });
        } else {
            res.status(400).send('Bad Request. Данные отсутствуют')
        }
    };

    addUser(req, res) {

        const newUser = req.body;

        if (newUser) {
            knex('users')
                .insert(newUser)
                .then(() => {
                    res.status(200).send(`Пользователь успешно добавлен`);
                })
                .catch((err) => {
                    res.status(500).send(err);
                });
        } else {
            res.status(400).send('Bad Request. Данные отсутствуют')
        };
    };

    updateUser(req, res) {

        const { id } = req.query;
        const upParams = req.body;

        if (id && upParams) {
            knex('users')
                .where('id', id)
                .update(upParams)
                .then(() => {
                    res.status(200).send('Пользователь успешно обновлен');
                })
                .catch((err) => {
                    res.status(500).send(err);
                });
        } else {
            res.status(400).send('Bad Request. Данные отсутствуют')
        };
    };

    delUser(req, res) {
        
        const { id } = req.query;

        if (id) {
            knex('users')
                .delete()
                .where('id', id)
                .then(() => {
                    res.status(201).send(`Пользователь успешно удален`);
                })
                .catch((err) => {
                    res.status(500).send(err);
                });
        } else {
            res.status(400).send('Bad Request. Данные отсутствуют')
        };
    };
};

module.exports = new UsersController();