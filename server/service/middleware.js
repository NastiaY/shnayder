const TokenServ = require('./TokenServ');
const knexConfig = require('../knexfile.js');
const knex = require('knex')(knexConfig.development);


module.exports = async function (req, res, next) {
    try {
        const accessToken = req.cookies.access_token;
        if (!accessToken) {
            return res.status(401).send('Токен аутентификации отсутствует');
        }

        const userData = TokenServ.validateAccessToken(accessToken);
        if (!userData) {
            return res.status(401).send('Неверный токен аутентификации');
        }

        const status = await knex('users').where('id', userData.id).select('status');
        if (status[0].status) {
            req.user = userData;
            next();
        } else {
            res.clearCookie('refresh_token');
            res.clearCookie('access_token');
            return res.status(403).send('Аккаунт заблокирован');
        }
    } catch (err) {
        return res.status(401).send('Пользователь не авторизован');
    };
};