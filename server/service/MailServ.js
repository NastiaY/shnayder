require('dotenv').config();
const nodemailer = require('nodemailer');


class MailServ {

    constructor() {
        this.transporter = nodemailer.createTransport({
            service: 'gmail',
            auth: {
                user: process.env.SMTP_USER,
                pass: process.env.SMTP_PASS
            },
            debug: true
        })
    };

    async sendMail(to) {

        await this.transporter.sendMail({
            from: process.env.SMTP_USER,
            to: to,
            subject: 'Добро пожаловать на SHNAYDER interior design',
            text: 'SHNAYDER interior design',
            html:
                `<div style='font-family: Montserrat, sans-serif'>
                    <h2>Добро пожаловать!</h2>
                    <h4>Спасибо за подписку на рассылку. Здесь мы будем делиться новостями,
                        уникальными предложениями и полезной информацией.</h4>
                    <a href=${process.env.FRONT_URL}><img width='147px' src='https://firebasestorage.googleapis.com/v0/b/shnayder-df141.appspot.com/o/static%2FlogoMini.png?alt=media&token=c687c667-a3ad-4145-b1c8-2670506c872c'/></a>
                </div>`
        })
    };

    async sendOrderMail(id, data) {

        let now = new Date();

        await this.transporter.sendMail({
            from: process.env.SMTP_USER,
            to: process.env.EMAIL_ADMIN,
            subject: 'Новая заявка',
            text: 'SHNAYDER interior design',
            html:
                `<div>
                    <h2>Вам поступила новая заявка на проект</h2>
                    <h4>Номер заявки: ${id}</h4>
                    <h4>Время: ${now}</h4>
                    <h4>От кого: ${data.fio}</h4>
                    <h4>email: ${data.email}</h4>
                    <h4>Телефон: ${data.phone}</h4>
                    <h4>Бриф: ${JSON.stringify(data.brif)}</h4>
                    <link>${process.env.FRONT_URL}/adminpanel/orders</link>
                </div>`
        })
    };

    async sendNotice(msg) {

        let now = new Date();

        await this.transporter.sendMail({
            from: process.env.SMTP_USER,
            to: process.env.EMAIL_ADMIN,
            subject: 'Вам пришло новое сообщение',
            text: 'SHNAYDER interior design',
            html:
                `<div>
                    <h4>${now}</h4>
                    <h3>Сообщение от ${msg.name} (${msg.email}):</h3>
                    <h4>${msg.message}</h4>
                    <link>${process.env.FRONT_URL}/adminpanel/chat/${msg.idUser}</link>
                </div>`
        })
    };

    async sendLinkReset(to, link) {

        await this.transporter.sendMail({
            from: process.env.SMTP_USER,
            to: to,
            subject: 'Восстановление доступа к аккаунту SHNAYDER interior design',
            text: 'SHNAYDER interior design',
            html:
                `<table style='font-family: Montserrat, sans-serif'>
                    <tr><td><h2>Для восстановление доступа к аккаунту перейдите по ссылке</h2></td></tr>
                    <tr><td style='padding: 0px 0px 30px'><a href='${process.env.FRONT_URL}/recovery/${link}'>Сменить пароль</a></td></tr>
                    <tr ><td ><a href=${process.env.FRONT_URL} ><img width='147px' src='https://firebasestorage.googleapis.com/v0/b/shnayder-df141.appspot.com/o/static%2FlogoMini.png?alt=media&token=c687c667-a3ad-4145-b1c8-2670506c872c'/></a></td></tr>
                </table>`
        });
    };

    async sendList(to, data) {

        await this.transporter.sendMail({
            from: process.env.SMTP_USER,
            to: to,
            subject: data.subject,
            text: 'SHNAYDER interior design',
            html:
                `<table style='font-family: Montserrat, sans-serif; width: 100%; padding: 0px 60px' align='center'>  
                    
                    <tr><td><table style='width: 100%; max-width: 600px; border: 1px solid #EAEAEA; background-color: white' align='center'>

                        <tr><td ><img src='${data.img}' alt='' style='width: 100%'/></td></tr>

                        <tr><td><table style='background-color: white; max-width: 594px; width: 100% !important' width='100%' cellspacing='30px' align='center'>

                            <tr><td align='center' style='font-size: 20px; font-weight: 600'>${data.subject}</td></tr>

                            <tr><td style='font-size: 14px'>${data.text}</td></tr>

                            <tr><td><table align='center'>
                                <tr><td style='width: auto; background-color: #2C615E; padding: 10px 20px' align='center'>
                                    <a href='${data.btn[0]}' style='font-size: 14px; text-decoration: none; color: white; white-space: nowrap'>${data.btn[1]}</a></td></tr>
                            </table></td></tr>

                            <tr><td><table style='width: 100%'>

                                <tr align='space-between'>
                                    <td rowspan="2" width='auto' style='min-width: 177px'>
                                        <img src=${data.logo} alt='SHNAYDER interior design' width='147px'/></td>
                                                        
                                    <td align='end'>
                                        <a href=${process.env.FRONT_URL} style='font-size: 14px; color: #949494; white-space: nowrap'>© 2023 “SHNAYDER interior design”</a></td>
                                </tr>

                                <tr align='left'><td align='end'>
                                    <a href='${process.env.BACK_URL}/api/mail/${to}' style='text-decoration: none; font-size: 12px; color: #949494'>Отписаться</a></td></tr>
                                                        
                            </table></td></tr>

                        </table></td></tr>

                    </table></td></tr>

                </table>`
        });
    };
};

module.exports = new MailServ();