const crypto = require('crypto');

function genSalt() {
  const ACCESS_TOKEN_SECRET = crypto.randomBytes(32).toString('hex');
  const REFRESH_TOKEN_SECRET = crypto.randomBytes(32).toString('hex');

  console.log("ACCESS_TOKEN_SECRET:", ACCESS_TOKEN_SECRET);
  console.log("REFRESH_TOKEN_SECRET:", REFRESH_TOKEN_SECRET);
};

module.exports = genSalt;
