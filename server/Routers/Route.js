const Router = require('express');
const AuthController = require('../controllers/AuthController.js');
const AuthMiddleware = require('../service/middleware.js');
const UsersController = require('../controllers/UsersController.js');
const MailController = require('../controllers/MailController.js');
const ProjectController = require('../controllers/ProjectController.js');
const OrdersController = require('../controllers/OrdersController.js');
const ChatController = require('../controllers/ChatController.js');
const ReviewController = require('../controllers/ReviewController.js');
const AccController = require('../controllers/AccController.js');

const Routers = new Router();

Routers.post('/reg', AuthController.registration);
Routers.post('/login', AuthController.login);
Routers.get('/auth', AuthController.authenticateToken);
Routers.post('/logout', AuthMiddleware, AuthController.logout);
Routers.post('/rec', AccController.postEmail);
Routers.put('/rec', AccController.updatePass);

Routers.get('/users', AuthMiddleware, UsersController.getUsers);
Routers.get('/user/:id', AuthMiddleware, UsersController.getUser);
Routers.post('/user', UsersController.addUser);
Routers.put('/user', AuthMiddleware, UsersController.updateUser);
Routers.delete('/user', AuthMiddleware, UsersController.delUser);

Routers.get('/mail', AuthMiddleware, MailController.getEmail);
Routers.get('/mail/:email', MailController.delEmail);
Routers.post('/mail', MailController.addEmail);
// Routers.put('/mail', AuthMiddleware, MailController.updateEmail);
Routers.delete('/mail', AuthMiddleware, MailController.deleteEmail);

Routers.post('/mailing', AuthMiddleware, MailController.sendEmail);

Routers.get('/projects', ProjectController.getProjects);
Routers.post('/projects', AuthMiddleware, ProjectController.addProject);
Routers.put('/projects', AuthMiddleware, ProjectController.updateProject);
Routers.delete('/projects', AuthMiddleware, ProjectController.delProject);

Routers.get('/orders', AuthMiddleware, OrdersController.getOrders);
Routers.post('/orders', OrdersController.addOrder);
Routers.put('/orders', AuthMiddleware, OrdersController.updateOrder);
Routers.delete('/orders', AuthMiddleware, OrdersController.delOrder);

Routers.get('/chat', AuthMiddleware, ChatController.getChat);
Routers.post('/chat', AuthMiddleware, ChatController.postMsg);
Routers.get('/room', AuthMiddleware, ChatController.getAllRoom);
Routers.put('/room', AuthMiddleware, ChatController.updateMsg);

Routers.get('/review', ReviewController.getReview);
Routers.post('/review', AuthMiddleware, ReviewController.addReview);
Routers.put('/review', AuthMiddleware, ReviewController.updateReview);
Routers.delete('/review', AuthMiddleware, ReviewController.delReview);

module.exports = Routers;