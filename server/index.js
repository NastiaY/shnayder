require('dotenv').config()
const express = require('express');
const cookieParser = require('cookie-parser');
const cors = require('cors');
const Routers = require('./Routers/Route.js');
const https = require('https');
const App = express();
const socketIo = require('socket.io');
const MailServ = require('./service/MailServ.js');
const fs = require('fs');

const port = process.env.PORT || 5000;

const options = {
    key: fs.readFileSync('./certificate/server.key'),
    cert: fs.readFileSync('./certificate/server.crt')
};

const server = https.createServer(options, App);

App.use(express.json());
App.use(cookieParser());
App.use(cors({
    origin: process.env.FRONT_URL,
    optionsSuccessStatus: 200,
    credentials: true,
    exposedHeaders: ['total-count']
}));
App.use('/api', Routers);

const Io = socketIo(server, {
    cors: {
      origin: process.env.FRONT_URL
    }
  });

let onlineUsers = new Set();

Io.on('connection', socket => {
    socket.on('message', data => {
        Io.emit('response', data);
        if (data.idUser !== 2 && !onlineUsers.has(2)) {
            MailServ.sendNotice(data);
        };
    });
    socket.on('userConnect', data => {
        onlineUsers.add(data);
        Io.emit('respOnlineUsers', [...onlineUsers]);
    });
    socket.on('disconect', data => {
        onlineUsers.delete(data);
        Io.emit('respOnlineUsers', [...onlineUsers]);
    });
});

const startApp = async() => {
    try {
        server.listen(port, () => console.log(`Сервер стартовал на ${port} порту`));
    } catch(err) {
        console.log(err);
    };
};

startApp();